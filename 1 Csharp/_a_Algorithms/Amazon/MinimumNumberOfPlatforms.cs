﻿// ReSharper disable UnusedMember.Global
//
// Given arrival and departure times of all trains that reach a railway station  Your task is to
// find the minimum number of platforms required for the railway station so that no train waits
// for another train to leave the station so that it can take its place in a waiting platform
//
// Note
//
// Consider that all the trains arrive on the same day and leave on the same day  Also arrival and
// departure times will not be same for a train but we can have arrival time of one train equal to
// departure of the other  In such cases we need different platforms ie at any given instance of
// time same platform can not be used for both departure of a train and arrival of another
//
// Complexities
//
//    Time    O(N)
//    Space   O(N)

using System;
//using System.IO;
//using System.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ardalis.GuardClauses;
using Testbed._a_Algorithms.Utilities;

//using System.Collections.Concurrent;
//using System.Threading.Tasks;

namespace Testbed._a_Algorithms.Amazon
{
    public class MinimumNumberOfPlatforms
    {
        public readonly struct TrainArrivalAndDeparture
        {
            public TrainArrivalAndDeparture(in DateTime arrival, in DateTime departure)
            {
                Arrival = arrival.AddSeconds(-arrival.Second); //0 vital
                Departure = departure.AddSeconds(-departure.Second); //0 vital

                if (Arrival >= Departure)
                    throw new ArgumentOutOfRangeException(nameof(arrival));

                if (Arrival.Date != Departure.Date)
                    throw new ArgumentOutOfRangeException(nameof(departure));

                ArrivalInMinutes = (uint)TimeSpan.FromTicks(arrival.Ticks).TotalMinutes;
                DepartureInMinutes = (uint)TimeSpan.FromTicks(departure.Ticks).TotalMinutes;

                //0 we remove the seconds component as its insignificant
            }

            public DateTime Arrival { get; }
            public DateTime Departure { get; }

            public uint ArrivalInMinutes { get; }
            public uint DepartureInMinutes { get; }
        }

        static public int Calculate(IReadOnlyCollection<TrainArrivalAndDeparture> schedule) //prefix sum with a twist
        {
            Guard.Against.Null(schedule, nameof(schedule));

            if (!schedule.Any())
                return 0;

            if (schedule.Count == 1)
                return 1;

            var min = (uint)0;
            var max = (uint)0;
            var gcd = (uint)0;
            Parallel.Invoke(
                () => min = schedule.Select(x => x.ArrivalInMinutes).AsParallel().Min(),
                () => max = schedule.Select(x => x.DepartureInMinutes).AsParallel().Max(),
                () => gcd = schedule
                    .SelectMany(x => new[] { x.ArrivalInMinutes, x.DepartureInMinutes })
                    .AsParallel()
                    .Aggregate(Gcd)
            );

            max += gcd; //order vital
            var aux = new int[1 + ((max - min) / gcd)]; //order
            Parallel.ForEach(
                schedule,
                (singleSchedule, _, __) =>
                {
                    var arrivalIndex = (singleSchedule.ArrivalInMinutes - min) / gcd;
                    Interlocked.Add(ref aux[arrivalIndex], +1);

                    var departureIndex = 1 + ((singleSchedule.DepartureInMinutes - min) / gcd); //0
                    Interlocked.Add(ref aux[departureIndex], -1);
                }
            );

            var maxOverlapCount = 0;
            for (var i = 1; i < aux.Length; i += 1)
            {
                aux[i] += aux[i - 1];
                maxOverlapCount = Math.Max(maxOverlapCount, aux[i]);
            }

            return maxOverlapCount;

            //0 if the vanilla departure time coincides with the arrival time of another train we consider this
            //  a collision   however if we use the original algorithm ala
            //
            //           var departureIndex = ((singleSchedule.DepartureInMinutes) / gcd) - min
            //
            //  then it wont count as a collision   to force the issue we need to move departureIndex one click
            //  further hence the +1 you see here
        }

        static private readonly Func<uint, uint, uint> Gcd = GlobalCommonDivisor.Gcd; //optimization
    }
}
