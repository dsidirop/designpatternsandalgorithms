﻿// dotnet run -p BenchmarkDotNetDemo.csproj -c Release

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbench
{
    public class Program
    {
        static public void Main(string[] args)
        {
            _ = BenchmarkRunner.Run<Foobar>();
        }

        [TestClass]
        [MemoryDiagnoser]
        public class Foobar
        {
            private const int Count = 10;
            static private readonly IEqualityComparer<string> CaseInsensitiveComparer = StringComparer.InvariantCultureIgnoreCase;

            static private HashSet<string> TwoBuddiesToNetworkConverter_EnumerableT((string friend1, string friend2) buddies)
            {
                var (friend1, friend2) = buddies;

                return UExtensions.Enumify(friend1, friend2).ToHashSet(CaseInsensitiveComparer);
            }

            static private HashSet<string> TwoBuddiesToNetworkConverter_EnumerableStrings((string friend1, string friend2) buddies) //2nd fastest and consumes 
            {
                var (friend1, friend2) = buddies;

                return UExtensions.EnumifyStrings(friend1, friend2).ToHashSet(CaseInsensitiveComparer);
            }

            static private HashSet<string> TwoBuddiesToNetworkConverter_EnumerableObjects((string friend1, string friend2) buddies)
            {
                var (friend1, friend2) = buddies;

                var result = new HashSet<string>(2, CaseInsensitiveComparer);
                foreach (var x in UExtensions.EnumifyObjects(friend1, friend2))
                {
                    result.Add((string) x);
                }

                return result;
            }


            static private HashSet<string> TwoBuddiesToNetworkConverter_ArrayOfStrings((string friend1, string friend2) buddies) //fastest but consumes the most memory
            {
                var (friend1, friend2) = buddies;

                return new[] {friend1, friend2}.ToHashSet(CaseInsensitiveComparer);
            }

            static private IEnumerable<(string friend1, string friend2)> SpawnFriendPairs(int count)
            {
                var rand = new Random();
                for (var i = 0; i < count; i += 1)
                {
                    var r = rand.Next();
                    yield return (friend1: "f1" + r, friend2: "f2" + r);
                }
            }

            [Benchmark]
            [TestMethod]
            public void TestTwoBuddiesToNetworkConverter_Parallel_EnumerableT()
            {
                foreach (var _ in SpawnFriendPairs(Count).AsParallel().Select(TwoBuddiesToNetworkConverter_EnumerableT))
                {
                }
            }

            [Benchmark]
            public void TestTwoBuddiesToNetworkConverter_NonParallel_EnumerableT()
            {
                foreach (var _ in SpawnFriendPairs(Count).Select(TwoBuddiesToNetworkConverter_EnumerableT))
                {
                }
            }

            [Benchmark]
            public void TestTwoBuddiesToNetworkConverter_ForEach_EnumerableT()
            {
                var rand = new Random();
                for (var i = 0; i < Count; i += 1)
                {
                    var r = rand.Next();
                    TwoBuddiesToNetworkConverter_EnumerableT((friend1: "f1" + r, friend2: "f2" + r));
                }
            }

            [Benchmark]
            public void TestTwoBuddiesToNetworkConverter_Parallel_EnumerableStrings()
            {
                foreach (var _ in SpawnFriendPairs(Count).AsParallel().Select(TwoBuddiesToNetworkConverter_EnumerableStrings))
                {
                }
            }

            [Benchmark]
            public void TestTwoBuddiesToNetworkConverter_NonParallel_EnumerableStrings()
            {
                foreach (var _ in SpawnFriendPairs(Count).Select(TwoBuddiesToNetworkConverter_EnumerableStrings))
                {
                }
            }

            [Benchmark]
            public void TestTwoBuddiesToNetworkConverter_ForEach_EnumerableStrings()
            {
                var rand = new Random();
                for (var i = 0; i < Count; i += 1)
                {
                    var r = rand.Next();
                    TwoBuddiesToNetworkConverter_EnumerableStrings((friend1: "f1" + r, friend2: "f2" + r));
                }
            }

            [Benchmark]
            public void TestTwoBuddiesToNetworkConverter_Parallel_EnumerableObjects()
            {
                foreach (var _ in SpawnFriendPairs(Count).AsParallel().Select(TwoBuddiesToNetworkConverter_EnumerableObjects))
                {
                }
            }

            [Benchmark]
            public void TestTwoBuddiesToNetworkConverter_NonParallel_EnumerableObjects()
            {
                foreach (var _ in SpawnFriendPairs(Count).Select(TwoBuddiesToNetworkConverter_EnumerableObjects))
                {
                }
            }

            
            [Benchmark]
            public void TestTwoBuddiesToNetworkConverter_ForEach_EnumerableObjects()
            {
                var rand = new Random();
                for (var i = 0; i < Count; i += 1)
                {
                    var r = rand.Next();
                    TwoBuddiesToNetworkConverter_EnumerableObjects((friend1: "f1" + r, friend2: "f2" + r));
                }
            }

            [Benchmark]
            public void TestTwoBuddiesToNetworkConverter_Parallel_ArrayOfStrings()
            {
                foreach (var _ in SpawnFriendPairs(Count).AsParallel().Select(TwoBuddiesToNetworkConverter_ArrayOfStrings))
                {
                }
            }

            [Benchmark]
            public void TestTwoBuddiesToNetworkConverter_NonParallel_ArrayOfStrings()
            {
                foreach (var _ in SpawnFriendPairs(Count).Select(TwoBuddiesToNetworkConverter_ArrayOfStrings))
                {
                }
            }

            [Benchmark]
            public void TestTwoBuddiesToNetworkConverter_ForEach_ArrayOfStrings()
            {
                var rand = new Random();
                for (var i = 0; i < Count; i += 1)
                {
                    var r = rand.Next();
                    TwoBuddiesToNetworkConverter_ArrayOfStrings((friend1: "f1" + r, friend2: "f2" + r));
                }
            }
        }

        static public class UExtensions
        {
            static public IEnumerable<T> Enumify<T>(T input1, T input2)
            {
                yield return input1;
                yield return input2;
            }

            static public IEnumerable<string> EnumifyStrings(string input1, string input2)
            {
                yield return input1;
                yield return input2;
            }

            static public IEnumerable EnumifyObjects(object input1, object input2)
            {
                yield return input1;
                yield return input2;
            }
        }
    }
}
