﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.StockSpan
{
    [TestClass]
    public class NextGreaterElementTestbedRed
    {
        [TestMethod]
        public void ShouldThrowArgumentExceptionGivenNullArray()
        {
            //Arrange
            var array = (int[]) null;

            //Act
            var action = new Action(() => Amazon.StockSpan.Calculate(array));

            //Assert
            action.Should().ThrowExactly<ArgumentException>().WithMessage("stockPricesPerDay");
        }
    }
}
