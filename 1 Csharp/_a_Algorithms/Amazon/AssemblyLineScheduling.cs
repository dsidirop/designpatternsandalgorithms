﻿// https://www.geeksforgeeks.org/assembly-line-scheduling-dp-34/
//
// Assembly Line Scheduling | DP-34
//
// A car factory has two assembly lines each with n stations  A station is denoted by [S[i],j] where i
// is either 1 or 2 and indicates the assembly line the station is on and j indicates the number of
// the station   The time taken per station is denoted by [a[i],j]  Each station is dedicated to some
// sort of work like engine fitting body fitting painting and so on   So a car chassis must pass
// through each of the n stations in order before exiting the factory  The parallel stations of the
// two assembly lines perform the same task  After it passes through station [S[i],j] it will continue
// to station [S[i],j+1] unless it decides to transfer to the other line  Continuing on the same line
// incurs no extra cost but transferring from line i at station j – 1 to station j on the other line
// takes time [t[i],j]  Each assembly line takes an entry time e[i] and exit time x[i] which may be different
// for the two lines   Give an algorithm for computing the minimum time it will take to build a car chassis

using Ardalis.GuardClauses;
using System;

namespace Testbed._a_Algorithms.Amazon
{
    public class AssemblyLineScheduling
    {
        static public int Calculate(
            int[,] a, //assembly costs
            int[,] t, //switch costs
            int[] e, //entry costs
            int[] x //exit costs
        )
        {
            Guard.Against.Null(a, nameof(a));
            Guard.Against.Null(t, nameof(t));

            Guard.Against.OutOfRange(a.GetLength(0), nameof(a), 2, 2);
            Guard.Against.OutOfRange(a.GetLength(1), nameof(a), 1, int.MaxValue);

            Guard.Against.OutOfRange(t.GetLength(0), nameof(t), 2, 2);
            Guard.Against.OutOfRange(t.GetLength(1), nameof(t), 1, int.MaxValue);

            Guard.Against.OutOfRange(t.GetLength(1), nameof(t), a.GetLength(1), a.GetLength(1));

            Guard.Against.OutOfRange(e.Length, nameof(e), 2, 2);
            Guard.Against.OutOfRange(x.Length, nameof(x), 2, 2);

            var n = a.GetLength(1);

            var first = e[0] + a[0, 0]; // time taken to leave first station in line 1
            var second = e[1] + a[1, 0]; // time taken to leave first station in line 2
            for (var i = 1; i < n; i++)
            {
                var up = Math.Min(first, second + t[1, i]) + a[0, i];
                var down = Math.Min(second, first + t[0, i]) + a[1, i];

                first = up;
                second = down;
            }

            first += x[0];
            second += x[1];

            return Math.Min(first, second);
        }
    }
}
