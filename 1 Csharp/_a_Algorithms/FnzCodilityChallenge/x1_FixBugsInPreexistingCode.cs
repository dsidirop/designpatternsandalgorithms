﻿// ReSharper disable UnusedVariable
// ReSharper disable UnusedMember.Global

namespace Testbed._a_Algorithms.FnzCodilityChallenge
{
    static public class MaritimeOcean1
    {
        static public void Tester()
        {
            var success0 = false;
            var success1 = false;
            var success2 = false;
            var success3 = false;
            var success4 = false;
            var success5 = false;
            var success6 = false;

            {
                var result = solution("a");
                success0 = result == 'a';
            }

            {
                var result = solution("bbaa");
                success1 = result == 'a';
            }

            {
                var result = solution("bba");
                success2 = result == 'b';
            }

            {
                var result = solution("bbaa");
                success3 = result == 'a';
            }

            {
                var result = solution("bbaaa");
                success4 = result == 'a';
            }

            {
                var result = solution("zzpweojfweohfewoihfbbzzz");
                success5 = result == 'z';
            }

            {
                var result = solution("ababab");
                success6 = result == 'a';
            }
        }

        static public char solution(string S)
        {
            int[] occurrences = new int[26];
            foreach (char ch in S)
            {
                occurrences[(int)ch - 'a']++;
            }

            char best_char = 'a';
            int best_res = occurrences[0];

            for (int i = 1; i < 26; i++)
            {
                if (occurrences[i] > best_res)
                {
                    best_char = (char)('a' + i);
                    best_res = occurrences[i];
                }
            }

            return best_char;
        }
    }
}


