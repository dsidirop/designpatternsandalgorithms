﻿using System;
using System.Globalization;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TrainArrivalAndDeparture = Testbed._a_Algorithms.Amazon.MinimumNumberOfPlatforms.TrainArrivalAndDeparture;

namespace Testbed._a_Algorithms.Amazon.Tests.MinimumNumberOfPlatforms.TrainArrivalAndDepartureTestbed
{
    [TestClass]
    public class TrainArrivalAndDepartureTestbedRed
    {
        [TestMethod]
        [DataRow(@"2010/01/10 10:30", @"2010/01/10 10:00")]
        [DataRow(@"2010/01/10 10:00", @"2010/01/10 10:00")]
        [DataRow(@"2010/01/10 10:00:00", @"2010/01/10 10:00:10")]
        public void ShouldThrowArgumentExceptionGivenArrivalAtOrAfterDeparture(string arrival, string departure)
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => new TrainArrivalAndDeparture(ParseDate(arrival), ParseDate(departure)));

            //Assert
            action.Should().ThrowExactly<ArgumentOutOfRangeException>().WithMessage("*'arrival'*");
        }

        [TestMethod]
        public void ShouldThrowArgumentOutOfRangeExceptionGivenArrivalAndDepartureOnDifferentDates()
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => new TrainArrivalAndDeparture(ParseDate(@"2010/01/10 10:00"), ParseDate(@"2010/01/11 10:30")));

            //Assert
            action.Should().ThrowExactly<ArgumentOutOfRangeException>().WithMessage("*'departure'*");
        }

        private const string DateFormat = @"yyyy'/'MM'/'dd HH':'mm";
        private const string DateFormatWithSeconds = @"yyyy'/'MM'/'dd HH':'mm':'ss";
        static private DateTime ParseDate(in string dateString)
        {
            var success1 = DateTime.TryParseExact(dateString, DateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out var date1);
            if (success1) return date1;

            var success2 = DateTime.TryParseExact(dateString, DateFormatWithSeconds, CultureInfo.InvariantCulture, DateTimeStyles.None, out var date2);
            if (success2) return date2;

            throw new ArgumentException(nameof(dateString));
        }
    }
}
