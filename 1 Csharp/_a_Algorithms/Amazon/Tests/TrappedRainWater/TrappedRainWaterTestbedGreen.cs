﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.TrappedRainWater
{
    [TestClass]
    public class TreeContentsEqualTestbedGreen
    {
        [TestMethod]
        [DataRow(new int[] { }, (ulong)0)]
        [DataRow(new[] { 10 }, (ulong)0)]
        [DataRow(new[] { 10, 4 }, (ulong)0)]
        [DataRow(new[] { -10 }, (ulong)10)]
        [DataRow(new[] { -10, 10 }, (ulong)10)]
        [DataRow(new[] { 10, -10 }, (ulong)10)]
        [DataRow(new[] { 10, 0, 3 }, (ulong)3)]
        [DataRow(new[] { 10, 0, 0, 3 }, (ulong)6)]
        [DataRow(new[] { 3, 0, 3 }, (ulong)3)]
        [DataRow(new[] { 1, 0, 1 }, (ulong)1)]
        [DataRow(new[] { 1, 0, 0 }, (ulong)0)]
        [DataRow(new[] { 0, 1, 0 }, (ulong)0)]
        [DataRow(new[] { -2, -3, -2 }, (ulong)7)] //waterpit
        [DataRow(new[] { 0, 1, 2, 3, 2, 1, 0 }, (ulong)0)] //mountain
        [DataRow(new[] { 3, 2, 1, 0, 3 }, (ulong)6)] //lopsided mountain
        [DataRow(new[] { 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1 }, (ulong)6)]
        [DataRow(new[] { 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 2, 1, 2, 1 }, (ulong)7)]
        public void ShouldCorrectMissingNumberGivenValidArray(in int[] brickHeights, in ulong expectedRainWater)
        {
            //Arrange
            //...

            //Act
            var result = Amazon.TrappedRainWater.Calculate(brickHeights);

            //Assert
            result.Should().Be(expectedRainWater);
        }
    }
}

