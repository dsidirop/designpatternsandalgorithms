﻿(function (self, $, _jsonLib, _document, _attendancesService, _artistsFollowingsService, undefined) { //0 enrichmodulepattern revealmodulepattern
    "use strict";

    self.Init = function () {
        var $doc = $(_document);
        $doc.on("click", ".js-toggle-attendance", btnToggleAttendance_click);
        $doc.on("click", ".js-toggle-followartist", btnToggleFollowArtist_click);
    };

    function btnToggleAttendance_click(ea) {
        var $button = $(ea.target);
        _attendancesService.toggleAttendance({
            data: {
                gigId: $button.attr("data-gig-id")
            },
            done: attendancesService_toggleAttendanceDone,
            fail: anyServiceCall_genericFailHandler,
            extraCallbackData: {
                $button: $button //1
            }
        });
    };

    function attendancesService_toggleAttendanceDone(response, extraCallbackData) { //1
        extraCallbackData.$button
            .removeClass(response.going ? "btn-default" : "btn-info")
            .addClass(response.going ? "btn-info" : "btn-default")
            .text(response.going ? "Going !" : "Going ?");
    };

    function btnToggleFollowArtist_click(ea) {
        var $button = $(ea.target);
        _artistsFollowingsService.toggleFollowing({
            data: {
                artistId: $button.attr("data-artist-id")
            },
            done: artistsFollowingsService_toggleFollowingDone,
            fail: anyServiceCall_genericFailHandler,
            extraCallbackData: {
                $button: $button //1
            }
        });
    };

    function artistsFollowingsService_toggleFollowingDone(response, xd) { //1
        xd.$button
            .removeClass(response.following ? "btn-default" : "btn-info")
            .addClass(response.following ? "btn-info" : "btn-default")
            .text(response.following ? "Following !" : "Follow ?");
    };

    function anyServiceCall_genericFailHandler(xd) {
        //_bootbox.dialog({ title: "Error", message: "Operation failed!" });
    };
}(window.GigsController = window.GigsController || {}, jQuery, JSON, window.document, window.AttendancesService, window.ArtistsFollowingsService));
//0 iife  immediately invoked function expression  https://appendto.com/2010/10/how-good-c-habits-can-encourage-bad-javascript-habits-part-1/
//1 to be a hundred percent on the safe side we want the donehandler to be bound to the button that initiated the event  we dont want to have the button
//  to be placed as a class wide variable because it is conceivable that the user might click on two different buttons back to back while the first ajax
//  call is still underway thus changing the button that was supposed to be used by the donehandler of the first ajax call  such caveats are very sneaky



// ReSharper disable once UnusedParameter  ReSharper disable once InconsistentNaming
(function (self, $, _jsonLib, undefined) { //0 enrichmodulepattern revealmodulepattern
    "use strict";

    var _dudfunc = function () { };

    self.toggleAttendance = function (ajaxConfig) { //data {gigid} done fail always extracallbackdata
        ajaxConfig.done = ajaxConfig.done || _dudfunc;
        ajaxConfig.fail = ajaxConfig.fail || _dudfunc;
        ajaxConfig.always = ajaxConfig.always || _dudfunc;

        var extraCallbackDataSnapshot = ajaxConfig.extraCallbackData; //bestpractice
        $.ajax({
                url: "/api/attendances", //[toggleattendance]
                data: _jsonLib.stringify({ "GigId": ajaxConfig.data.gigId }),
                method: "POST",
                dateType: "json",
                contentType: "application/json"
            })
            .done(function (response) { ajaxConfig.done(response, extraCallbackDataSnapshot); })
            .fail(function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 401) return; //handled globally

                ajaxConfig.fail(extraCallbackDataSnapshot);
            })
            .always(function () { ajaxConfig.always(extraCallbackDataSnapshot); });
    };
}(window.AttendancesService = window.AttendancesService || {}, jQuery, JSON));
//0 iife  immediately invoked function expression  https://appendto.com/2010/10/how-good-c-habits-can-encourage-bad-javascript-habits-part-1/
