﻿-- count the total number of tickets sold for a theatric play
-- order the results in descending ticket sales order and if two
-- or more plays have sold the same amount of tickets then by
-- ascending play id

-- write your code in SQLite 3.11.0
SELECT     plays.id, plays.title, res.summed_reserved_tickets
FROM       plays
JOIN
           (
				SELECT     play_id, SUM (reserved_tickets) AS summed_reserved_tickets
                FROM       reservations 
                GROUP BY   play_id
		   ) AS res
ON
           plays.id = res.play_id
ORDER BY
		   res.summed_reserved_tickets DESC,
		   plays.id ASC