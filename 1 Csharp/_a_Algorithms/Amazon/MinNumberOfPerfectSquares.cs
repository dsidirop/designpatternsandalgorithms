﻿// ReSharper disable UnusedMember.Global
//
// https://youtu.be/NWWPbKC4qV4?t=185
//
// Make a method which accepts a positive integer number or zero and returns the minimum number
// of perfect squares which add up to the given number
//
// Examples
//
//                            n=12 -> r=3 because 12 = 2^2 + 2^2 + 2^2
//
// Notes
//
//    This is a dp (dynamic programming) problem   The key here is to realize that
//
//               f(N) = 1 + min( f(N - 1^2), f(N - 2^2), f(N - 3^3), ..., f(N - k^k) )
//
//    where k is such that k^2 <= N   Following our thoughts to the conclusion we can easily tell that
//    these recursive tree-construction will eventually generate all possible combinations of numbers
//    and their subsquares between 1 and N   we can thus solve the dp problem in a bottomup fashion
//
// Complexities
//
//    Time    O(number * square_root(number))
//    Space   O(number)
//

using System;
using System.Collections.Generic;
using Ardalis.GuardClauses;

namespace Testbed._a_Algorithms.Amazon
{
    public class MinNumberOfPerfectSquares
    {
        static public uint Calculate(in int number)
        {
            Guard.Against.Negative(number, nameof(number));

            if (number <= 1)
                return 1; //1^2

            var minPureSquaresCount = new Dictionary<int, uint>
            {
                [0] = 0,
                [1] = 1
            };

            for (var i = 1; i <= number; i += 1) //bottom up buildup
            {
                for (var j = 1; j * j <= i; j += 1)
                {
                    var m = i - j * j;

                    minPureSquaresCount[i] = !minPureSquaresCount.TryGetValue(i, out var existingCount)
                        ? minPureSquaresCount[m] + 1
                        : Math.Min(minPureSquaresCount[m] + 1, existingCount);
                }
            }

            return minPureSquaresCount[number];
        }
    }
}
