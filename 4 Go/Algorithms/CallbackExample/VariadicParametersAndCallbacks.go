package CallbackExample

type Client struct {
	authKey string
}

type ClientOption func(c *Client) // this is how we define a callback

// putting it all together via:
//
//       NewClient(WithAuth("my-authorization-key"))
//
func NewClient(opts ...ClientOption) *Client {
	client := &Client{}
	for _, opt := range opts {
		opt(client) // invoke callbacks one by one
	}

	return client
}

func WithAuth(key string) ClientOption { // this is the ClientOption function type
	return func(c *Client) {
		c.authKey = key
	}
}
