package main

import (
	"container/list"
	"fmt"
)

func main() {
	l := list.New() // Create a new list and put some numbers in it
	e4 := l.PushBack(4)
	e1 := l.PushFront(1)
	l.InsertBefore(3, e4)
	l.InsertAfter(2, e1)

	for e := l.Front(); e != nil; e = e.Next() { // Iterate through list and print its contents
		fmt.Println(e.Value)
	}
}
