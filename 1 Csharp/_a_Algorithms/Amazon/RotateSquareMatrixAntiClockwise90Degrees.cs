﻿// https://www.geeksforgeeks.org/inplace-rotate-square-matrix-by-90-degrees/
//
// rotate square matrix anticlockwise 90 degrees in place aka without using extra memory
//
// Input
// 1  2  3
// 4  5  6
// 7  8  9
//
// Output
// 3  6  9
// 2  5  8
// 1  4  7
//
// Solution Description
//
// Step1   We perform a transpose on the input array first
//
// 1  4  7
// 2  5  8
// 3  6  9
//
// Step2   We then flip the matrix vertically
//
// 3  6  9
// 2  5  8
// 1  4  7
//
// Resources
//
//      https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/arrays/multidimensional-arrays
//

using System;

namespace Testbed._a_Algorithms.Amazon
{
    static public class RotateSquareMatrixAntiClockwise90Degrees
    {
        static public void Rotate<T>(ref T[,] matrix)
        {
            if (!matrix.IsSquareMatrix())
                throw new ArgumentException(nameof(matrix));

            Transpose(ref matrix);
            FlipVertically(ref matrix);
        }

        static public void Transpose<T>(ref T[,] matrix)
        {
            if (!matrix.IsSquareMatrix())
                throw new ArgumentException(nameof(matrix));

            var edgeSize = matrix.GetLength(0);
            for (var rowIndex = 0; rowIndex < edgeSize; rowIndex++)
            {
                for (var columnIndex = 0; columnIndex < edgeSize; columnIndex++)
                {
                    if (columnIndex <= rowIndex) continue; //0

                    var temp = matrix[rowIndex, columnIndex];
                    matrix[rowIndex, columnIndex] = matrix[columnIndex, rowIndex];
                    matrix[columnIndex, rowIndex] = temp;
                }
            }

            //0 we only need to process the elements that are above the main diagonal in the upper right corner
            //  all other elements can be skipped
        }

        static public void FlipVertically<T>(ref T[,] matrix)
        {
            if (matrix == null)
                throw new ArgumentException(nameof(matrix));

            var rowsCount = matrix.GetLength(0);
            if (rowsCount <= 1)
                return;

            var columnsCount = matrix.GetLength(1);
            var halfRowsCount = rowsCount / 2;
            for (var rowIndex = 0; rowIndex < halfRowsCount; rowIndex++) //0
            {
                for (var columnIndex = 0; columnIndex < columnsCount; columnIndex++)
                {
                    var temp = matrix[rowIndex, columnIndex];
                    matrix[rowIndex, columnIndex] = matrix[rowsCount - 1 - rowIndex, columnIndex];
                    matrix[rowsCount - 1 - rowIndex, columnIndex] = temp;
                }
            }

            //0 we only need to process the elements that are above the horizontal axis   all other elements can be skipped
        }

        static private bool IsSquareMatrix<T>(this T[,] matrix) => matrix != null && matrix.GetLength(0) == matrix.GetLength(1);
    }
}
