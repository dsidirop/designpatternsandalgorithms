﻿// https://www.geeksforgeeks.org/edit-distance-dp-5/
//
// Given two strings str1 and str2 and below operations that can be performed on str1
// Find minimum number of edits aka 'operations' required to convert ‘str1’ into ‘str2’
//
//        Insert
//        Remove
//        Replace
//
// This is like the
//
//    EditDistanceDpTabulizationApproach
//
// Only this time we have reduced the space footprint to O(l2) (down from O(l1xl2))
//
// Complexities
//
//    Time    O(l1 x l2)
//    Space   O(l2)
//

using Ardalis.GuardClauses;
using System;

namespace Testbed._a_Algorithms.Amazon
{
    public class EditDistanceDpSmartTabulizationApproach
    {
        static public int Calculate(string s1, string s2)
        {
            Guard.Against.Null(s1, nameof(s1));
            Guard.Against.Null(s2, nameof(s2));

            var l1 = s1.Length;
            var l2 = s2.Length;
            var dp = new int[2, l1 + 1]; //1
            for (var i = 0; i <= l1; i++) //2
            {
                dp[0, i] = i;
            }

            for (var i = 1; i <= l2; i++) //3 for s2
            {
                for (var j = 0; j <= l1; j++) //4 for s1
                {
                    if (j == 0) //5
                    {
                        dp[i % 2, j] = i;
                    }
                    else if (s1[j - 1] == s2[i - 1])
                    {
                        dp[i % 2, j] = dp[(i - 1) % 2, j - 1];
                    }
                    else
                    {
                        var iMod2 = i % 2;
                        var iMinusOneMod2 = (i - 1) % 2;

                        dp[iMod2, j] = 1 + Math.Min(
                            dp[iMinusOneMod2, j],
                            Math.Min(
                                dp[iMod2, j - 1],
                                dp[iMinusOneMod2, j - 1]
                            )
                        );
                    }
                }
            }

            return dp[l2 % 2, l1]; //6

            //1 create a dp array to memoize result of previous computations
            //2 base condition when second string is empty then we remove all characters 
            //3 start filling the dp   this loop run for every character in second string
            //4 this loop compares the char from second string with first string characters
            //5 if first string is empty then we have to perform add character operation
            //  to get second string
            //6 after complete fill the DP array if the len2 is even then we end up in
            //  the 0th row else we end up in the 1th row so we take len2 % 2 to get row
        }
    }
}
