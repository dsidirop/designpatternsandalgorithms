﻿// ReSharper disable UnusedMember.Global
//
// Given an array arr[] of N non-negative integers representing height of the
// number of brick-blocks at index i as A[i] where the width of each block is 1
//
// Compute how much water can be trapped in between blocks after raining
//
//
//               __1__
//       ----1---|| ||---1----
//          |_|         |_|
//
// Notes
//
//    - Can the brick count be negative? Yes   It means bricks are missing from
//      the ground and water can actually be stored in such a hole in the ground
//
// Complexity
//
//    Time    O(n)
//    Space   O(1)

using Ardalis.GuardClauses;

namespace Testbed._a_Algorithms.Amazon
{
    public class TrappedRainWater
    {
        static public decimal Calculate(in int[] wallHeightPerPosition)
        {
            Guard.Against.Null(wallHeightPerPosition, nameof(wallHeightPerPosition));

            var length = wallHeightPerPosition.Length;
            if (length == 0)
                return 0;

            var maxIndex = wallHeightPerPosition.Length - 1;
            if (maxIndex == 0)
                return wallHeightPerPosition[0] < 0
                    ? -wallHeightPerPosition[0]
                    : 0;

            var (waterSum1, talestLastBarrierIndex) = CalculateImpl(
                wallHeightPerPosition: wallHeightPerPosition,
                startingIndex: 0,
                endIndex: maxIndex,
                forwardsVsBackwards: true
            );

            var (waterSum2, _) = CalculateImpl(
                wallHeightPerPosition: wallHeightPerPosition,
                startingIndex: maxIndex,
                endIndex: talestLastBarrierIndex,
                forwardsVsBackwards: false
            );

            return waterSum1 + waterSum2;
        }

        static private (decimal WaterSum, int TalestLastBarrierIndex) CalculateImpl(
            in int[] wallHeightPerPosition,
            in int startingIndex,
            in int endIndex,
            in bool forwardsVsBackwards
        )
        {
            Guard.Against.Null(wallHeightPerPosition, nameof(wallHeightPerPosition));
            Guard.Against.Negative((decimal) endIndex, nameof(endIndex));
            Guard.Against.Negative((decimal) startingIndex, nameof(startingIndex));

            if (startingIndex == endIndex)
                return (0, startingIndex);

            var increment = forwardsVsBackwards ? +1 : -1;

            var waterSum = (decimal) 0;
            var startBarrierWallIndex = startingIndex;
            var startBarrierWallHeight = 0;
            for (; startBarrierWallIndex != endIndex + increment; startBarrierWallIndex += increment)
            {
                startBarrierWallHeight = wallHeightPerPosition[startBarrierWallIndex];
                if (startBarrierWallHeight > 0)
                    break;

                if (startBarrierWallHeight < 0)
                {
                    waterSum += (ulong) -startBarrierWallHeight;
                }
            }

            if (startBarrierWallIndex == endIndex + increment)
                return (WaterSum: waterSum, TalestLastBarrierIndex: endIndex);

            startBarrierWallHeight = wallHeightPerPosition[startBarrierWallIndex];
            for (var nextHeight = 0; startBarrierWallIndex != endIndex; startBarrierWallIndex += increment)
            {
                nextHeight = wallHeightPerPosition[startBarrierWallIndex + increment];
                if (startBarrierWallHeight > nextHeight) //find the first barrier wall
                    break;

                startBarrierWallHeight = nextHeight;
            }

            var waterInPond = (ulong) 0;
            for (
                var i = startBarrierWallIndex + increment; //now find the next barrier wall
                i != endIndex + increment;
                i += increment
            )
            {
                var currentWallHeight = wallHeightPerPosition[i];
                if (startBarrierWallHeight <= currentWallHeight)
                {
                    waterSum += waterInPond;

                    waterInPond = 0; //order
                    startBarrierWallIndex = i; //order
                    startBarrierWallHeight = currentWallHeight; //order
                    continue;
                }

                waterInPond += currentWallHeight >= 0
                    ? (ulong) (startBarrierWallHeight - currentWallHeight)
                    : (ulong) -currentWallHeight;
            }

            return (WaterSum: waterSum, TalestLastBarrierIndex: startBarrierWallIndex);
        }
    }
}
