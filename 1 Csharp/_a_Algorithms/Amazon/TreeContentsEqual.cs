﻿//
// Given two arbitrary binary trees like the following ones
//
// a:      02
//      /      \
//     03      10
//    /  \    /  \
//   4    7  20   17
//
// b:      01
//      /      \
//     90      10
//    /  \
//   04  20
//
// Write a time and space efficient method which determines whether they are equal content wise
//
// Notes
//
//    - Trees being arbitrary means exactly that they are typically unordered
//    - and that they need not be balanced
//    - A tree can contain duplicate nodes
//    - The Solution should scale well even when both trees have millions of nodes
//
// Complexities
//
//      Time    O(min(count(a), count(b)))
//      Space   O(count(a) + count(b))
//

using System.Collections.Generic;
using System.Linq;

namespace Testbed._a_Algorithms.Amazon
{
    static public class TreeContentsEqual
    {
        public class TreeNode
        {
            public int Data { get; set; }
            public TreeNode Left { get; set; }
            public TreeNode Right { get; set; }
        }

        static public bool Calculate(in TreeNode a, in TreeNode b)
        {
            if (a == null || b == null)
                return a == null && b == null;

            using var streamA = TraverseInPlace(a).GetEnumerator();
            using var streamB = TraverseInPlace(b).GetEnumerator();

            var frequencies = new Dictionary<int, int>();
            while (true)
            {
                var hasNextA = streamA.MoveNext();
                var hasNextB = streamB.MoveNext();
                if (!hasNextA && !hasNextB)
                    break;

                if (!hasNextA || !hasNextB)
                    return false; //the two trees are definately not equal in total count of nodes

                var dataA = streamA.Current;
                var dataB = streamB.Current;
                if (dataA == dataB) //optimization
                    continue;

                UpdateFrequencies(dataA, frequencies, +1);
                UpdateFrequencies(dataB, frequencies, -1);
            }

            return !frequencies.Any();

            static void UpdateFrequencies(in int data, in IDictionary<int, int> freq, in int plusOneOrMinusOne)
            {
                if (freq.TryAdd(data, plusOneOrMinusOne))
                    return;

                var newValue = (freq[data] += plusOneOrMinusOne);
                if (newValue == 0) freq.Remove(data);
            }
        }

        // https://stackoverflow.com/a/791378/863651  this method consumes O(1) space
        static private IEnumerable<int> TraverseInPlace(TreeNode root)
        {
            var curr = (TreeNode) null;
            var right = (TreeNode) null;
            var parent = root;
            while (parent != null)
            {
                curr = parent.Left;
                if (curr != null)
                {
                    while (curr != right && curr.Right != null) // search for thread
                    {
                        curr = curr.Right;
                    }

                    if (curr != right)
                    {
                        curr.Right = parent; // insert thread
                        yield return parent.Data;
                        parent = parent.Left;
                        continue;
                    }

                    curr.Right = null; //   remove thread left subtree of P already traversed
                    //                      this restores the node to original state
                }
                else
                {
                    yield return parent.Data;
                }

                right = parent;
                parent = parent.Right;
            }
        }
    }
}

//
// this traversion is a bit more optimal but still not the best space wise
//
// static private IEnumerable<int> TraverseUsingStack(TreeNode root)
// {
//     if (root == null)
//         yield break;
// 
//     var stack = new Queue<TreeNode>();
//     stack.Enqueue(root);
//     while (stack.Any())
//     {
//         var node = stack.Dequeue();
//         yield return node.Data;
// 
//         if (node.Left != null) stack.Enqueue(node.Left);
//         if (node.Right != null) stack.Enqueue(node.Right);
//     }
// }