﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.MinPriorityQueue
{
    [TestClass]
    public class MinPriorityQueueFromArrayTestbedTweak
    {
        [TestMethod]
        public void ShouldReturnNullGivenEmptyHeap()
        {
            //Arrange
            var queue = new MinPriorityQueueFromArray<int>();

            //Act
            var action = new Action(() => queue.Tweak(1, 2));

            //Assert
            action.Should().ThrowExactly<ArgumentException>();
        }

        [TestMethod]
        [DataRow(1, 2)]
        [DataRow(1, 1)]
        [DataRow(2, 1)]
        public void ShouldReturnSingleItemGivenSingleItemHeap(int initial, int eventual)
        {
            //Arrange
            var queue = new MinPriorityQueueFromArray<int>();
            queue.Insert(initial);

            //Act
            queue.Tweak(initial, eventual);
            var min = queue.Peek();

            //Assert
            min.Should().Be(eventual);
        }
    }
}
