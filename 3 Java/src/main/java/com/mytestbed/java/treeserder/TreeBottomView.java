package com.mytestbed.java.treeserder;

// https://www.geeksforgeeks.org/bottom-view-binary-tree/
//
// Given a Binary Tree we need to print the bottom view from left to right   A node x is there
// in output if x is the bottommost node at its horizontal distance    Horizontal distance of
// left child of a node x is equal to horizontal distance of x minus 1 and that of right child
// is horizontal distance of x plus 1

import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

public class TreeBottomView
{
    public static void main(String[] args)
    {
        TreeNode root = spawnTree();

        Map<Integer, TreeNode> results = bottomView(root);

        System.out.println("Bottom view is:");
        for (Map.Entry<Integer, TreeNode> x : results.entrySet())
        {
            System.out.print(" " + x.getValue().value);
        }
    }

    public static TreeNode spawnTree()
    {
        TreeNode root = new TreeNode(4);

        root.left = new TreeNode(2);
        root.left.left = new TreeNode(1);
        root.left.right = new TreeNode(3);

        root.right = new TreeNode(6);
        root.right.left = new TreeNode(5);
        root.right.right = new TreeNode(7);

        return root;
    }

    static class TreeNode
    {
        Integer value;
        TreeNode left;
        TreeNode right;

        TreeNode(Integer value)
        {
            this.value = value;
        }
    }

    public static Map<Integer, TreeNode> bottomView(TreeNode root)
    {
        class TreeNodeAndHorDistancePair
        {
            TreeNode node;
            int horizontalDistance;

            TreeNodeAndHorDistancePair(TreeNode node, int horizontalDistance)
            {
                this.node = node;
                this.horizontalDistance = horizontalDistance;
            }
        }

        Map<Integer, TreeNode> results = new TreeMap<>();
        if (root == null)
            return results;

        Queue<TreeNodeAndHorDistancePair> queue = new LinkedList<TreeNodeAndHorDistancePair>()
        {{
            add(new TreeNodeAndHorDistancePair(root, 0));
        }};
        while (!queue.isEmpty())
        {
            TreeNodeAndHorDistancePair current = queue.poll();

            if (current.node.left != null)
            {
                queue.add(new TreeNodeAndHorDistancePair(current.node.left, current.horizontalDistance - 1));
            }

            if (current.node.right != null)
            {
                queue.add(new TreeNodeAndHorDistancePair(current.node.right, current.horizontalDistance + 1));
            }

            results.put(current.horizontalDistance, current.node);
        }

        return results;
    }
}
