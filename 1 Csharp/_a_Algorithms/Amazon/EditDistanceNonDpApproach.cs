﻿// https://www.geeksforgeeks.org/edit-distance-dp-5/
//
// Given two strings str1 and str2 and below operations that can performed on str1
// Find minimum number of edits aka 'operations' required to convert ‘str1’ into ‘str2’
//
//        Insert
//        Remove
//        Replace
//
// The approach shown bellow isnt optimal because it doesnt employ a dynamic-programming
// mindset and its facilities
//
// Complexities
//
//    Time:    O(3^s1length)                exponential
//    Space:   O(min(s1length, s2length))   because the recursive nature of the algorithm implies the use of a stack
//

using Ardalis.GuardClauses;
using System;

namespace Testbed._a_Algorithms.Amazon
{
    public class EditDistanceNonDpApproach
    {
        static public int Calculate(string s1, string s2)
        {
            Guard.Against.Null(s1, nameof(s1));
            Guard.Against.Null(s2, nameof(s2));

            return CalculateImpl(s1, s2, s1.Length, s2.Length);
        }

        static private int CalculateImpl(
            string s1,
            string s2,
            in int l1,
            in int l2
        )
        {
            Guard.Against.Null(s1, nameof(s1));
            Guard.Against.Null(s2, nameof(s2));
            Guard.Against.Negative(l1, nameof(l1));
            Guard.Against.Negative(l2, nameof(l2));

            if (l1 == 0) //0 first string emptied
                return l2;

            if (l2 == 0) //1 second string emptied
                return l1;

            if (s1[l1 - 1] == s2[l2 - 1]) //2
                return CalculateImpl(s1, s2, l1 - 1, l2 - 1);

            return 1 + Math.Min(
                CalculateImpl(s1, s2, l1, l2 - 1), //insert
                Math.Min(
                    CalculateImpl(s1, s2, l1 - 1, l2), //remove
                    CalculateImpl(s1, s2, l1 - 1, l2 - 1) //replace
                )
            );

            //0 if first string is empty the only option is to insert all characters of second string into first
            //1 if second string is empty the only option is to remove all characters of first string
            //2 if the trailing chars of the two strings are the same nothing much to do   ignore last characters
            //  and get count for the remainders of the two strings
        }
    }
}
