﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.KadanesAlgorithm
{
    [TestClass]
    public class KadanesAlgorithmTestbedGreen
    {
        [TestMethod]
        [DataRow(new[] { 1 }, 0, 0, "1")]
        [DataRow(new[] { 0, 1 }, 0, 1, "1")]
        [DataRow(new[] { 1, 0 }, 0, 0, "1")]
        [DataRow(new[] { 1, 2, 3 }, 0, 2, "6")]
        [DataRow(new[] { 3, 2, 1 }, 0, 2, "6")]
        [DataRow(new[] { 3, 2, 1 }, 0, 2, "6")]
        [DataRow(new[] { -1, 1 }, 1, 1, "1")]
        [DataRow(new[] { -1, 1, -2, 2, -4 }, 3, 3, "2")]
        [DataRow(new[] { -3, -2, -1, -2, -3 }, 2, 2, "-1")]
        [DataRow(new[] { -1, 1, 2, -1, 2, -4 }, 1, 4, "4")]
        [DataRow(new[] { -1, 1, 2, 0, 2, -4 }, 1, 4, "5")]
        [DataRow(new[] { -1, 4, 1000, -1000, 0, 3000, -4 }, 1, 5, "3004")]
        [DataRow(new[] { -1, 4, 1000, -1000, 0, -2000, 3000, -4 }, 6, 6, "3000")]
        [DataRow(new[] { int.MaxValue, int.MaxValue }, 0, 1, "4294967294")]
        public void ShouldReturnFoundGivenMatchingParameters(in int[] input, in int expectedSolutionStartIndex, in int expectedSolutionEndIndex, in string expectedSumString)
        {
            //Arrange
            var expectedSum = decimal.Parse(expectedSumString);

            //Act
            var results = Amazon.KadanesAlgorithm.Calculate(input);

            //Assert
            results.Sum.Should().Be(expectedSum);
            results.EndIndex.Should().Be(expectedSolutionEndIndex);
            results.StartIndex.Should().Be(expectedSolutionStartIndex);
        }
    }
}
