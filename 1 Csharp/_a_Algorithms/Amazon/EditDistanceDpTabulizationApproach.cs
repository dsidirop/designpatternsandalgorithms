﻿// https://www.geeksforgeeks.org/edit-distance-dp-5/
//
// Given two strings str1 and str2 and below operations that can performed on str1
// Find minimum number of edits aka 'operations' required to convert ‘str1’ into ‘str2’
//
//        Insert
//        Remove
//        Replace
//
// Complexities
//
//    Time:    O(l1 x l2)
//    Space:   O(l1 x l2)
//
// Note this approach can be further optimized in terms of space   Have a look at
//
//    EditDistanceDpSmartTabulizationApproach
//
// Which demonstrates this approach

using Ardalis.GuardClauses;
using System;

namespace Testbed._a_Algorithms.Amazon
{
    public class EditDistanceDpTabulizationApproach
    {
        static public int Calculate(string s1, string s2)
        {
            Guard.Against.Null(s1, nameof(s1));
            Guard.Against.Null(s2, nameof(s2));

            return CalculateImpl(s1, s2, s1.Length, s2.Length);
        }

        static private int CalculateImpl(
            string s1,
            string s2,
            in int l1,
            in int l2
        )
        {
            Guard.Against.Null(s1, nameof(s1));
            Guard.Against.Null(s2, nameof(s2));
            Guard.Against.Negative(l1, nameof(l1));
            Guard.Against.Negative(l2, nameof(l2));

            var cache = new int[l1 + 1, l2 +1];
            for (var i = 0; i <= l1; ++i) // fill d[][] in a beginning to end mode
            {
                for (var j = 0; j <= l2; ++j)
                {
                    if (i == 0) // if first string is empty only option is to insert all characters of second string 
                    {
                        cache[i, j] = j; // min operations = j
                    }
                    else if (j == 0) // if second string is empty only option is to remove all characters of second string 
                    {
                        cache[i, j] = i; // min operations = i
                    }
                    else if (s1[i - 1] == s2[j - 1]) // if last characters are the same ignore last char and recur for remaining string 
                    {
                        cache[i, j] = cache[i - 1, j - 1];
                    }
                    else // if the last character is different consider all possibilities and find the minimum 
                    {
                        cache[i, j] = 1 + Math.Min(
                            cache[i, j - 1], //        insert       the elements [i,j-1] [i-1,j] and [i-1,j-1] have
                            Math.Min( //                            already been prefilled in previous iterations
                                cache[i - 1, j], //    remove       and are available for this part of the mechanism
                                cache[i - 1, j - 1] // replace
                            )
                        );
                    }
                }
            }

            return cache[l1, l2];
        }
    }
}
