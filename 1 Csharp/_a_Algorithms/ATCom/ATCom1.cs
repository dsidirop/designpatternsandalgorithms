﻿// ReSharper disable UnusedVariable
// ReSharper disable UnusedMember.Global

//using System;
//using System.Collections.Generic;
//using System.Linq;

//public class Prefix
//{
//    public static IEnumerable<string> Prefixes(IEnumerable<string> words, int length) {
//        if (length < 0)
//            throw new ArgumentException(nameof(length));
        
//        return words
//            .Where(x => x.Length >= length)
//            .Select(x => x.Substring(0, length))
//            .Distinct();
//    }
    
//    public static void Main(string[] args)
//    {
//        foreach (var p in Prefixes(new string[] { "many", "manly", "men", "maybe", "my" }, 1))
//            Console.WriteLine(p);
//    }
//}