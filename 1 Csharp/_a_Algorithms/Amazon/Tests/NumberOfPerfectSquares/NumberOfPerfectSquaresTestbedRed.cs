﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.NumberOfPerfectSquares
{
    [TestClass]
    public class MinNumberOfPerfectSquaresTestbedRed
    {
        [TestMethod]
        public void ShouldThrowArgumentExceptionGivenNegativeNumber()
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => Amazon.MinNumberOfPerfectSquares.Calculate(-1));

            //Assert
            action.Should().ThrowExactly<ArgumentException>().WithMessage("*'number'*");
        }
    }
}
