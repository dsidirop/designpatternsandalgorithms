﻿// Write a function that, given a list and a target sum, returns zero-based indices of any two distinct elements whose
// sum is equal to the target sum. If there are no such elements, the function should return null.
//
// For example, FindTwoSum(new List<int>() { 1, 3, 5, 7, 9 }, 12) should return any of the following tuples of indices:
//
//    1, 4 (3 + 9 = 12)
//    2, 3 (5 + 7 = 12)
//    3, 2 (7 + 5 = 12)
//    4, 1 (9 + 3 = 12)

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Testbed._a_Algorithms.FindTwoSum
{
    static public class TwoSum
    {
        static public void Tester()
        {
            var indixes = FindAllTwoSumIndexes(new List<int> { 12, 12, 6, 6, 6, 0 }, 12);
        }

        //https://stackoverflow.com/a/53249822/863651   <-- this is my own answer on stackoverflow    dont be alarmed    check my cv
        static public IEnumerable<Tuple<int, int>> FindAllTwoSumIndexes(IList<int> list, long desiredSum)
        {
            var count = list?.Count;
            if (list == null || count <= 1)
                return null;

            var results = new List<Tuple<int, int>>(32);
            var indexesMap = new ConcurrentDictionary<long, List<int>>(); //0 value-to-indexes
            for (var i = 0; i < count; i++)
            {
                var currentValue = list[i];
                var amountNeededToReachDesiredSum = desiredSum - currentValue;
                if (indexesMap.TryGetValue(amountNeededToReachDesiredSum, out var indexes))
                {
                    results.AddRange(indexes.Select(matchingIndex => Tuple.Create(matchingIndex, i)));
                }

                indexesMap.AddOrUpdate(
                    key: currentValue,
                    addValueFactory: key => new List<int> { i },
                    updateValueFactory: (key, value) =>
                    {
                        value.Add(i);
                        return value;
                    }
                );
            }

            return results.Any() ? results.OrderBy(x => x.Item1).ThenBy(x => x.Item2).ToList() : null;

            //0 bare in mind that the same value might be found over multiple indexes    we need to take this into account
            //  also note that we use concurrentdictionary not for the sake of concurrency or anything but because we like
            //  the syntax of the addorupdate method which doesnt exist in the simple dictionary
        }

        // suboptimal simpleton approach
        // 
        // static public IEnumerable<Tuple<int, int>> FindTwoSumIndexes(IList<int> list, int desiredSum)
        // {
        //     var count = list?.Count;
        //     if (count == null || count <= 1)
        //         return null;
        // 
        //     var results = new List<Tuple<int, int>>(32);
        //     for (var i = 0; i < count; i++)
        //     {
        //         for (var j = i + 1; j < count; j++)
        //         {
        //             if (((decimal) list[i]) + list[j] == desiredSum)
        //             {
        //                 results.Add(new Tuple<int, int>(i, j));
        //             }
        //         }
        //     }
        // 
        //     return results.Any() ? results : null;
        // }
    }
}