﻿// given an arbitrary array and a number n shift the elements of the array by n
// so as to rotate the elements of the array accordingly

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using FluentAssertions;

namespace Testbed._a_Algorithms.RotateArray
{
    [TestClass]
    public class LogEntriesStatisticsAnalyser
    {
        [TestMethod]
        public void Tester()
        {
            //Arrange
            var array = new object[] { 1, 2, 3, 4, 5 };
            var expectedResult = new object[] { 4, 5, 1, 2, 3};
            
            //Act
            Rotate_Impl_Optimal(array: ref array, n: 2);

            //Assert
            array.Should().Equal(expectedResult);
        }

        static public void Rotate_Impl_Optimal(ref object[] array, int n) //0
        {
            if (array == null)
                return;

            if (array.Length <= 1)
                return;

            n = n % array.Length;
            if (n == 0)
                return;

            if (n < 0)
                n = array.Length - (-n);

            Array.Reverse(array, index: 0000000000000000, length: array.Length - n); //order
            Array.Reverse(array, index: array.Length - n, length: n); //order
            Array.Reverse(array, index: 0000000000000000, length: array.Length);     //order

            //0 https://codereview.stackexchange.com/a/128900/184415
            //
            //  [3, 8,| 9, 7, 6] - original array
            //  [8, 3,| 9, 7, 6] - reversed first part
            //  [8, 3,| 6, 7, 9] - also reversed second part
            //  [9, 7, 6,| 3, 8] - the Solution
        }

        static public void Rotate_Impl_Simple(object[] a, int n)
        {
            if (a == null)
                return;

            if (a.Length <= 1)
                return;

            n = n % a.Length;
            if (n == 0)
                return;

            if (n < 0)
                n = a.Length - (-n);

            var firstElements = a.Take(a.Length - n);
            var trailingElements = a.Skip(a.Length - n).Take(n);

            trailingElements
                .Concat(firstElements) //dont use union here!
                .ToArray() //0 potential for memory abuse
                .CopyTo(a, 0);

            //0 the problem with this impl lies exactly in the call to .ToArray()
            //  we might get an out of memory exception when a is too big
            //  there is a better implementation we can use
        }
    }
}