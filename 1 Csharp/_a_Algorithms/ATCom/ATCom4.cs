﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.ATCom
{
    [TestClass]
    public class DateTransform
    {
        private const string OutputFormat = @"yyyyMMdd";

        static private readonly string[] AcceptableDateFormats = {
            @"yyyy'/'MM'/'dd", // notice that the use of the singlequote
            @"dd'/'MM'/'yyyy", // is a deliberate as a means to guard
            @"MM'-'dd'-'yyyy"  // against misinterpretation of the / and - chars
        };

        static public List<string> ChangeDateFormat(List<string> dates)
            => dates
                .Select(
                    dateString => DateTime.TryParseExact(
                        dateString,
                        AcceptableDateFormats,
                        CultureInfo.InvariantCulture,
                        DateTimeStyles.None,
                        out var d
                    )
                        ? (DateTime?)d
                        : null
                )
                .Where(x => x != null)
                .Select(x => x.Value.ToString(OutputFormat))
                .ToList();

        [TestMethod]
        public void Testbed()
        {
            var input1 = new List<string> { "2010/03/30", "15/12/2016", "11-15-2012", "20130720" };

            ChangeDateFormat(input1).Should().BeEquivalentTo("20100330", "20121115", "20161215");

            var input2 = new List<string> { "20130720" };

            ChangeDateFormat(input2).Should().BeEquivalentTo();
        }
    }
}