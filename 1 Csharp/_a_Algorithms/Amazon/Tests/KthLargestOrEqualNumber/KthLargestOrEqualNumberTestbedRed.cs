﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.KthLargestOrEqualNumber
{
    [TestClass]
    public class KthLargestOrEqualNumberTestbedRed
    {
        [TestMethod]
        public void ShouldThrowNullArgumentExceptionGivenNullArrayOfNumbers()
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => Amazon.KthLargestOrEqualNumber.Calculate(null, 1));

            //Assert
            action.Should().ThrowExactly<ArgumentNullException>().WithMessage("*'numbers'*");
        }

        [TestMethod]
        public void ShouldThrowArgumentOutOfRangeExceptionGivenEmptyArray()
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => Amazon.KthLargestOrEqualNumber.Calculate(new int[] {}, 1));

            //Assert
            action.Should().ThrowExactly<ArgumentOutOfRangeException>().WithMessage("*'numbers'*");
        }

        [TestMethod]
        public void ShouldThrowArgumentOutOfRangeExceptionGivenKBeingGreaterThanTheArraySize()
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => Amazon.KthLargestOrEqualNumber.Calculate(new [] {1}, 2));

            //Assert
            action.Should().ThrowExactly<ArgumentOutOfRangeException>().WithMessage("*'k'*");
        }
    }
}
