﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.StackSort
{
    [TestClass]
    public class StackSortTestbed
    {
        [TestMethod]
        public void ShouldReturnEmptyCollectionGivenEmptyHeap()
        {
            //Arrange
            var stack = new StackSort<int>();

            //Act
            var result = stack.Sort();

            //Assert
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void ShouldReturnSingleItemCollectionGivenSingleItemHeap()
        {
            //Arrange
            var stack = new StackSort<int>();
            stack.Push(2);
            stack.Push(1);

            var result = stack.Sort();

            //Assert
            result.Should().BeEquivalentTo(new[] { 2, 1 }, options => options.WithStrictOrdering());
        }
    }
}
