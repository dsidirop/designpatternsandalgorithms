﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.RearrangeArrayAlternateSorting
{
    [TestClass]
    public class RearrangeArrayAlternateSortingTestbedRed
    {
        [TestMethod]
        public void ShouldThrowNullArgumentExceptionGivenNullInputArray()
        {
            //Arrange
            var input = (int[]) null;

            //Act
            var action = new Action(() => Amazon.RearrangeArrayAlternateSorting.Calculate(ref input));

            //Assert
            action.Should().ThrowExactly<ArgumentNullException>().WithMessage("*'input'*");
        }
    }
}
