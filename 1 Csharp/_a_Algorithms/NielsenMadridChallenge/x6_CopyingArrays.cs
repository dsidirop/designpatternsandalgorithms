﻿// ReSharper disable UnusedVariable
// ReSharper disable UnusedMember.Global
// ReSharper disable NotAccessedVariable

using System;

namespace Testbed._a_Algorithms.NielsenMadridChallenge
{
    static public class x1_CopyingArrays
    {
        static public void Tester()
        {
            var a = new int[] {3, 56, 2, 45, 1000, 92, 77};
            var b = new int[] {23, 5, 12, 4, 111, 29, 7};

            // b -> 56, 5, 45, 4

        }

        static public void CopyingArrays(int[] a, int[] b)
        {
            if (a.Length != b.Length)
                throw new ArgumentException(nameof(a));

            for (var i = 0; i < a.Length; i++)
            {
                if (i % 2 == 0)
                    continue;

                b[i - 1] = a[i];
            }
        }
    }
}

//void Producer(void)
//{
//    item nextProduced;
//    while (1)
//    {
//        while ((free_index + 1) mod buff_max == full_index);
//        buff[free_index] = nextProduced;
//        free_index = (free_index + 1) mod buff_max;
//    }
//}

//void Consumer(void)
//{
//    int item;
//    Message m;
//    while (1)
//    {
//        receive(Producer, &m);
//        item = extracted_item();
//        send(Producer, &m);
//        consume_item(item);
//    }
//}
