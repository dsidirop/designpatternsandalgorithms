﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Testbed._c_Utilities.Cache
{
    // This is a generic cache subsystem based on key/value pairs where key is generic too   Key must be unique    Every cache entry has its
    // own timeout  Cache is thread safe and will delete expired entries on its own using system.threading.timers which run on threadpool threads
    // Inspired by  https://www.codeproject.com/Articles/1033606/Cache-T-A-threadsafe-Simple-Efficient-Generic-In-m   with minor tweaks
    public class Cache<K, T> : IDisposable
    {
        private readonly int _cacheMaxSize;

        public Cache(int cacheMaxSize = 0)
        {
            if (cacheMaxSize < 0)
                throw new ArgumentOutOfRangeException(nameof(cacheMaxSize));

            _cache = cacheMaxSize == 0
                ? new Dictionary<K, Tuple<Timer, T, DateTime>>()
                : new Dictionary<K, Tuple<Timer, T, DateTime>>(cacheMaxSize + 1);
            _cacheMaxSize = cacheMaxSize;
            _grandLockSlim = new ReaderWriterLockSlim();
        }

        #region AddOrUpdate, Get, Remove, Exists, Clear

        public bool AddOrUpdate(K key, T newObject, Func<T, T, T> updateFactory = null)
            => AddOrUpdate(
                key: key,
                newObject: newObject,
                updateFactory: updateFactory,
                restartTimerIfExists: true,
                cacheEntryLifetimeInSecs: Timeout.Infinite
            );

        public bool AddOrUpdate(K key, T newObject, int cacheEntryLifetimeInSecs, bool restartTimerIfExists = true, Func<T, T, T> updateFactory = null)
        {
            if (disposed) return false;

            if (cacheEntryLifetimeInSecs != Timeout.Infinite && cacheEntryLifetimeInSecs < 1)
                throw new ArgumentOutOfRangeException(nameof(cacheEntryLifetimeInSecs));

            var added = false;

            updateFactory ??= _defaultUpdateFactory;

            _grandLockSlim.EnterWriteLock();
            try
            {
                var dueTimeInMilliseconds = cacheEntryLifetimeInSecs == Timeout.Infinite ? Timeout.Infinite : cacheEntryLifetimeInSecs * 1000;

                var y = (Tuple<Timer, T, DateTime>)null;

                if (_cache.TryGetValue(key, out y))
                {
                    if (restartTimerIfExists)
                    {
                        y.Item1.Change(dueTime: dueTimeInMilliseconds, period: Timeout.Infinite);
                    }
                    _cache[key] = Tuple.Create(y.Item1, updateFactory(y.Item2, newObject), DateTime.UtcNow);
                }
                else
                {
                    added = true;
                    var newTimer = new Timer(
                        callback: RemoveByTimer,
                        state: key,
                        dueTime: dueTimeInMilliseconds,
                        period: Timeout.Infinite
                    );

                    _cache.Add(key, Tuple.Create(newTimer, newObject, DateTime.UtcNow));

                    TrimCacheIfOverflowing();
                }
            }
            finally
            {
                _grandLockSlim.ExitWriteLock();
            }

            return added;
        }
        static private readonly Func<T, T, T> _defaultUpdateFactory = (oldEntry, newEntry) => newEntry;

        public T this[K key] => Get(key);

        public T Get(K key)
        {
            if (disposed)
                return default(T);

            _grandLockSlim.EnterReadLock();
            try
            {
                var result = (Tuple<Timer, T, DateTime>)null;
                return _cache.TryGetValue(key, out result) ? result.Item2 : default(T);
            }
            finally
            {
                _grandLockSlim.ExitReadLock();
            }
        }

        public bool TryGet(K key, out T value)
        {
            if (disposed)
            {
                value = default(T);
                return false;
            }

            _grandLockSlim.EnterReadLock();
            try
            {
                var x = (Tuple<Timer, T, DateTime>)null;
                var found = _cache.TryGetValue(key, out x);
                value = found ? x.Item2 : default(T);
                return found;
            }
            finally
            {
                _grandLockSlim.ExitReadLock();
            }
        }

        public void Remove(Predicate<K> keyPattern)
        {
            if (disposed) return;

            _grandLockSlim.EnterWriteLock();
            try
            {
                var matchingKvpsSnapshot = _cache
                    .Where(x => keyPattern(x.Key))
                    .ToList();

                foreach (var kvp in matchingKvpsSnapshot)
                {
                    try
                    {
                        kvp.Value.Item1.Dispose();
                    }
                    catch
                    {
                    }

                    _cache.Remove(kvp.Key);
                }
            }
            finally
            {
                _grandLockSlim.ExitWriteLock();
            }
        }

        public void Remove(K key)
        {
            if (disposed) return;

            _grandLockSlim.EnterWriteLock();
            try
            {
                var y = (Tuple<Timer, T, DateTime>)null;
                if (!_cache.TryGetValue(key, out y))
                    return;

                try
                {
                    y.Item1.Dispose();
                }
                catch
                {
                }

                _cache.Remove(key);
            }
            finally
            {
                _grandLockSlim.ExitWriteLock();
            }
        }

        public bool Exists(K key)
        {
            if (disposed)
                return false;

            _grandLockSlim.EnterReadLock();
            try
            {
                return _cache.ContainsKey(key);
            }
            finally
            {
                _grandLockSlim.ExitReadLock();
            }
        }

        #endregion

        #region IDisposable implementation & Clear

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this); //0 bestpractice
        }
        //0 If the class implementing idisposable is not sealed then it should include the call to gc.suppressfinalize(this) even if
        //  it doesnt include a user defined finalizer     This is necessary to ensure proper semantics for derived types that add a
        //  userdefined finalizer but only override the protected Dispose(bool) method

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                disposed = true;
                if (disposing) // dispose managed resources
                {
                    Clear();
                    _grandLockSlim.Dispose();
                }
                // dispose unmanaged resources
            }
        }

        public void Clear()
        {
            _grandLockSlim.EnterWriteLock();
            try
            {
                try
                {
                    foreach (var t in _cache.Values.Select(y => y.Item1))
                        t.Dispose();
                }
                catch
                {
                }

                _cache.Clear();
            }
            finally
            {
                _grandLockSlim.ExitWriteLock();
            }
        }

        #endregion

        private void TrimCacheIfOverflowing()
        {
            if (_cacheMaxSize <= 0 || _cache.Count <= _cacheMaxSize)
                return;

            var entriesToEvict = _cache
                .OrderBy(x => x.Value.Item3)
                .Take((_cache.Count - _cacheMaxSize) + (_cacheMaxSize / 10))
                .ToList();

            foreach (var x in entriesToEvict)
            {
                try
                {
                    x.Value.Item1.Dispose();
                    _cache.Remove(x.Key);
                }
                catch
                {
                }
            }
        }

        private void RemoveByTimer(object state) => Remove((K)state);

        private bool disposed;
        private readonly ReaderWriterLockSlim _grandLockSlim;
        private readonly Dictionary<K, Tuple<Timer, T, DateTime>> _cache;
    }

    public class Cache : Cache<string, object>
    {
    }
}
