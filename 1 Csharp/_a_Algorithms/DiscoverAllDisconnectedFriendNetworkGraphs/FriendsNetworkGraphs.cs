﻿// Friend Groups
//
// Write a function that given a list of relationships outputs the contents of each friend group
// A relationship is a ‘friendship’ between two people
//
// example input:
//
//    {"rob", "nick"}
//    {"tyler", "scott"}
//    {"rob", "katia"}
//    {"aaliyara", "katia"}
//    {"james", "scott"}
//
// would output:
//
//    {"rob", "nick", "katia", "aaliyara"}
//    {"james", "scott", "tyler"}
//
// canon solution is shown here:
//
//    https://www.geeksforgeeks.org/connected-components-in-an-undirected-graph/
//
// the solution shown below is an adhoc solution

using System;
using System.Collections.Generic;
using System.Linq;
using Testbed._a_Algorithms.Amazon;

namespace Testbed._a_Algorithms.DiscoverAllDisconnectedFriendNetworkGraphs
{
    public class FriendsNetworkGraphs
    {
        static private readonly IEqualityComparer<string> CaseInsensitiveComparer = StringComparer.InvariantCultureIgnoreCase;

        static private List<HashSet<string>> InitNetworks(IEnumerable<(string friend1, string friend2)> friendRelations)
        {
            static HashSet<string> TwoBuddiesToNetworkConverter((string friend1, string friend2) buddies)
            {
                static bool NameNotDudPredicate(string f) => !string.IsNullOrWhiteSpace(f);
                static string NameTrimSelector(string f) => f.Trim();

                var (friend1, friend2) = buddies;
                return UExtensions
                    .Enumify(friend1, friend2)
                    .Where(NameNotDudPredicate)
                    .Select(NameTrimSelector)
                    .ToHashSet(CaseInsensitiveComparer);
            }

            return friendRelations
                .Select(TwoBuddiesToNetworkConverter)
                .ToList();
        }

        public List<HashSet<string>> DetermineRelationships(List<(string friend1, string friend2)> friendRelations)
        {
            var networks = InitNetworks(friendRelations);
            if (networks.Count <= 1)
                return networks;

            var coagulatedNetworks = new List<HashSet<string>>(networks.Count / 2);
            while (true)
            {
                var atLeastOneIntersectionFound = false;
                while (networks.Any())
                {
                    var currentNetwork = networks.Pop();

                    bool NetworksIntersectPredicate(HashSet<string> c) => currentNetwork.Any(c.Contains);

                    var intersectingNetwork = coagulatedNetworks.FirstOrDefault(NetworksIntersectPredicate);
                    if (intersectingNetwork == null)
                    {
                        coagulatedNetworks.Add(currentNetwork);
                        continue;
                    }

                    atLeastOneIntersectionFound = true;
                    intersectingNetwork.AddRange(currentNetwork);
                }

                if (!atLeastOneIntersectionFound) //no progress was made   this means that the results cannot be optimized further
                {
                    break;
                }

                networks = coagulatedNetworks;
                coagulatedNetworks = new List<HashSet<string>>(networks.Count / 2);
            }
            
            return coagulatedNetworks;
        }
    }

    static public class UExtensions
    {
        static public IEnumerable<T> Enumify<T>(T input1, T input2)
        {
            yield return input1;
            yield return input2;
        }

        static public IEnumerable<T> AddRange<T>(this HashSet<T> hashSet, IEnumerable<T> additionalItems)
        {
            foreach (var x in additionalItems)
            {
                hashSet.Add(x);
            }

            return hashSet;
        }
    }
}