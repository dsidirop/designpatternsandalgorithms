﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.RotateMatrixAntiClockwise90Degrees
{
    [TestClass]
    public class TransposeTestbedRed
    {
        [TestMethod]
        [DataRow(1, 0)]
        [DataRow(0, 1)]
        [DataRow(2, 3)]
        public void ShouldResultInArgumentExceptionGivenNonSquareMatrix(int rowsCount, int columnsCount)
        {
            //Arrange
            var matrix = new int[rowsCount, columnsCount];

            //Act
            var action = new Action(() => RotateSquareMatrixAntiClockwise90Degrees.Transpose(ref matrix));

            //Assert
            action.Should().ThrowExactly<ArgumentException>().WithMessage("matrix");
        }
    }
}
