using System;
using System.Threading;

namespace Testbed._c_Utilities.Cache
{
    public sealed class CalculatedGrpsCache
    {
        private readonly int _defaultCacheTimeoutForAllEntries;
        private readonly Cache<string, CalculatedGrp> _cache = new Cache<string, CalculatedGrp>();

        public CalculatedGrpsCache(int defaultCacheTimeoutForAllEntries)
        {
            if (defaultCacheTimeoutForAllEntries != Timeout.Infinite && defaultCacheTimeoutForAllEntries < 1)
                throw new ArgumentOutOfRangeException(nameof(defaultCacheTimeoutForAllEntries));

            _defaultCacheTimeoutForAllEntries = defaultCacheTimeoutForAllEntries;
        }

        public bool TryGet(string key, out CalculatedGrp value) => _cache.TryGet(key, out value);

        public void AddOrUpdate(CalculatedGrp calculatedGrp) => _cache.AddOrUpdate(calculatedGrp.Key, calculatedGrp, _defaultCacheTimeoutForAllEntries);
    }
}