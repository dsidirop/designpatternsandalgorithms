﻿// ReSharper disable UnusedMember.Global
//
// Implement a min-priority-queue backed by a dynamic array which supports
//
// - pop 0(1)
// - peek 0(1)
// - insert 0(1)
// - decrease  
//
// Negotation
//
// - can we assume it will fit in memory? yes
//

using System;
using System.Collections.Generic;
using System.Linq;

namespace Testbed._a_Algorithms.Amazon
{
    public class MinPriorityQueueFromArray<T>
    {
        private readonly IList<T> _list = new List<T>(32);
        private readonly Comparer<T> _comparer = Comparer<T>.Default;

        public T Peek()
        {
            if (!_list.Any())
                throw new InvalidOperationException("Heap is empty!");

            return _list[0];
        }

        public bool TryPeek(out T result)
        {
            result = default;
            if (!_list.Any())
                return false;

            result = _list[0];
            return true;
        }

        public bool TryPop(out T result)
        {
            result = default;
            if (!_list.Any())
                return false;

            result = Pop();
            return true;
        }

        public T Pop()
        {
            if (!_list.Any())
                throw new InvalidOperationException("Heap is empty!");

            if (_list.Count == 1)
            {
                var result = _list[0];
                _list.RemoveAt(0);
                return result;
            }

            var minimum = _list[0];
            _list[0] = _list.Pop();

            BubbleDown(index: 0);

            return minimum;
        }

        public void Insert(T key)
        {
            _list.Add(key);
            BubbleUp(_list.Count - 1);
        }

        public void Tweak(T key, T newValue)
        {
            var index = _list.IndexOf(key);
            if (index == -1) throw new ArgumentException(nameof(key));
            
            var oldValue = _list[index];
            var oldVsNew = _comparer.Compare(oldValue, newValue);
            if (oldVsNew == 0) return;

            _list[index] = newValue;
            if (oldVsNew == 1)
            {
                BubbleUp(index);
                return;
            }

            BubbleDown(index);
        }

        private void BubbleUp(int index)
        {
            while (true)
            {
                if (index == 0) return;

                var parentIndex = FindParentIndex(index);
                var child = _list[index];
                var parent = _list[parentIndex];
                if (_comparer.Compare(child, parent) >= 0) return;

                _list[index] = parent;
                _list[parentIndex] = child;
                index = parentIndex;
            }
        }

        static private int FindParentIndex(int index)
            => (index - 1) / 2;

        private void BubbleDown(int index)
        {
            while (true)
            {
                var minChildIndex = FindSmallerChildIndex(index);
                if (minChildIndex == -1) return;

                var a = _list[index];
                var b = _list[minChildIndex];
                if (_comparer.Compare(a, b) <= 0) return;

                _list[index] = b;
                _list[minChildIndex] = a;
                index = minChildIndex;
            }
        }

        private int FindSmallerChildIndex(int index)
        {
            var left = index * 2 + 1;
            var right = left + 1;
            if (left >= _list.Count) return -1;
            if (right >= _list.Count) return left;

            return _comparer.Compare(_list[left], _list[right]) == -1
                ? left
                : right;
        }
    }

    static internal class Ux
    {
        static public T Pop<T>(this IList<T> list)
        {
            if (!list.Any())
                throw new InvalidOperationException("List is empty!");

            var last = list.Last();
            list.RemoveAt(list.Count - 1);
            return last;
        }
    }
}