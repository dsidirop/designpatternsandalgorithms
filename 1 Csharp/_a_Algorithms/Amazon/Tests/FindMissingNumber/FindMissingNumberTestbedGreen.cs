﻿using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.FindMissingNumber
{
    [TestClass]
    public class RearrangeArrayAlternateSortingTestbedGreen
    {
        [TestMethod]
        [DataRow(new uint[] { }, (uint)1)]
        [DataRow(new uint[] { 1, 2, 3 }, (uint)4)]
        [DataRow(new uint[] { 3, 2, 1 }, (uint)4)]
        [DataRow(new uint[] { 2, 3, 1 }, (uint)4)]
        [DataRow(new uint[] { 2, 3, 4 }, (uint)1)]
        [DataRow(new uint[] { 2, 1, 4 }, (uint)3)]
        public void ShouldCorrectMissingNumberGivenValidArray(in uint[] input, in uint expectedMissingNumber)
        {
            //Arrange
            //...

            //Act
            var missingNumber = FindMissingNumberInArray.Calculate(input);

            //Assert
            missingNumber.Should().Be(expectedMissingNumber);
        }

        [TestMethod]
        public void ShouldCorrectMissingNumberGivenHugeArray()
        {
            //Arrange
            var expectedMissingNumber = (uint)987_654;
            var input = Enumerable
                .Range(1, 1_000_000)
                .Select(x => (uint)x)
                .Where(x => x != expectedMissingNumber)
                .ToArray();

            //Act
            var missingNumber = FindMissingNumberInArray.Calculate(input);

            //Assert
            missingNumber.Should().Be(expectedMissingNumber);
        }
    }
}
