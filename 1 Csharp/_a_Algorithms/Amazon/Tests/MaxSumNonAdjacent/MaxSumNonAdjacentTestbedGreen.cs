﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.MaxSumNonAdjacent
{
    [TestClass]
    public class NextGreaterElementTestbedGreen
    {
        [TestMethod]
        public void ShouldReturnEmptyArrayGivenEmptyStockPricesArray()
        {
            //Arrange
            var input = new int[] { };

            //Act
            var results = Amazon.MaxSumNonAdjacent.Calculate(input);

            //Assert
            results.Should().Be(0);
        }

        [TestMethod]
        public void ShouldReturnSingleElementArrayGivenSinglePriceArray()
        {
            //Arrange
            var input = new[] {1, 1000, 2, 3, 1000, 1, 3, 2000};
            var expectedResults = 4000;

            //Act
            var results = Amazon.MaxSumNonAdjacent.Calculate(input);

            //Assert
            results.Should().Be(expectedResults);
        }
    }
}
