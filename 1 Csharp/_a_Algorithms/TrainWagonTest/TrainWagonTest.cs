﻿// A train has wagonCount wagons indexed as 0, 1, ..., wagonCount - 1  Each wagon needs to be filled in the constructor of the Train  with function fillWagon
// which accepts the wagons index and returns the wagons cargo   Each train can carry only one specific type of cargo T   The code below works but the server
// has enough memory only for a small train  Refactor the code so that the server has enough memory even for a large train
//
// ReSharper disable UnusedMember.Global

using System;
using System.Collections.Generic;
using System.Linq;

namespace Testbed._a_Algorithms.TrainWagonTest
{
    static public class TrainStationTester
    {
        static public void Tester()
        {
            var train = new Train<string>(
                wagonCount: 10,
                fillWagon: wagonIndex => $"Wagon: {wagonIndex}"
            );

            for (var i = 0; i < 10; i++)
            {
                Console.WriteLine(train.PeekWagon(i));
            }
        }
    }

    public class Train<T> where T : class
    {
        private readonly Dictionary<int, T> _wagons = new Dictionary<int, T>();

        public Train(int wagonCount, Func<int, T> fillWagon)
        {
            for (var i = 0; i < wagonCount; i++)
            {
                var cargo = fillWagon(i);
                _wagons[i] = _wagons.Values.FirstOrDefault(x => x.Equals(cargo)) ?? cargo; //this used to be   _wagons[i] = cargo;
            }
        }

        public T PeekWagon(int wagonIndex)
        {
            return (T)_wagons[wagonIndex];
        }
    }
}
