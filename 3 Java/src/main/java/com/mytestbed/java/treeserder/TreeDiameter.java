package com.mytestbed.java.treeserder;

// https://www.geeksforgeeks.org/print-nodes-top-view-binary-tree/
//
// Top view of a binary tree is the set of nodes visible when the tree is viewed from the top
// Given a binary tree print the top view of it   The output nodes can be printed in any order
//
// Expected time complexity is O(n)
//
// A node x is there in output if x is the topmost node at its horizontal distance   Horizontal
// distance of left child of a node x is equal to horizontal distance of x minus 1 and that of
// right child is horizontal distance of x plus 1

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

public class TreeDiameter
{
    public static void main(String[] args)
    {
        TreeNode root = spawnTree();

        int diameter = treeDiameter(root);

        System.out.println("Tree Diameter is: " + diameter);
    }

    public static TreeNode spawnTree()
    {
        TreeNode root = new TreeNode(4);

        root.left = new TreeNode(2);
        root.left.left = new TreeNode(1);
        root.left.right = new TreeNode(3);

        root.right = new TreeNode(6);
        root.right.left = new TreeNode(5);
        root.right.right = new TreeNode(7);

        return root;
    }

    static class TreeNode
    {
        Integer value;
        TreeNode left;
        TreeNode right;

        TreeNode(Integer value)
        {
            this.value = value;
        }
    }

    public static int treeDiameter(TreeNode root)
    {
        if (root == null)
            return 0;

        final AtomicReference<Integer> diameter = new AtomicReference<>(Integer.MIN_VALUE);
        treeHeight(root, height -> diameter.set(Math.max(diameter.get(), height)));

        return diameter.get();
    }

    public static int treeHeight(TreeNode root, Consumer<Integer> callback)
    {
        if (root == null)
            return 0;

        int leftHeight = treeHeight(root.left, callback);
        int rightHeight = treeHeight(root.right, callback);

        callback.accept(1 + leftHeight + rightHeight);

        return 1 + Math.max(leftHeight, rightHeight);
    }
}
