﻿using System;

using FluentAssertions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.DutchNationalFlagSortArrayOfOnesAndZerosAndTwosTests
{
    [TestClass]
    public class DutchNationalFlagSortArrayOfOnesAndZerosAndTwosTestbedAlternative
    {
        [TestMethod]
        public void ShouldReturnEmptyArray_GivenEmptyArray()
        {
            //Arrange
            var input = new int[] { };

            //Act
            DutchNationalFlagSortArrayOfOnesAndZerosAndTwos.CalculateParallelFlavour(ref input);

            //Assert
            input.Should().BeEmpty();
        }

        [TestMethod]
        public void ShouldReturnSingleElementArray_GivenSingleElementArray()
        {
            //Arrange
            var input = new[] { 0 };

            //Act
            DutchNationalFlagSortArrayOfOnesAndZerosAndTwos.CalculateParallelFlavour(ref input);

            //Assert
            input.Should().HaveCount(1);
        }

        [TestMethod]
        public void ShouldThrowException_GivenInvalidArray()
        {
            //Arrange
            var input = new[] { 3 };

            //Act
            var action = (Action) (() => DutchNationalFlagSortArrayOfOnesAndZerosAndTwos.CalculateParallelFlavour(ref input));

            //Assert
            action.Should().Throw<Exception>();
        }

        [TestMethod]
        public void ShouldReturnSortedArray_GivenValidArray()
        {
            //Arrange
            var input = new[] {2, 2, 1, 2, 0, 0, 0};
            var initialLength = input.Length;

            //Act
            DutchNationalFlagSortArrayOfOnesAndZerosAndTwos.CalculateParallelFlavour(ref input);

            //Assert
            input.Should().HaveCount(initialLength);
            input.Should().BeInAscendingOrder();
        }
    }
}

