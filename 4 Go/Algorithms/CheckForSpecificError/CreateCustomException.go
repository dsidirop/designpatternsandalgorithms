package main

import (
	"errors"
	"fmt"
	"os"
)

type RequestError struct {
	StatusCode int

	Err error
}

func (r *RequestError) Error() string {
	return fmt.Sprintf("status %d: err: %v", r.StatusCode, r.Err)
}

func doRequest() (int, error) {
	return 0, &RequestError{
		StatusCode: 503,
		Err:        errors.New("unavailable"),
	}
}

func CreateCustomException() {
	_, err := doRequest()
	if err != nil {
		var requestError *RequestError
		if errors.As(err, &requestError) {
			fmt.Println(requestError.StatusCode)
		}

		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println("success!")
}
