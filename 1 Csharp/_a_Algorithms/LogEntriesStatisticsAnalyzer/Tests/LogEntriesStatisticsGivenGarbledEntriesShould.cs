﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Testbed._a_Algorithms.LogEntriesStatisticsAnalyzer.Tests
{
    [TestClass]
    public class LogEntriesStatisticsGivenGarbledEntriesShould : LogEntriesStatisticsTestbedBase
    {
        [TestMethod]
        public async Task ReturnOneGarbledEntryGivenGarbledStatusCodeInFile()
        {
            var tempFilePath = Path.GetTempFileName();
            try
            {
                //Arrange
                var expectedStatsPerHour = Enumerable.Range(start: 0, count: 24).ToDictionary(x => (byte)x, y => new HourlyStats());
                var expectedGarbledLinesCount = 1;

                await File.WriteAllTextAsync(tempFilePath, SpawnLogEntry(hour: 10, statusCode: HttpStatusCode.InternalServerError).Replace("\t500", "\tabc"), Encoding.ASCII);

                //Act
                var analysis = await new LogEntriesStatistics(new FileSystem()).CalculateErrorRatesAsync(tempFilePath);

                //Assert
                analysis.Should().BeEquivalentTo((expectedStatsPerHour, expectedGarbledLinesCount));
            }
            finally //Cleanup
            {
                if (!string.IsNullOrWhiteSpace(tempFilePath))
                {
                    File.Delete(tempFilePath);
                }
            }
        }

        [TestMethod]
        public async Task ReturnOneGarbledEntryGivenGarbledLineWithTooManyTabsFile()
        {
            var tempFilePath = Path.GetTempFileName();
            try
            {
                //Arrange
                var expectedStatsPerHour = Enumerable.Range(start: 0, count: 24).ToDictionary(x => (byte)x, y => new HourlyStats());
                var expectedGarbledLinesCount = 1;

                await File.WriteAllTextAsync(
                    tempFilePath,
                    $"{SpawnLogEntry(hour: 10, statusCode: HttpStatusCode.OK)}\tfoo",
                    Encoding.ASCII
                );

                //Act
                var analysis = await new LogEntriesStatistics(new FileSystem()).CalculateErrorRatesAsync(tempFilePath);

                //Assert
                analysis.Should().BeEquivalentTo((expectedStatsPerHour, expectedGarbledLinesCount));
            }
            finally //Cleanup
            {
                if (!string.IsNullOrWhiteSpace(tempFilePath))
                {
                    File.Delete(tempFilePath);
                }
            }
        }

        [DataTestMethod]
        [DataRow("10:00:0\t", DisplayName = "Time missing digits")]
        [DataRow("1:0:0::0\t", DisplayName = "Time having too many semicolons")]
        [DataRow("24:00:00\t", DisplayName = "Time above 23:59")]
        [DataRow("ab:00:00\t", DisplayName = "Invalid time")]
        public async Task ReturnOneGarbledEntryGivenGarbledLineWithMalformedHourFile(string malformedTime)
        {
            var tempFilePath = Path.GetTempFileName();
            try
            {
                //Arrange
                var expectedStatsPerHour = Enumerable.Range(start: 0, count: 24).ToDictionary(x => (byte)x, y => new HourlyStats());
                var expectedGarbledLinesCount = 1;

                await File.WriteAllTextAsync(
                    tempFilePath,
                    SpawnLogEntry(hour: 10, statusCode: HttpStatusCode.OK).Replace("10:00:00\t", malformedTime),
                    Encoding.ASCII
                );

                //Act
                var analysis = await new LogEntriesStatistics(new FileSystem()).CalculateErrorRatesAsync(tempFilePath);

                //Assert
                analysis.Should().BeEquivalentTo((expectedStatsPerHour, expectedGarbledLinesCount));
            }
            finally //Cleanup
            {
                if (!string.IsNullOrWhiteSpace(tempFilePath))
                {
                    File.Delete(tempFilePath);
                }
            }
        }
    }
}
