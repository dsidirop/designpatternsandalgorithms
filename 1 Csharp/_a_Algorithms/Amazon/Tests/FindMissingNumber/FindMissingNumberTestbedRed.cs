﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.FindMissingNumber
{
    [TestClass]
    public class RearrangeArrayAlternateSortingTestbedRed
    {
        [TestMethod]
        public void ShouldThrowNullArgumentExceptionGivenNullInputArray()
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => FindMissingNumberInArray.Calculate(null));

            //Assert
            action.Should().ThrowExactly<ArgumentNullException>().WithMessage("*'input'*");
        }

        [TestMethod]
        public void ShouldThrowArgumentArgumentExceptionGivenInputArrayWithZeros()
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => FindMissingNumberInArray.Calculate(new uint[] { 1, 2, 0 }));

            //Assert
            action.Should().ThrowExactly<ArgumentException>().WithMessage("input");
        }
    }
}
