﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable UnusedMember.Global

// Equilibrium index of an array
//
// Equilibrium index of an array is an index such that the sum of elements at lower indexes is
// equal to the sum of elements at higher indexes. For example, in an array A:
// 
// Example :
// 
// Input : A[] = {-7, 1, 5, 2, -4, 3, 0}
// Output : 3
//
// 3 is an equilibrium index, because:
//
// A[0] + A[1] + A[2]  =  A[4] + A[5] + A[6]
//
// Write a function int equilibrium(int[] arr, int n); that given a sequence arr[] of size n
// returns an equilibrium index (if any) or -1 if no equilibrium indexes exist
//
// For this scenario assume that N is always between 1 and 100000

namespace Testbed._a_Algorithms.EquilibriumIndices
{
    static public class EquilibriumIndices
    {
        static public void Tester()
        {
            var results = string.Join("\r\n", FindEquilibriumIndices(new[] { -7, 1, 5, 2, -4, 3, 0 }));
            Console.WriteLine($"** Equilibrium indexes:\r\n\r\n{results}");
        }

        static private IEnumerable<int> FindEquilibriumIndices(IEnumerable<int> sequence)
        {
            var left = 0L; //0 vital
            var right = sequence.Select(x => (long)x).Sum(); //1 vital
            var index = 0;
            foreach (var element in sequence)
            {
                right -= element;
                if (left == right)
                    yield return index;

                left += element;
                index++;
            }

            //0 its vital to employ a long in order to protect ourselves from overflows and underflows
            //1 casting to long is performed so that .sum() will employ a long to performing the summing    since we are dealing
            //  with a maximum of 100k ints we are safe from overflowing or underflowing because a long can easily fit those in
        }
    }
}