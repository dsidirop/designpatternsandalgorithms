﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.FirstBadVersion
{
    [TestClass]
    public class FirstBadVersionTestbedRed
    {
        [TestMethod]
        public void ShouldThrowNullArgumentExceptionGivenNullCallback()
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => Amazon.FirstBadVersion.SpawnFinder(null));

            //Assert
            action.Should().ThrowExactly<ArgumentNullException>().WithMessage("*'isBadVersion'*");
        }
    }
}
