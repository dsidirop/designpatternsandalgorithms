# TODO

- Create a class which allows scanning a given directory (recursively) and emits/returns all files matching given constraints (file extensions, size, last modified date)

  To scale efficiently across multiple processors we need to take advantage of TPL
  To help the consumer side of things its prudent to employ channels so as to implement N-producers/M-consumers
