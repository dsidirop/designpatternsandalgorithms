﻿// ReSharper disable UnusedVariable
// ReSharper disable UnusedMember.Global

using System;
using System.Collections.Generic;
using System.Linq;

namespace Testbed._a_Algorithms.MaritimeOcean
{
    static public class MaritimeOcean4
    {
        static public void Tester()
        {
            //the implementation works under the assumption that the indeces passed into it
            //are sane and there are no broken links

            var success0 = false;
            var success1 = false;
            var success2 = false;
            var success3 = false;

            {
                var internalNodesCount = Count();
                success0 = internalNodesCount == 0;
            }
            
            {
                var internalNodesCount = Count(1, 3, 1, -1, 3);
                success1 = internalNodesCount == 2;
            }

            {
                var internalNodesCount = Count(10, 20, -1, 30);
                success2 = internalNodesCount == 0;
            }

            {
                var internalNodesCount = Count(-1);
                success3 = internalNodesCount == 0;
            }
        }

        static public int Count(params int[] tree)
        {
            if (tree.Length == 0)
                return 0;

            var uniqueTreePointersSet = new HashSet<int>(tree);
            if (!uniqueTreePointersSet.Contains(-1))
                throw new ArgumentException($@"MO.CNT01 [BUG] Tree doesn't contain a root node!");

            if (tree.Length == 1)
                return 0;

            return tree
                .Select((parentIndex, ownIndex) => ownIndex)
                .Count(x => uniqueTreePointersSet.Contains(x));
        }
    }
}

