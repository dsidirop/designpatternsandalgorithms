﻿using System.IO;

using Ardalis.GuardClauses;

using Testbed._a_Algorithms.Utilities;

namespace Testbed._a_Algorithms.LogEntriesStatisticsAnalyzer
{
    // inspired by the internal class readlinesiterator found in the dotnetframework reference
    // implementation   we had to duplicate the implementation here
    //
    // an iterator that returns a single line at-a-time from a given file   known issues which
    // cannot be changed to remain compatible
    //
    //  - the underlying streamreader is allocated upfront for the ienumerable<t> before 
    //    getenumerator has even been called  while this is good in that exceptions such as 
    //    directorynotfoundexception and filenotfoundexception are thrown directly by 
    //    file.readlines which the user probably expects it also means that the reader 
    //    will be leaked if the user never actually foreachs over the enumerable and hence 
    //    calls dispose on at least one ienumerator<t> instance
    //
    //  - reading to the end of the ienumerator<t> disposes it  this means that dispose 
    //    is called twice in a normal foreach construct.
    //
    //  - ienumerator<t> instances from the same ienumerable<t> party on the same underlying 
    //    reader (dev10 bugs 904764)
    //
    public class StreamLinesIterator : Iterator<string>
    {
        private StreamReader _reader;

        public StreamLinesIterator(StreamReader reader)
        {
            Guard.Against.Null(reader, nameof(reader));

            _reader = reader;
        }

        protected override Iterator<string> Clone()
            => new StreamLinesIterator(_reader);

        public override bool MoveNext()
        {
            if (_reader == null)
                return false;

            Current = _reader.ReadLine();
            if (Current != null)
                return true;

            Dispose();

            return false;
        }

        protected override void Dispose(bool disposing)
        {
            try
            {
                if (!disposing) return;

                _reader?.Dispose();
            }
            finally
            {
                _reader = null;
                base.Dispose(disposing);
            }
        }
    }
}