﻿// ReSharper disable UnusedMember.Global
//
// Fix the bugs in this piece of code:
//
// public static double Average(int a, int b)
// {
//      return a + b / 2;
// }

using System;

namespace Testbed._a_Algorithms.MathematicCaveats
{
    static public class AveragePitfall
    {
        static public void Tester()
        {
            var average0 = Average(0, 1);
            var success0 = Math.Abs(average0 - 0.5) < 0.001;

            var average1 = Average(2, 1);
            var success1 = Math.Abs(average1 - 1.5) < 0.001;

            var average2 = Average(2, 2);
            var success2 = Math.Abs(average2 - 2) < 0.001;

            var average3 = Average(int.MaxValue, int.MaxValue);
            var success3 = Math.Abs(average3 - int.MaxValue) < 0.001;

            var average4 = Average(int.MinValue, int.MinValue);
            var success4 = Math.Abs(average4 - int.MinValue) < 0.001;

            var average5 = Average(int.MaxValue, -int.MaxValue);
            var success5 = Math.Abs(average5 - 0) < 0.001;

            var average6 = Average(-int.MaxValue, int.MaxValue);
            var success6 = Math.Abs(average6 - 0) < 0.001;

            var average7 = Average(0, 0);
            var success7 = Math.Abs(average7 - 0) < 0.001;

            var average8 = Average(0, int.MaxValue);
            var success8 = Math.Abs(average8 - ((double) int.MaxValue / 2)) < 0.001;
        }

        static public double Average(int a, int b)
        {
            return (a / 2) + (b / 2) + (((double)((a % 2) + (b % 2))) / 2); //0

            //0 we need to dodge the overflow bullet
        }
    }
}


