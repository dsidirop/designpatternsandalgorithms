﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.EditDistanceDPSmartTabulizationApproach
{
    [TestClass]
    public class EditDistanceDPSmartTabulizationApproachTestbedRed
    {
        [TestMethod]
        [DataRow(null, "")]
        [DataRow("", null)]
        public void ShouldThrowArgumentExceptionGivenNullArray(string s1, string s2)
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => EditDistanceDpSmartTabulizationApproach.Calculate(s1, s2));

            //Assert
            action.Should().ThrowExactly<ArgumentNullException>().WithMessage("*'s?'*");
        }
    }
}
