﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.MinPriorityQueue
{
    [TestClass]
    public class MinPriorityQueueFromArrayTestbedPeek
    {
        [TestMethod]
        public void ShouldReturnNullGivenEmptyHeap()
        {
            //Arrange
            var queue = new MinPriorityQueueFromArray<int>();

            //Act
            var action = new Action(() => queue.Peek());

            //Assert
            action.Should().ThrowExactly<InvalidOperationException>();
        }

        [TestMethod]
        public void ShouldReturnSingleItemGivenSingleItemHeap()
        {
            //Arrange
            const int expected = 1;
            var queue = new MinPriorityQueueFromArray<int>();
            queue.Insert(expected);

            //Act
            var result = queue.Peek();

            //Assert
            result.Should().Be(expected);
        }

        [TestMethod]
        public void ShouldReturnMinItemGivenFullHeap()
        {
            //Arrange
            const int expected = 1;
            var queue = new MinPriorityQueueFromArray<int>();
            queue.Insert(10);
            queue.Insert(20);
            queue.Insert(30);
            queue.Insert(expected);

            //Act
            var result = queue.Peek();

            //Assert
            result.Should().Be(expected);
        }
    }
}