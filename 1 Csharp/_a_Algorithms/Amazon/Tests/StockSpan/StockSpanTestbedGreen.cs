﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.StockSpan
{
    [TestClass]
    public class NextGreaterElementTestbedGreen
    {
        [TestMethod]
        public void ShouldReturnEmptyArrayGivenEmptyStockPricesArray()
        {
            //Arrange
            var stockPricesForEachDay = new int[] { };

            //Act
            var results = Amazon.StockSpan.Calculate(stockPricesForEachDay);

            //Assert
            results.Should().BeEmpty();
        }

        [TestMethod]
        public void ShouldReturnSingleElementArrayGivenSinglePriceArray()
        {
            //Arrange
            var stockPricesForEachDay = new[] { 1 };
            var expectedStockPricesSpans = new[] { 1 };

            //Act
            var results = Amazon.StockSpan.Calculate(stockPricesForEachDay);

            //Assert
            results.Should().BeEquivalentTo(expectedStockPricesSpans);
        }

        [TestMethod]
        [DataRow(new[] { 1, 2 }, new[] { 1, 2 })]
        [DataRow(new[] { 100, 80, 60, 70, 60, 75, 85 }, new[] { 1, 1, 1, 2, 1, 4, 6 })]
        [DataRow(new[] { 100, 80, 60, 50, 40, 30, 20 }, new[] { 1, 1, 1, 1, 1, 1, 1 })]
        public void ShouldReturnProperResultGivenMultiPriceArray(int[] stockPricesForEachDay, int[] expectedStockPricesSpans)
        {
            //Arrange
            //...

            //Act
            var results = Amazon.StockSpan.Calculate(stockPricesForEachDay);

            //Assert
            results.Should().BeEquivalentTo(expectedStockPricesSpans);
        }
    }
}
