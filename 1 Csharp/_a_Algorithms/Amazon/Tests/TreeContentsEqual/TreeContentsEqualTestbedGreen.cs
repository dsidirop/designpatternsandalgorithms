﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace Testbed._a_Algorithms.Amazon.Tests.TreeContentsEqual
{
    [TestClass]
    public class TreeContentsEqualTestbedGreen
    {
        [TestMethod]
        [DataRow(new int[] { }, new int[] { })]
        [DataRow(new[] { 1 }, new[] { 1 })]
        [DataRow(new[] { 2, 1 }, new[] { 2, 1 })]
        [DataRow(new[] { 2, 2 }, new[] { 2, 2 })]
        [DataRow(new[] { 2, 3, 1 }, new[] { 2, 1, 3 })]
        [DataRow(new[] { 2, 3, 1, 2 }, new[] { 2, 1, 3, 2 })]
        public void ShouldReportTrueGivenTreesWithSameContents(in int[] a, in int[] b)
        {
            //Arrange
            var treeA = a.ToBalancedTree();
            var treeB = b.ToBalancedTree();

            //Act
            var result = Amazon.TreeContentsEqual.Calculate(treeA, treeB);

            //Assert
            result.Should().Be(true);
        }

        [TestMethod]
        [DataRow(new int[] { }, new int[] { 1 })]
        [DataRow(new[] { 1 }, new[] { 2 })]
        [DataRow(new[] { 1, 2 }, new[] { 2 })]
        [DataRow(new[] { 2, 1 }, new[] { 2, 2 })]
        [DataRow(new[] { 2, 3, 1 }, new[] { 2, 1, 4 })]
        public void ShouldReportFalseGivenTreesWithDifferentContents(in int[] a, in int[] b)
        {
            //Arrange
            var treeA = a.ToBalancedTree();
            var treeB = b.ToBalancedTree();

            //Act
            var result = Amazon.TreeContentsEqual.Calculate(treeA, treeB);

            //Assert
            result.Should().Be(false);
        }

        [TestMethod]
        public void ShouldReportTrueGivenHugeTreesWithSameContents()
        {
            //Arrange
            var treeA = Enumerable.Range(0, 30).ToBalancedTree();
            var treeB = Enumerable.Range(0, 30).Reverse().ToBalancedTree();

            //Act
            var result = Amazon.TreeContentsEqual.Calculate(treeA, treeB);

            //Assert
            result.Should().Be(true);
        }
    }

    static internal class Ux
    {
        static public Amazon.TreeContentsEqual.TreeNode ToBalancedTree(this IEnumerable<int> a)
        {
            if (!(a?.Any() ?? false))
                return null;

            var queue = new Queue<Amazon.TreeContentsEqual.TreeNode>();
            queue.Enqueue(new Amazon.TreeContentsEqual.TreeNode
            {
                Data = a.FirstOrDefault()
            });

            var root = queue.Peek();
            foreach (var x in a.Skip(1))
            {
                var node = queue.Peek();
                if (node.Left != null && node.Right != null)
                {
                    queue.Dequeue();
                    node = queue.Peek();
                }

                var newNode = new Amazon.TreeContentsEqual.TreeNode {Data = x};
                queue.Enqueue(newNode);
                if (node.Left == null)
                {
                    node.Left = newNode;
                }
                else
                {
                    node.Right = newNode;
                }
            }

            return root;
        }
    }
}

