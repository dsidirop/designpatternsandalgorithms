﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Testbed._a_Algorithms.DiscoverAllDisconnectedFriendNetworkGraphs.Tests
{
    static public class UShuffleX
    {
        static public IEnumerable<TSource> Shuffle<TSource>(this IEnumerable<TSource> source)
        {
            var rng = new Random();

            int RandomKeySelector(TSource a) => rng.Next();

            return source.OrderBy(RandomKeySelector);
        }
    }
}