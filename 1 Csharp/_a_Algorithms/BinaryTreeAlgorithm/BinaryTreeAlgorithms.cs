﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Testbed._a_Algorithms.BinaryTreeAlgorithm
{
    static public class BinaryTreeAlgorithms
    {
        static public void Tester()
        {
            var subnode1 = new Node { Data = 2 };
            var subnode2 = new Node { Data = 4 };
            var root = new Node { Data = 3, Left = subnode1, Right = subnode2 };

            var result1 = IsValidBinarySearchTree(root);

            InorderMorrisTraversal(root);
        }

        static public bool IsValidBinarySearchTree(Node currentNode) //0
        {
            if (currentNode == null) return false;

            var last = (Node)null;
            var stack = new Stack<Node>(128);
            while (stack.Any() || currentNode != null)
            {
                if (currentNode != null)
                {
                    stack.Push(currentNode);
                    currentNode = currentNode.Left;
                }
                else
                {
                    currentNode = stack.Pop();
                    if (last != null && currentNode.Data <= last.Data) //0
                        return false;

                    last = currentNode;
                    currentNode = currentNode.Right;
                }
            }

            return true;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////

        static public void InorderMorrisTraversal(Node root)
        {
            if (root == null) return;

            var current = root;
            while (current != null)
            {
                if (current.Left == null)
                {
                    Debug.WriteLine(current.Data);
                    current = current.Right;
                }
                else
                {
                    var pre = current.Left;
                    while (pre.Right != null && pre.Right != current)
                    {
                        pre = pre.Right;
                    }

                    //pre = predecessor of current node
                    if (pre.Right == null) //make the link
                    {
                        pre.Right = current;
                        current = current.Left;
                    }
                    else //Break the link
                    {
                        pre.Right = null;
                        Debug.WriteLine(current.Data);
                        current = current.Right;
                    }
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////
    }
    //0 complexity is O(n) since we visit each and every node
    //1 the definition of bst is that it is a ordered set thus duplicates are not allowed to be inserted

    public class Node
    {
        public int Data;
        public Node Left { get; set; }
        public Node Right { get; set; }
    }
}