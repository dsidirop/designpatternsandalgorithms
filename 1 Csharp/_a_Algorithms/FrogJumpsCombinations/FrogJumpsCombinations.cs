﻿using System;
using System.Linq;

namespace Testbed._a_Algorithms.FrogJumpsCombinations
{
    static public class FrogJumpsCombinations
    {
        static public void NumberOfWaysTester()
        {
            var result = NumberOfWays(6);
        }

        static public double NumberOfWays(int distance)
        {
            if (distance <= 0) return 0;

            var min = -distance;
            var max = Math.Floor(((double)-distance) / 2);
            var totalMovementCombinations = (double)0;
            for (var t = min; t <= max; t++)
            {
                var countMove = -distance - 2 * t;
                var countJump = +distance + 1 * t;

                var combinations = countMove == 0 || countJump == 0 ? 1 : Factorial(countMove + countJump) / (Factorial(countMove) * Factorial(countJump));
                totalMovementCombinations += combinations;
            }

            return totalMovementCombinations;
        }

        static public double Factorial(int i)
        {
            if (i < 0)
                return -1;

            if (i == 0 || i == 1)
                return 1;

            return Enumerable
                .Range(start: 1, count: i - 1)
                .Aggregate( //overflowing is a problem here for sure
                    seed: (double) 1,
                    func: (current, x) => current * x
                );
        }

        // We define a diophantine equation of the form
        //
        // 1*CountMove + 2*CountJump = TotalDistance
        //
        // We immediately see that GCD(1,2)=1 (Grant Common Denominator) Its apparent that any integer value of TotalDistance will be divisible by GCD(1,2)=1
        // Thus the equation has integral solutions for all values of TotalDistance   An apparent special Solution for the equation is
        //
        // (CountMove[0], CountJump[0]) = (-TotalDistance, +TotalDistance)
        //
        // Thus the family of integral solutions of the equation are given by the following tuple of equations (t is on Z space):
        //
        // CountMove = CountMove[0] - 2*t   <=>   CountMove = -TotalDistance - 2*t
        // CountJump = CountJump[0] + 1*t   <=>   CountJump = +TotalDistance + 1*t
        //
        // We enforce CountMove >= 0 & CountJump >= 0 on the above tuple of equations and we derive that:
        //
        // -TotalDistance <= t <= Math.Floor(-TotalDistance/2)
        //
        // With that we can derive the entire family of K=TotalDistance-Math.Floor(TotalDistance/2) solutions (CountMove,CountJump)
        // For each pair (CountMove[i],CountJump[i]) the number of permutations between moves and jumps is given by
        //
        // MovementCombination[i] = [ CountMove[i] + CountJump[i] ] ! / [ CountMove[i]! * CountJump[i]! ] the exclamation mark stands for factorial
        //
        // The total number of possible movements is thus
        //
        // Sum[ MovementCombination[i] ] where i is an integer getting all its values from space (1, TotalDistance-Math.Floor(TotalDistance/2))
    }
}
