﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.RearrangeArrayAlternateSorting
{
    [TestClass]
    public class RearrangeArrayAlternateSortingTestbedGreen
    {
        [TestMethod]
        [DataRow(new[] {1}, new[] {1})]
        [DataRow(new[] {1, 2}, new[] {2, 1})]
        [DataRow(new[] {1, 2, 3}, new[] {3, 1, 2})]
        [DataRow(new[] {1, 2, 3, 4}, new[] {4, 1, 3, 2})]
        [DataRow(new[] {1, 2, 3, 4, 5}, new[] {5, 1, 4, 2, 3})]
        [DataRow(new[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, new[] {10, 1, 9, 2, 8, 3, 7, 4, 6, 5})]
        [DataRow(new[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, new[] {11, 1, 10, 2, 9, 3, 8, 4, 7, 5, 6})]
        [DataRow(new[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}, new[] {12, 1, 11, 2, 10, 3, 9, 4, 8, 5, 7, 6})]
        public void ShouldCorrectMissingNumberGivenValidArray(ref int[] input, in int[] expectedOutput)
        {
            //Arrange
            //...

            //Act
            Amazon.RearrangeArrayAlternateSorting.Calculate(ref input);

            //Assert
            input.Should().BeEquivalentTo(expectedOutput, options => options.WithStrictOrdering());
        }
    }
}
