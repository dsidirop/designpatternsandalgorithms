﻿// https://www.geeksforgeeks.org/the-stock-span-problem/
//
// Assuming we have a series of n daily price quotes for a stock calculate the daily span of said stock
// price for each and everyone of the given n days
//
// Footnotes:
//
// The span S[i] of the stocks price on a given day [i] is defined as the maximum number of consecutive
// days just before the given day for which the price of the stock on the current day is less than or
// equal to its price on the given day
//
// For example if an array of 7 days prices is given as
//
//      {100, 80, 60, 70, 60, 75, 85}
//
// then the span values for corresponding 7 days are
//
//      {1, 1, 1, 2, 1, 4, 6}
//
// Optimum Solution Analysis
//
//      https://youtu.be/-IFmgue8sF0?t=470
//
// The core concept is that we employ a stack to track down the local maximums while we iterate the
// given array forward
//
//      Space O(N)   in the worst case scenario where the stock prices are in descending order
//      Time  0(N)   its O(2N) actually which gets simplified to O(N)
//
//
// Footnote   This problem can be converted to the "next greater element" problem

using System;
using System.Collections.Generic;
using System.Linq;

namespace Testbed._a_Algorithms.Amazon
{
    static public class StockSpan
    {
        static public int[] Calculate(int[] stockPricesPerDay)
        {
            if (stockPricesPerDay == null)
                throw new ArgumentException(nameof(stockPricesPerDay));

            var length = stockPricesPerDay.Length;
            if (length == 0)
                return new int[] { };

            var resultingSpans = new List<int>(stockPricesPerDay.Length / 4) { 1 };
            if (length == 1)
                return resultingSpans.ToArray();

            var indexesOfLocalSalients = new Stack<int>(new[] { 0 });
            for (var i = 1; i < length; i++)
            {
                for (
                    var current = stockPricesPerDay[i];
                    indexesOfLocalSalients.Any() && stockPricesPerDay[indexesOfLocalSalients.Peek()] < current;
                    indexesOfLocalSalients.Pop()
                )
                {
                }

                var span = indexesOfLocalSalients.Any()
                    ? i - indexesOfLocalSalients.Peek()
                    : i + 1;

                resultingSpans.Add(span);
                indexesOfLocalSalients.Push(i);
            }

            return resultingSpans.ToArray();
        }
    }
}
