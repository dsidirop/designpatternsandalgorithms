﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.SubarrayWithGivenSum
{
    [TestClass]
    public class CountTheTripletsTestbedRed
    {
        [TestMethod]
        public void ShouldThrowNullArgumentExceptionGivenNullInputArray()
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => Amazon.SubarrayWithGivenSum.Calculate(null, 1));

            //Assert
            action.Should().ThrowExactly<ArgumentNullException>().WithMessage("*'input'*");
        }

        [TestMethod]
        public void ShouldThrowArgumentExceptionGivenEmptyInputArray()
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => Amazon.SubarrayWithGivenSum.Calculate(new int[] { }, 1));

            //Assert
            action.Should().ThrowExactly<ArgumentException>().WithMessage("*'input'*");
        }

        [TestMethod]
        [DataRow(0)]
        [DataRow(-1)]
        public void ShouldThrowArgumentExceptionGivenNegativeOrZeroTargetSum(int targetSum)
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => Amazon.SubarrayWithGivenSum.Calculate(new[] { 1 }, targetSum));

            //Assert
            action.Should().ThrowExactly<ArgumentException>().WithMessage("*'targetSum'*");
        }
    }
}
