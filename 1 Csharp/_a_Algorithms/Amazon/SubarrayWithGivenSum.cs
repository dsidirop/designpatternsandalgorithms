﻿//
// https://practice.geeksforgeeks.org/problems/subarray-with-given-sum/0
// https://www.youtube.com/watch?v=z__1ys07eKo
//
// Given an unsorted array A of size N of nonnegative integers find a continuous subarray which adds to a given number S
//
// Input
//
// The first line of input contains an integer T denoting the number of test cases  Then T test cases follow   Each test case
// consists of two lines  The first line of each test case is S where S is the desired sum  The second line of each test case
// contains N space separated integers denoting the array elements
//
// Output
//
// For each testcase in a new line print the starting position (0 indexing) and length of the first such occuring subarray from
// the left if sum equals to subarray else print -1
//
// Constraints
//
//    1 <= T <= 100
//    1 <= N <= 107
//    1 <= Ai <= 1010
//

using System;
using System.Linq;
using Ardalis.GuardClauses;

namespace Testbed._a_Algorithms.Amazon
{
    public class SubarrayWithGivenSum
    {
        static public void Mainn()
        {
            var firstLine = Console.ReadLine();
            if (!decimal.TryParse(firstLine, out var targetSum))
            {
                Console.Write($@"** Error: '{firstLine}' is not a valid number ");
            }

            var secondLine = Console.ReadLine();
            var numbers = secondLine.Split(new[] {@" "}, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToArray();

            var results = Calculate(numbers, targetSum);

            Console.WriteLine(results.Index + " " + results.Length);
        }

        static public (int Index, int Length) Calculate(in int[] input, in decimal targetSum)
        {
            Guard.Against.Null(input, nameof(input));
            Guard.Against.NegativeOrZero(input.Length, nameof(input));
            Guard.Against.NegativeOrZero(targetSum, nameof(targetSum));

            var length = input.Length;
            var sum = (long)0; //0 vital
            var startIndex = 0;
            for (var i = 0; i < length; ++i)
            {
                var current = input[i];

                sum += current;
                if (sum == targetSum) //order
                {
                    return (startIndex, i - startIndex + 1);
                }

                if (current == targetSum) //order   jackpot case   keep it here
                {
                    return (i, 1);
                }

                while (sum > targetSum && startIndex <= i)
                {
                    sum -= input[startIndex]; //order
                    ++startIndex; //order
                    if (sum == targetSum) //order
                    {
                        return (startIndex, i - startIndex + 1);
                    }
                }
            }

            return (-1, -1);

            //0 we use long to guard against overflowing
        }
    }
}