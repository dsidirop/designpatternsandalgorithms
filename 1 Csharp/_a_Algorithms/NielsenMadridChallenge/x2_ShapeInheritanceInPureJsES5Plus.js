﻿// Show how we can implement a Shape base class in pure javascript which includes a single method GetNumberOfEdges()
// which as the name implies returns the number of edges in a shape   then implement this interface in two subclasses
// Pentagon and Triangle
//
// note: this particular implementation uses classes which where introduced in ECMAScript5
//
// https://www.sitepoint.com/javascript-private-class-fields/
//

export class ΙShape {
    // defining the method as an arrow => function has some subtle advantages in certain
    // contexts such as react    the point is that when we invoke the instance like so:
    //
    //      var foo = new IShape().GetNumberOfEdges;
    //      foo();  <--- 'this' will defined here thanks to the arrow function impl
    //
    // 'this' will keep on pointing to the container instance. If we had implemented the
    //  method in the classical way then this would be inside foo() above
    //
    GetNumberOfEdges = () => {
        // console.log(this);
        return -1;
    }

    get Point() {
        return ``;
    }

    get X() {
        return -1;
    }

    get Y() {
        return -1;
    }

    set X(x) {
        return x;
    }

    set Y(y) {
        return y;
    }
}

export default class Pentagon extends ΙShape { //default export
    _x = 0;
    _y = 0;

    constructor(x, y) {
        super(); //vital to call super

        this._x = x;
		this._y = y;
    }

    GetNumberOfEdges = () => {
        return 5;
    }

    get Point() {
        return `(${_x}, ${_y})`;
    }

    set X(x) {
        return this._x = x;
    }

    set Y(y) {
        return this._y = y;
    }

    get X() {
        return this._x;
    }

    get Y() {
        return this._y;
    }

    static SomeStaticMethod(foo, bar) {
        return foo + bar;
    }
}

export class Triangle extends ΙShape { //non default export
    _x = 0;
    _y = 0;

    constructor(x, y) {
        super(); //vital to call super

        this._x = x;
        this._y = y;
    }

    GetNumberOfEdges = () => {
        return 3;
    }

    get Point() {
        return `(${_x}, ${_y})`;
    }

    set X(x) {
        return this._x = x;
    }

    set Y(y) {
        return this._y = y;
    }

    get X() {
        return this._x;
    }

    get Y() {
        return this._y;
    }
}

