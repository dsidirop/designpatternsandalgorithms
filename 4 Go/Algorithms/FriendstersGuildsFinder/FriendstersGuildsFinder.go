package FriendstersGuildsFinder

import "github.com/dsidirop/go-algorithms/FriendstersGuildsFinder/Individual"

type Engine struct {
	allIndividuals map[string]Individual.IIndividual
}

type IEngine interface {
	Run() [][]string
	InitGraph(friendRelations []Friendship) IEngine
}

func SpawnFriendstersGuildsFinder() IEngine {
	return &Engine{
		allIndividuals: make(map[string]Individual.IIndividual),
	}
}

type Friendship struct {
	a, b string
}

func (e *Engine) Run() [][]string {
	panic("implement me")
}

func (e *Engine) InitGraph(friendRelations []Friendship) IEngine {
	for _, r := range friendRelations {
		individualA := e.ensureIndividualRegistered(r.a)
		individualB := e.ensureIndividualRegistered(r.b)

		individualA.EnsureFriendship(individualB)
		individualB.EnsureFriendship(individualA)
	}

	return e
}

func (e *Engine) ensureIndividualRegistered(name string) Individual.IIndividual {
	individual, alreadyExists := e.allIndividuals[name]
	if !alreadyExists {
		individual = Individual.Spawn(name)
		e.allIndividuals[name] = individual
	}

	return individual
}
