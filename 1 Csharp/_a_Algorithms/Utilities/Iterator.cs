﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Testbed._a_Algorithms.Utilities
{
    // abstract iterator borrowed from linq  Used in anticipation of need for similar enumerables in the future
    public abstract class Iterator<TSource> : IEnumerable<TSource>, IEnumerator<TSource>
    {
        private readonly int _threadId;

        protected int State { get; set; }

        protected Iterator()
        {
            _threadId = Thread.CurrentThread.ManagedThreadId;
        }

        public TSource Current { get; protected set; }
        object IEnumerator.Current => Current;
        void IEnumerator.Reset() => throw new NotSupportedException();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        protected abstract Iterator<TSource> Clone();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            State = -1;
            Current = default;
        }

        public IEnumerator<TSource> GetEnumerator()
        {
            if (_threadId == Thread.CurrentThread.ManagedThreadId && State == 0)
            {
                State = 1;
                return this;
            }

            var duplicate = Clone();

            duplicate.State = 1;
            return duplicate;
        }

        public abstract bool MoveNext();
    }
}