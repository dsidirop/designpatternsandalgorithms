﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Globalization;
using System.Linq;
using TrainArrivalAndDeparture = Testbed._a_Algorithms.Amazon.MinimumNumberOfPlatforms.TrainArrivalAndDeparture;

namespace Testbed._a_Algorithms.Amazon.Tests.MinimumNumberOfPlatforms
{
    [TestClass]
    public class MinimumNumberOfPlatformsTestbedGreen
    {
        [DataTestMethod]
        public void ShouldReturnZeroGivenNoSchedules()
        {
            //Arrange
            //...

            //Act
            var results = Amazon.MinimumNumberOfPlatforms.Calculate(new TrainArrivalAndDeparture[] { });

            //Assert
            results.Should().Be(0);
        }

        [DataTestMethod]
        [DataRow(new[] {@"2010/01/10 10:00,2010/01/10 10:30"})]
        [DataRow(new[] {@"2010/01/10 10:00,2010/01/10 10:30", @"2010/01/10 10:35,2010/01/10 11:30", @"2010/01/10 11:35,2010/01/10 12:00"})]
        public void ShouldReturnOneGivenScheduleWithNoOverlaps(in string[] input)
        {
            //Arrange
            var schedule = input
                .Select(x => x.Split(new[] {@","}, StringSplitOptions.None))
                .Select(x => new TrainArrivalAndDeparture(ParseDate(x[0]), ParseDate(x[1])))
                .ToArray();

            //Act
            var results = Amazon.MinimumNumberOfPlatforms.Calculate(schedule);

            //Assert
            results.Should().Be(1);
        }

        [DataTestMethod]
        [DataRow(
            new[]
            {
                @"2010/01/10 10:00,2010/01/10 10:30",
                @"2010/01/10 10:30,2010/01/10 11:30"
            },
            2
        )]
        [DataRow(
            new[]
            {
                @"2010/01/10 10:00,2010/01/10 10:30",
                @"2010/01/10 10:30,2010/01/10 11:30",
                @"2010/01/10 11:30,2010/01/10 12:00"
            },
            2
        )]
        [DataRow(
            new[]
            {
                @"2010/01/10 10:00,2010/01/10 10:30",
                @"2010/01/10 10:00,2010/01/10 10:30",
                @"2010/01/10 10:00,2010/01/10 10:30"
            },
            3
        )]
        [DataRow(
            new[]
            {
                @"2010/01/10 10:00,2010/01/10 10:30",
                @"2010/01/10 10:10,2010/01/10 10:20",
                @"2010/01/10 10:15,2010/01/10 10:30"
            },
            3
        )]
        [DataRow(
            new[]
            {
                @"2010/01/10 09:00,2010/01/10 09:30",
                @"2010/01/10 09:10,2010/01/10 09:20",
                @"2010/01/10 10:00,2010/01/10 10:30",
                @"2010/01/10 10:10,2010/01/10 10:20",
                @"2010/01/10 10:15,2010/01/10 10:30"
            },
            3
        )]
        public void ShouldReturnCorrectNumberOfPlatformsGivenSchedulesWithOverlaps(in string[] input, in int expectedNumberOfPlatforms)
        {
            //Arrange
            var schedule = input
                .Select(x => x.Split(new[] {@","}, StringSplitOptions.None))
                .Select(x => new TrainArrivalAndDeparture(ParseDate(x[0]), ParseDate(x[1])))
                .ToArray();

            //Act
            var results = Amazon.MinimumNumberOfPlatforms.Calculate(schedule);

            //Assert
            results.Should().Be(expectedNumberOfPlatforms);
        }

        static private DateTime ParseDate(in string dateString) => DateTime.ParseExact(dateString, @"yyyy'/'MM'/'dd HH':'mm", CultureInfo.InvariantCulture);
    }
}
