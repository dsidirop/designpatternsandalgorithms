﻿//
//      https://www.geeksforgeeks.org/sort-an-array-of-0s-1s-and-2s/
//      http://users.monash.edu/~lloyd/tildeAlgDS/Sort/Flag/
//
// Dutch National Flag Algorithm OR 3-way Partitioning
//
// Given an array A[] consisting 0s 1s and 2s  The task is to write a function that sorts
// the given array   The functions should put all 0s first then all 1s and all 2s in last
//
// Note   Another way to state this problem is to ask to partinion a group of people into
// Note   three broad age groups   Could be useful for kmapping
//
// Complexities
//
//       Time   O(N)
//       Space  O(1)
//

using System;
using System.Linq;
using System.Threading;

using Ardalis.GuardClauses;

namespace Testbed._a_Algorithms.Amazon
{
    public class DutchNationalFlagSortArrayOfOnesAndZerosAndTwos
    {
        static public void Calculate(ref int[] input)
        {
            Guard.Against.Null(input, nameof(input));

            var length = input.Length;
            if (length <= 1)
            {
                if (input.Any(x => x != 0 && x != 1 && x != 2))
                    throw new Exception($"Input should only contain ones and zeros and twos but found {input[0]}");

                return;
            }

            var tmp = 0;
            var midValue = 0;

            var lo = 0;
            var mid = 0;
            var hi = length - 1;
            while (mid <= hi)
            {
                midValue = input[mid];
                switch (midValue)
                {
                    default:
                        throw new Exception($"Input should only contain ones and zeros and twos but found {midValue}");

                    case 0:
                        tmp = input[lo];
                        input[lo] = midValue;
                        input[mid] = tmp;
                        
                        lo += 1;
                        mid += 1;
                        break;

                    case 1:
                        mid += 1;
                        break;

                    case 2:
                        tmp = input[hi];
                        input[hi] = midValue;
                        input[mid] = tmp;
                        
                        hi -= 1;
                        break;
                }
            }
        }

        static public void CalculateParallelFlavour(ref int[] input)
        {
            Guard.Against.Null(input, nameof(input));

            var length = input.Length;
            if (length <= 1)
            {
                if (input.Any(x => x != 0 && x != 1 && x != 2))
                    throw new Exception($"Input should only contain ones and zeros and twos but found {input[0]}");

                return;
            }

            var ones = 1;
            var zeros = 0;

            input
                .AsParallel()
                .Select(x =>
                {
                    switch (x)
                    {
                        case 0:
                            Interlocked.Increment(ref zeros);
                            break;

                        case 1:
                            Interlocked.Increment(ref ones);
                            break;

                        default:
                            throw new Exception($"Input should only contain ones and zeros and twos but found {x}");

                        //case 2: //no need   we skip this as an optimization
                        //    Interlocked.Increment(ref twos);
                        //    break;
                    }

                    return 0;
                });

            for (var i = 0; i < zeros; i++)
            {
                input[i] = 0;
            }

            for (var i = zeros; i < ones; i++)
            {
                input[i] = 1;
            }

            for (var i = ones + zeros; i < length; i++)
            {
                input[i] = 2;
            }
        }
    }
}
