﻿// ReSharper disable UnusedMember.Global
//
// Write a function which given two integer non negative numbers A and B
// will return the number of acebits in their product A*B

using System;
using System.Linq;

namespace Testbed._a_Algorithms.Amazon
{
    static public class CountAceBitsInProduct
    {
        static public void Tester()
        {
            var success1 = false;
            var success2 = false;
            var success3 = false;

            {
                var a = int.MaxValue;
                var b = int.MaxValue;
                var expectedCount = AlternativeSlowSolution(a, b);

                var onesCount = Solution(a, b);

                success1 = onesCount == expectedCount;
            }

            {
                var a = int.MaxValue;
                var b = int.MaxValue;
                var expectedCount = AlternativeSlowSolution(a, b);

                var onesCount = Solution(a, b);

                success2 = onesCount == expectedCount;
            }

            {
                var a = 100000000;
                var b = 100000000;
                var expectedCount = AlternativeSlowSolution(a, b);

                var onesCount = Solution(a, b);

                success3 = onesCount == expectedCount;
            }
        }

        static public int Solution(int a, int b)
        {
            if (a < 0)
                throw new ArgumentException(nameof(a));

            if (b < 0)
                throw new ArgumentException(nameof(b));

            if (a == 0
                || b == 0)
                return 0;

            var product = (long)a * b; //0

            var count = 0;
            while (product > 0)
            {
                count += (int) (product & 1); //zero or one
                product >>= 1;
            }

            return count;

            //0 its vital to cast the first int to a long so as to ensure that the resulting type will
            //  be a long that can withstand the worst case scenario maxint x maxint without overflowing
        }

        static public int AlternativeSlowSolution(int a, int b) //0
        {
            return Convert.ToString((long) a * b, 2).Count(x => x == '1');

            //0 this does the trick but its not as efficient!   still useful for proofchecking the fast solution though!
        }
    }
}