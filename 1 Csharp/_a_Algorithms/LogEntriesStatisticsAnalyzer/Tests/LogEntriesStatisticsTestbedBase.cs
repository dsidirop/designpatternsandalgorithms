﻿using System;
using System.Net;

namespace Testbed._a_Algorithms.LogEntriesStatisticsAnalyzer.Tests
{
    public abstract class LogEntriesStatisticsTestbedBase
    {
        private const string Tab = "\t";
        private const string Url = @"http://something.com/foo/bar";

        static protected string SpawnLogEntry(int hour, HttpStatusCode statusCode)
            => string.Format($@"01/01/2000{Tab}{hour:D2}:00:00{Tab}{Url}{Tab}{(int)statusCode}");
    }
}
