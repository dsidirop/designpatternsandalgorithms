﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.IO.Abstractions;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Testbed._a_Algorithms.LogEntriesStatisticsAnalyzer.Tests
{
    [TestClass]
    public class LogEntriesStatisticsGivenCancellationOrderShould : LogEntriesStatisticsTestbedBase
    {
        [TestMethod]
        public async Task ReturnNullResultsGivenCancellationTokenSetImmediately()
        {
            var tempFilePath = Path.GetTempFileName();
            try
            {
                //Arrange
                using var tokenSource = new CancellationTokenSource();

                await File.WriteAllTextAsync(tempFilePath, @"", Encoding.ASCII);

                //Act
                tokenSource.Cancel();
                var analysis = await new LogEntriesStatistics(new FileSystem()).CalculateErrorRatesAsync(
                    filepath: tempFilePath,
                    tokenSource: tokenSource
                );

                //Assert
                analysis.Should().BeNull();
            }
            finally //Cleanup
            {
                if (!string.IsNullOrWhiteSpace(tempFilePath))
                {
                    File.Delete(tempFilePath);
                }
            }
        }

        
        [TestMethod]
        public async Task ReturnNullResultsGivenCancellationTokenSetAfterSmallDelay()
        {
            var tempFilePath = Path.GetTempFileName();
            try
            {
                //Arrange
                using var tokenSource = new CancellationTokenSource();

                await File.WriteAllTextAsync(tempFilePath, @"", Encoding.ASCII);

                const int totalLogEntries = 1_000_000;
                await using (var fileStream = new FileStream(tempFilePath, FileMode.Create))
                await using (var streamWriter = new StreamWriter(fileStream, Encoding.ASCII))
                {
                    for (var i = 0ul; i < totalLogEntries; i++)
                    {
                        await streamWriter.WriteLineAsync(SpawnLogEntry(hour: 1, statusCode: HttpStatusCode.OK));

                        if (i % 10_000 == 0)
                        {
                            await streamWriter.FlushAsync();
                        }
                    }

                    await streamWriter.FlushAsync();
                }

                //Act
                tokenSource.CancelAfter(50);
                var analysis = await new LogEntriesStatistics(new FileSystem()).CalculateErrorRatesAsync(
                    filepath: tempFilePath,
                    tokenSource: tokenSource
                );

                //Assert
                analysis.Should().BeNull();
            }
            finally //Cleanup
            {
                if (!string.IsNullOrWhiteSpace(tempFilePath))
                {
                    File.Delete(tempFilePath);
                }
            }
        }
    }
}
