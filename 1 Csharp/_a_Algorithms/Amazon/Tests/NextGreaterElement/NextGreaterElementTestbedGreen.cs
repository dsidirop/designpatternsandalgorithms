﻿using System;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.NextGreaterElement
{
    [TestClass]
    public class NumberOfPerfectSquaresTestbedGreen
    {
        [TestMethod]
        public void ShouldReturnEmptyArrayGivenEmptyStockPricesArray()
        {
            //Arrange
            var input = new int[] { };

            //Act
            var results = Amazon.NextGreaterElement.Calculate(input);

            //Assert
            results.Should().BeEmpty();
        }

        [TestMethod]
        public void ShouldReturnSingleElementArrayGivenSinglePriceArray()
        {
            //Arrange
            var input = new[] {10};
            var expectedResults = new[]
            {
                new Amazon.NextGreaterElement.Result {Index = 0, Value = 10, GneIndex = null, GneValue = null}
            };

            //Act
            var results = Amazon.NextGreaterElement.Calculate(input);

            //Assert
            results.Should().BeEquivalentTo(expectedResults);
        }

        [DataTestMethod]
        public void ShouldReturnProperResultGivenMultiPriceArray1()
        {
            //Arrange
            var input = new[] {4, 3, 2, 1};
            var expectedResults = new[]
            {
                new Amazon.NextGreaterElement.Result {Index = 0, Value = 4, GneIndex = null, GneValue = null},
                new Amazon.NextGreaterElement.Result {Index = 1, Value = 3, GneIndex = null, GneValue = null},
                new Amazon.NextGreaterElement.Result {Index = 2, Value = 2, GneIndex = null, GneValue = null},
                new Amazon.NextGreaterElement.Result {Index = 3, Value = 1, GneIndex = null, GneValue = null}
            };

            //Act
            var results = Amazon.NextGreaterElement.Calculate(input);

            //Assert
            results.Should().BeEquivalentTo(expectedResults);
        }

        [DataTestMethod]
        public void ShouldReturnProperResultGivenMultiPriceArray2()
        {
            //Arrange
            var input = new[] {1, 2, 3, 4};
            var expectedResults = new[]
            {
                new Amazon.NextGreaterElement.Result {Index = 0, Value = 1, GneIndex = 1, GneValue = 2},
                new Amazon.NextGreaterElement.Result {Index = 1, Value = 2, GneIndex = 2, GneValue = 3},
                new Amazon.NextGreaterElement.Result {Index = 2, Value = 3, GneIndex = 3, GneValue = 4},
                new Amazon.NextGreaterElement.Result {Index = 3, Value = 4, GneIndex = null, GneValue = null}
            };

            //Act
            var results = Amazon.NextGreaterElement.Calculate(input);

            //Assert
            results.Should().BeEquivalentTo(expectedResults);
        }

        [DataTestMethod]
        public void ShouldReturnProperResultGivenMultiPriceArray3()
        {
            //Arrange
            var input = new[] {4, 5, 2, 25};
            var expectedResults = new[]
            {
                new Amazon.NextGreaterElement.Result {Index = 0, Value = 4, GneIndex = 1, GneValue = 5},
                new Amazon.NextGreaterElement.Result {Index = 1, Value = 5, GneIndex = 3, GneValue = 25},
                new Amazon.NextGreaterElement.Result {Index = 2, Value = 2, GneIndex = 3, GneValue = 25},
                new Amazon.NextGreaterElement.Result {Index = 3, Value = 25, GneIndex = null, GneValue = null}
            };

            //Act
            var results = Amazon.NextGreaterElement.Calculate(input);

            //Assert
            results.Should().BeEquivalentTo(expectedResults);
        }

        [DataTestMethod]
        public void ShouldReturnProperResultGivenMultiPriceArray4()
        {
            //Arrange
            var input = new[] {4, 4, 4, 4};
            var expectedResults = new[]
            {
                new Amazon.NextGreaterElement.Result {Index = 0, Value = 4, GneIndex = null, GneValue = null},
                new Amazon.NextGreaterElement.Result {Index = 1, Value = 4, GneIndex = null, GneValue = null},
                new Amazon.NextGreaterElement.Result {Index = 2, Value = 4, GneIndex = null, GneValue = null},
                new Amazon.NextGreaterElement.Result {Index = 3, Value = 4, GneIndex = null, GneValue = null}
            };

            //Act
            var results = Amazon.NextGreaterElement.Calculate(input);

            //Assert
            results.Should().BeEquivalentTo(expectedResults);
        }
    }
}
