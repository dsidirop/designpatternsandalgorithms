﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Testbed._a_Algorithms.Utilities;

namespace Testbed._a_Algorithms.ATCom
{
    [TestClass]
    public class MovieNight5IndexArray
    {
        public class Movie
        {
            public DateTime Start { get; private set; }
            public DateTime End { get; private set; }

            public ulong StartMinutes { get; set; }
            public ulong EndMinutes { get; set; }

            public Movie(DateTime start, DateTime end)
            {
                Start = start;
                End = end;

                StartMinutes = (ulong)TimeSpan.FromTicks(start.Ticks).TotalMinutes;
                EndMinutes = (ulong)TimeSpan.FromTicks(end.Ticks).TotalMinutes;

                if (start.Ticks != TimeSpan.FromMinutes(StartMinutes).Ticks)
                    throw new Exception(nameof(start));

                if (end.Ticks != TimeSpan.FromMinutes(EndMinutes).Ticks)
                    throw new Exception(nameof(end));
            }
        }

        static public bool CanViewAll(Movie[] movies)
        {
            var len = movies.Length;
            if (len <= 1)
                return true;

            var gcd = 0UL;
            var min = ulong.MaxValue;
            var max = ulong.MinValue;
            Parallel.Invoke( //126ms vs 196ms of the plain approach
                () => min = movies.Select(m => m.StartMinutes).AsParallel().Min(),
                () => max = movies.Select(m => m.EndMinutes).AsParallel().Max(),
                () => gcd = movies.SelectMany(m => new[] { m.StartMinutes, m.EndMinutes }).AsParallel().Aggregate(GlobalCommonDivisor.Gcd)
            );

            var aux = new int[(max - min) / gcd + 1];
            Parallel.ForEach(
                movies,
                (m, _, __) =>
                {
                    var startIndex = (m.StartMinutes - min) / gcd; // starting point of the interval
                    Interlocked.Increment(ref aux[startIndex]);

                    var endIndex = (m.EndMinutes - min) / gcd; // end point of the interval
                    Interlocked.Decrement(ref aux[endIndex]);
                }
            );

            for (var i = 1; i < aux.Length; i++)
            {
                aux[i] += aux[i - 1]; // calculating the prefix sum
                if (aux[i] > 1) // overlap
                    return false;
            }

            return true; // if we reach here then no overlap
        }

        [TestMethod]
        public void CanViewAllShouldReturnTrueGivenEmptyListOfMovies()
        {
            //Arrange
            var movies1 = new Movie[] { };

            //Act
            var verdict = CanViewAll(movies1);

            //Assert
            verdict.Should().BeTrue();
        }

        [TestMethod]
        public void CanViewAllShouldReturnTrueGivenSingleItemList()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;
            var movies1 = new[]
            {
                new Movie(DateTime.Parse("1/1/2015 20:00", format), DateTime.Parse("1/1/2015 21:30", format))
            };

            //Act
            var verdict = CanViewAll(movies1);

            //Assert
            verdict.Should().BeTrue();
        }

        [TestMethod]
        public void CanViewAllShouldReturnTrueGivenNonOverlappingMovies()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;
            var movies1 = new[]
            {
                new Movie(DateTime.Parse("1/1/2015 20:00", format), DateTime.Parse("1/1/2015 21:30", format)),
                new Movie(DateTime.Parse("1/1/2015 21:30", format), DateTime.Parse("1/1/2015 23:00", format)),
                new Movie(DateTime.Parse("1/1/2015 23:10", format), DateTime.Parse("1/1/2015 23:30", format))
            };

            //Act
            var verdict = CanViewAll(movies1);

            //Assert
            verdict.Should().BeTrue();
        }

        [TestMethod]
        public void CanViewAllShouldReturnFalseGivenOverlappingMovies1()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;
            var movies2 = new[]
            {
                new Movie(DateTime.Parse("1/1/2015 20:00", format), DateTime.Parse("1/1/2015 21:30", format)),
                new Movie(DateTime.Parse("1/1/2015 21:15", format), DateTime.Parse("1/1/2015 21:45", format)), //overlap
                new Movie(DateTime.Parse("1/1/2015 21:30", format), DateTime.Parse("1/1/2015 23:00", format))
            };

            //Act
            var verdict = CanViewAll(movies2);

            //Assert
            verdict.Should().BeFalse();
        }

        [TestMethod]
        public void CanViewAllShouldReturnFalseGivenOverlappingMovies2()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;
            var movies3 = new[]
            {
                new Movie(DateTime.Parse("1/1/2015 21:15", format), DateTime.Parse("1/1/2015 21:45", format)),
                new Movie(DateTime.Parse("1/1/2015 20:00", format), DateTime.Parse("1/1/2015 21:30", format)), //overlap
                new Movie(DateTime.Parse("1/1/2015 21:30", format), DateTime.Parse("1/1/2015 23:00", format))
            };

            //Act
            var verdict = CanViewAll(movies3);

            //Assert
            verdict.Should().BeFalse();
        }

        [TestMethod]
        public void CanViewAllShouldReturnTrueGivenMillionsOfNonOverlappingMovies()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;
            var current = DateTime.Parse("1/1/2015 21:15", format);
            var movies = Enumerable.Range(0, 1_000_000).Select(x =>
            {
                var start = current;
                current = current.AddMinutes(5);
                return new Movie(start, current);
            }).ToArray();

            //Act
            var verdict = CanViewAll(movies);

            //Assert
            verdict.Should().BeTrue();
        }

        [TestMethod]
        public void CanViewAllShouldReturnFalseGivenMillionsOfOverlappingMovies()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;

            var current = DateTime.Parse("1/1/2015 21:15", format);
            var movies = Enumerable.Range(0, 1_000_000).Select(x =>
            {
                if (x == 650_000)
                {
                    return new Movie(DateTime.Parse("1/1/2015 21:15", format), DateTime.Parse("1/1/2015 21:30", format));
                }

                var start = current;
                current = current.AddMinutes(5);
                return new Movie(start, current);
            }).ToArray();

            //Act
            var verdict = CanViewAll(movies);

            //Assert
            verdict.Should().BeFalse();
        }
    }
}
