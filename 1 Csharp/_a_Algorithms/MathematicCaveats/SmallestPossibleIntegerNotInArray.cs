﻿//           Write a function:
//           
//           class Solution { public int Solution(int[] A); }
//           
//           that, given an array A of N integers, returns the smallest positive integer(greater than 0) that does not occur in A.
//           
//           For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.
//           
//           Given A = [1, 2, 3], the function should return 4.
//           
//           Given A = [−1, −3], the function should return 1.
//           
//           Write an efficient algorithm for the following assumptions:
//           
//           N is an integer within the range[1..100, 000];
//           each element of array A is an integer within the range[−1, 000, 000..1, 000, 000].

using System;
using System.Collections.Generic;
using System.Linq;

namespace Testbed._a_Algorithms.MathematicCaveats
{
    static public class SmallestPossibleIntegerNotInArray
    {
        static public void Tester()
        {
            var success0 = false;
            var success1 = false;
            var success2 = false;
            var success3 = false;
            var success4 = false;

            {
                var result = GetSmallestPossibleIntegerNotInArray(new[] { 1, 2, 3 });
                success0 = result == 4;
            }

            {
                var result = GetSmallestPossibleIntegerNotInArray(new[] { 1, 2, 4 });
                success1 = result == 3;
            }

            {
                var result = GetSmallestPossibleIntegerNotInArray(new int[] { });
                success2 = result == 1;
            }

            {
                var result = GetSmallestPossibleIntegerNotInArray(new int[] { -1 });
                success3 = result == 1;
            }

            {
                var result = GetSmallestPossibleIntegerNotInArray(new [] { 1, 3, 3, 3, 6, 4, 1, 2 });
                success4 = result == 5;
            }
        }
        
        static public int GetSmallestPossibleIntegerNotInArray(IList<int> ints)
        {
            if (ints == null)
                throw new ArgumentException(nameof(ints));

            var uniquesCount = 0;
            return ints
                .Where(x => x > 0)
                .OrderBy(x => x)
                .Distinct()
                .Select((x, i) =>
                {
                    uniquesCount++;
                    return Tuple.Create(x, i + 1);
                })
                .FirstOrDefault(tuple => tuple.Item1 != tuple.Item2)
                ?.Item2 ?? (uniquesCount + 1);
        }
    }
}
