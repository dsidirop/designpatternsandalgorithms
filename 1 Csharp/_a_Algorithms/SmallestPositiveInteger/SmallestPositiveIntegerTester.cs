﻿// Codility demo-test challenge
//
// Write a function:
// 
// class Solution { public int Solution(int[] A); }
// 
// that, given an array A of N integers, returns the smallest positive integer(greater than 0) that does not occur in A.
// 
// For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.
// 
// Given A = [1, 2, 3], the function should return 4.
// 
// Given A = [−1, −3], the function should return 1.
// 
// Write an efficient algorithm for the following assumptions:
// 
// N is an integer within the range[1..100, 000];
// each element of array A is an integer within the range[−1, 000, 000..1, 000, 000].
//
// The optimum Solution should be O(n) complexity and constant memory footprint

// ReSharper disable UnusedMember.Global

using System;
using System.Linq;

namespace Testbed._a_Algorithms.SmallestPositiveInteger
{
    static public class SmallestPositiveIntegerTester
    {
        static public void Tester()
        {
            var s = new Solution();

            var res0 = s.solution(new int[] { });
            var syccess0 = res0 == 1;

            var res1 = s.solution(new[] {1, 3, 6, 4, 1, 2}); //beware the duplicates here
            var syccess1 = res1 == 5;

            var res2 = s.solution(new[] {1, 2, 3});
            var syccess2 = res2 == 4;

            var res3 = s.solution(new[] {-1, -3});
            var syccess3 = res3 == 1;

            var res4 = s.solution(new[] { 10 });
            var syccess4 = res4 == 1;

            var res5 = s.solution(new[] { 1000000 });
            var syccess5 = res5 == 1;

            var res6 = s.solution(new[] { -1000000 });
            var syccess6 = res6 == 1;
        }

        public class Solution
        {
            //optimal Solution   used O(n) complexity with minimal memory footprint
            public int solution(int[] A)
            {
                var positivesOnlySet = A
                    .Where(x => x > 0)
                    .ToArray();

                if (!positivesOnlySet.Any())
                    return 1;

                var totalCount = positivesOnlySet.Length;
                for (var i = 0; i < totalCount; i++) //O(n) complexity
                {
                    var abs = Math.Abs(positivesOnlySet[i]) - 1;
                    if (abs < totalCount && positivesOnlySet[abs] > 0) //notice the greater than zero check 
                        positivesOnlySet[abs] = -positivesOnlySet[abs];
                }

                for (var i = 0; i < totalCount; i++) //O(n) complexity
                {
                    if (positivesOnlySet[i] > 0)
                        return i + 1;
                }

                return totalCount + 1;
            }
            // https://codereview.stackexchange.com/a/179091/184415
            // 
            //    Solution(A)
            //    Filter out non-positive values from A
            //    For each int in filtered
            //        Let a zero-based index be the absolute value of the int - 1
            //        If the filtered range can be accessed by that index  and  filtered[index] is not negative
            //            Make the value in filtered[index] negative
            //    
            //    For each index in filtered
            //        if filtered[index] is positive
            //            return the index + 1 (to one-based)
            //    
            //    If none of the elements in filtered is positive
            //        return the length of filtered + 1 (to one-based)
            //    
            //    So an array A = [1, 2, 3, 5, 6], would have the following transformations:
            //    
            //    abs(A[0]) = 1, to_0idx = 0, A[0] = 1, make_negative(A[0]), A = [-1,  2,  3,  5,  6]
            //    abs(A[1]) = 2, to_0idx = 1, A[1] = 2, make_negative(A[1]), A = [-1, -2,  3,  5,  6]
            //    abs(A[2]) = 3, to_0idx = 2, A[2] = 3, make_negative(A[2]), A = [-1, -2, -3,  5,  6]
            //    abs(A[3]) = 5, to_0idx = 4, A[4] = 6, make_negative(A[4]), A = [-1, -2, -3,  5, -6]
            //    abs(A[4]) = 6, to_0idx = 5, A[5] is inaccessible,          A = [-1, -2, -3,  5, -6]
            //    
            //    A linear search for the first positive value returns an index of 3. Converting back to a one-based index results in Solution(A)=3+1=4

            //Solution with hashset()   works but its O(n x log(n))   we can do better
            //
            //public int Solution(int[] A) // write your code in C# 6.0 with .NET 4.5 (Mono)
            //{
            //    var positivesOnlySet = new HashSet<int>(A.Where(x => x > 0));
            //    if (!positivesOnlySet.Any())
            //        return 1;
            //
            //    for (var i = 1; i <= positivesOnlySet.Count; i++)
            //    {
            //        if (!positivesOnlySet.Contains(i))
            //            return i;
            //    }
            //
            //    return positivesOnlySet.Count + 1;
            //}

            //Solution with .sort()   works but its O(n x log(n))   we can do better
            //
            //public int Solution(int[] A) // write your code in C# 6.0 with .NET 4.5 (Mono)
            //{
            //    var positivesOnlySorted = A.Where(x => x > 0).OrderBy(x => x).Distinct().ToArray();
            //    if (!positivesOnlySorted.Any())
            //        return 1;
            //
            //    if (positivesOnlySorted[0] == 1 && positivesOnlySorted[positivesOnlySorted.Length - 1] == positivesOnlySorted.Length)
            //        return positivesOnlySorted.Length + 1; //fully sorted
            //
            //    var i = 0;
            //    positivesOnlySorted.FirstOrDefault(x => x != ++i);
            //
            //    return i;
            //}
        }
    }
}

