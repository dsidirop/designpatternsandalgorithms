﻿namespace Testbed._a_Algorithms.LogEntriesStatisticsAnalyzer
{
    public class HourlyStats
    {
        public long ErrorsCount; // { get; set; } //dont
        public long EntriesCount; // { get; set; }

        public double ErrorRate { get; set; }

        public void CalculateErrorRate()
        {
            ErrorRate = EntriesCount == 0
                ? 0
                : (double)ErrorsCount / EntriesCount;
        }
    }
}