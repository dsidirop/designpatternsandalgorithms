﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.CountTheTriplets
{
    [TestClass]
    public class CountTheTripletsTestbedRed
    {
        [TestMethod]
        public void ShouldThrowNullArgumentExceptionGivenNullInputArray()
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => Amazon.CountTheTriplets.Calculate(null));

            //Assert
            action.Should().ThrowExactly<ArgumentNullException>().WithMessage("*'input'*");
        }

        [TestMethod]
        public void ShouldThrowArgumentExceptionGivenNonDistinctArray()
        {
            //Arrange
            var input = new [] { 2, 2 };

            //Act
            var action = new Action(() => Amazon.CountTheTriplets.Calculate(input));

            //Assert
            action.Should().ThrowExactly<ArgumentException>().WithMessage("*'input'*");
        }
    }
}
