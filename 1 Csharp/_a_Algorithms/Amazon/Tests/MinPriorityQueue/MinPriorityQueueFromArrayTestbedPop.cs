﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.MinPriorityQueue
{
    [TestClass]
    public class MinPriorityQueueFromArrayTestbedPop
    {
        [TestMethod]
        public void ShouldReturnNullGivenEmptyHeap()
        {
            //Arrange
            var queue = new MinPriorityQueueFromArray<int>();

            //Act
            var action = new Action(() => queue.Pop());

            //Assert
            action.Should().ThrowExactly<InvalidOperationException>();
        }

        [TestMethod]
        public void ShouldReturnSingleItemGivenSingleItemHeap()
        {
            //Arrange
            const int expected = 1;
            var queue = new MinPriorityQueueFromArray<int>();
            queue.Insert(expected);

            //Act
            var result = queue.Pop();

            //Assert
            result.Should().Be(expected);
        }

        [TestMethod]
        public void ShouldReturnMinItemGivenFullHeap()
        {
            //Arrange
            var queue = new MinPriorityQueueFromArray<int>();
            queue.Insert(10);
            queue.Insert(30);
            queue.Insert(20);
            queue.Insert(1);

            //Act
            var result1 = queue.Pop();
            var result2 = queue.Pop();
            var result3 = queue.Pop();
            queue.TryPeek(out var result4a);
            queue.TryPop(out var result4b);
            var success5 = queue.TryPop(out var result5);
            var success6 = queue.TryPeek(out var result6);

            //Assert
            result1.Should().Be(1);
            result2.Should().Be(10);
            result3.Should().Be(20);
            
            result4a.Should().Be(30);
            result4b.Should().Be(30);
            
            success5.Should().Be(false);
            result5.Should().Be(0);

            success6.Should().Be(false);
            result6.Should().Be(0);
        }
    }
}