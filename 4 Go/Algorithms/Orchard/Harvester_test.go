package Orchard

import "testing"

func Test_Harvest_ShouldReturnSumOfAllApples_GivenKandLThatSumToTotalLength(t *testing.T) {
	//Arrange
	var harvester = SpawnHarvester()
	var expectedResult = 10

	//Act
	var actualResult, err = harvester.Harvest([]int{1, 2, 3, 4}, 1, 3)

	//Assert
	if err == nil && actualResult == expectedResult {
		t.Logf("Passed!")
		return
	}

	t.Errorf("Expected '%v' but got '%v'", expectedResult, actualResult)
}
