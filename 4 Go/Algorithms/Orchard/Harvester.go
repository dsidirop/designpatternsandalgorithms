// ** Codility Challenge for Verizon Connect   2018/11/12 **
//
// Alice and Bob work in a beautiful orchard. There are N apple trees in the orchard. The apple trees are arranged in a row and they are numbered from 1 to N.
//
// Alice is planning to collect all the apples from K consecutive trees and Bob is planning to collect all the apples from L consecutive trees.
//
// They want to choose two disjoint segments (one consisting of K trees for Alice and the other consisting of L trees for Bob) so as not to disturb each other.
// What is the maximum number of apples that they can collect? Write a function that given an array A consisting of N integers denoting the number of apples
// on each apple tree in the row, and integers K and L denoting, respectively, the number of trees that Alice and Bob can choose when collecting, returns the
// maximum number of apples that can be collected by them, or -1 if there are no such intervals.
//
// For example, given
//
// A = [6,1,4,6,3,2,7,4], K=3, L=2
//
// your function should return 24, because Alice can choose trees 3 to 5 and collect 4 + 6 + 3 = 13 apples, and Bob can choose trees 7 to 8 and collect 7 + 4 = 11
// apples. Thus, they will collect 13 + 11 = 24 apples in total, and that is the maximum number that can be achieved.
//
// Given:
//
// A = [10, 19, 15], K = 2, L = 2
//
// your function should return -1, because it is not possible for Alice and Bob to choose two disjoint intervals.
//
// Assume that: N is an integer within the range[2..600];
//
// - K and L are integers within the range[1..N - 1];
// - Each element of array A is an integer within the range[1..500]
//
// In your Solution focus on correctness.
//
// The performance of your Solution will not be the focus of the assessment.

package Orchard

import (
	"errors"
)

type IHarvester interface {
	Harvest(a []int, k int, l int) (sum int, error error)
}

type Harvester struct {
}

func SpawnHarvester() IHarvester {
	return &Harvester{}
}

func (o *Harvester) Harvest(a []int, k int, l int) (int, error) {
	if k <= 0 || l <= 0 {
		return -1, errors.New("k and l must be positives numbers")
	}

	var totalCount = len(a)
	if k+l > totalCount {
		return -1, nil
	}

	var max = 0
	for x := range GetAllPossibleSegmentPairsPermutations(a, k, l) {
		var currentSum = 0

		var startIndex1 = x[0]
		var endIndex1 = startIndex1 + x[1]

		var startIndex2 = x[2]
		var endIndex2 = startIndex2 + x[3]

		for _, i := range a[startIndex1:endIndex1] {
			currentSum += i
		}

		for _, i := range a[startIndex2:endIndex2] {
			currentSum += i
		}

		if max < currentSum {
			max = currentSum
		}
	}

	return max, nil
}

func GetAllPossibleSegmentPairsPermutations(a []int, k int, l int) <-chan [4]int { //0
	var channel = make(chan [4]int, 0) //zero buffering
	var totalCount = len(a)

	go func() {
		defer close(channel) //cleanup

		for i := 0; i+k <= totalCount; i++ {
			for j := 0; j+l <= i; j++ { //behind
				channel <- [4]int{i, k, j, l} // (i,k) (j,l)
			}

			for j := i + k; j+l <= totalCount; j++ { //ahead
				channel <- [4]int{i, k, j, l} // (i,k) (j,l)
			}
		}
	}()

	return channel

	//0  GetAllPossibleSegmentPairsPermutations is an iterator over all the numbers from 0 to the limit
}
