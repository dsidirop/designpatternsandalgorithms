﻿using System;
using System.Collections.Generic;
using System.Linq;

using Genumerics;

namespace Testbed._a_Algorithms.Utilities
{
    static public class GlobalCommonDivisor
    {
        static public T FindGcd<T>(this IEnumerable<T> arr)
        {
            if (!(arr?.Any() ?? false))
                throw new ArgumentException(nameof(arr));

            var one = Number.One<T>(); //optimization
            var zero = Number.Zero<T>(); //optimization
            return arr.Aggregate((x, current) => Gcd(x, current, zero, one));
        }

        static public T Gcd<T>(T a, T b) => Gcd(a, b, zero: Number.Zero<T>(), one: Number.One<T>());

        static public T Gcd<T>(T a, T b, T zero, T one)
        {
            if (Number.Equals(a, one) || Number.Equals(b, one)) return one;

            while (true)
            {
                if (Number.Equals(a, zero)) return b;

                var a1 = a;
                a = Number.Remainder(b, a); // a = b % a
                b = a1;
            }
        }
    }
}

