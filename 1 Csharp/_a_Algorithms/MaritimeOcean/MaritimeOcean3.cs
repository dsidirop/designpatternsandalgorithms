﻿// ReSharper disable UnusedVariable
// ReSharper disable UnusedMember.Global

using System;
using System.Threading.Tasks;

namespace Testbed._a_Algorithms.MaritimeOcean
{
    static public class MaritimeOcean3
    {
        static public void Tester()
        {
            DelayedPrint(10, "10").Wait();
        }

        static private async Task DelayedPrint(int wait, string str)
        {
            await Task.Delay(wait);
            Console.WriteLine(str);
        }
    }
}
