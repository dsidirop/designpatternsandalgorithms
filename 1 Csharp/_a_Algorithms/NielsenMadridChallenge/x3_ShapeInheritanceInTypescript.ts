﻿// Show how we can implement a Shape interface in typescript which includes a single method GetNumberOfEdges()
// which as the name implies returns the number of edges in a shape and then implement this interface in two
// classes Pentagon and Triangle

export interface IShape {
    GetNumberOfEdges(): number;
}

export default class Pentagon implements IShape { //default export
    GetNumberOfEdges(): number {
        return 5;
    }
}

export class Triangle implements IShape { //non default export
    GetNumberOfEdges(): number {
        return 3;
    }
}