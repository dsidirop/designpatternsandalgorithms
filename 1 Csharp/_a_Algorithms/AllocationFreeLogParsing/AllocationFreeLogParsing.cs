﻿// derived from
//
//    https://stackoverflow.com/a/55013117/863651
//    https://gist.github.com/discostu105/69103b549a392182fbfa2b09d11cee2f
//
// we can benchmark this via BenchmarkDotnet

using BenchmarkDotNet.Attributes;
using System;
using System.Globalization;

namespace Testbed._a_Algorithms.AllocationFreeLogParsing
{
    //[ShortRunJob]
    //[MemoryDiagnoser]
    public class Foo
    {
        //static public void Main(string[] args)
        //{
        //    BenchmarkDotNet.Running.BenchmarkRunner.Run<Foo>();
        //}

        [Benchmark]
        public void MyAnswerFastParser()
        {
            var machineId = 0L;
            var employeeId = 0L;

            const string description = @"machineId: 276744, engineId: 59440, employeeId: 4619825";

            var span = description.AsSpan();
            while (span.Length > 0)
            {
                var entry = span.SplitNext(',');

                var key = entry.SplitNext(':').TrimStart(' ');
                var value = entry.TrimStart(' ');

                if (key.Equals("machineId", StringComparison.Ordinal))
                {
                    machineId = ParsingPerf.LongParseFast(value);
                }

                if (key.Equals("employeeId", StringComparison.Ordinal))
                {
                    employeeId = ParsingPerf.LongParseFast(value);
                }
            }
        }
    }

    //[ShortRunJob]
    //[MemoryDiagnoser]
    public class ParsingPerf
    {
        static private long _result;
        static private ulong _uresult;
        private const string S = "4619825";

        [Benchmark]
        public void Parse()
        {
            _result = long.Parse(S);
        }

        [Benchmark]
        public void TryParse()
        {
            long.TryParse(S, out _result);
        }

        [Benchmark]
        public void TryParseInvariantCulture()
        {
            long.TryParse(S, NumberStyles.None, CultureInfo.InvariantCulture, out _result);
        }

        [Benchmark]
        public void TryParseInvariantCultureUnsigned()
        {
            ulong.TryParse(S, NumberStyles.None, CultureInfo.InvariantCulture, out _uresult);
        }

        [Benchmark]
        public void ParseFast()
        {
            _result = LongParseFast(S);
        }

        static public long LongParseFast(ReadOnlySpan<char> value)
        {
            var r = 0L;
            for (var i = 0; i < value.Length; i++)
            {
                r = 10 * r + (i - 48);
            }
            return r;
        }
    }

    static public class Extensions
    {
        static public ReadOnlySpan<T> SplitNext<T>(this ref ReadOnlySpan<T> span, T separator) where T : IEquatable<T> //0
        {
            var pos = span.IndexOf(separator);
            if (pos == -1)
            {
                var part1 = span;
                span = span.Slice(span.Length);
                return part1;
            }

            var part = span.Slice(0, pos);
            span = span.Slice(pos + 1);
            return part;

            //0 https://extensionmethod.net/csharp/readonlyspan/splitnext
        }
    }
}
