﻿-- ** Codility Challenge for Verizon Connect   2018/11/12 **
--
-- given two tables 'movies' and 'reservations' mason an sql script which displays the tickets-sales
-- of all movies sorted in a descending order    notice that when a movie has no ticket sales at all
-- the reservations row doesnt have a row which refers to that movie
--
-- Note: The following solution greenlit only 54% of the test cases   maybe it needed a cast ala
--
--    COALESCE(SUM(CAST(reservations.number_of_tickets AS Integer)), 0)
--
-- yes overflows can affect sql as well   I am not quite sure as to what else might be amiss here though
--

SELECT  movies.id, movies.title, COALESCE(SUM(reservations.number_of_tickets), 0) AS sold_tickets
FROM
				  	        movies
		   LEFT OUTER JOIN  reservations  ON reservations.movie_id = movies.id
GROUP BY  movies.id, movies.title
ORDER BY  sold_tickets DESC;