``` ini

BenchmarkDotNet=v0.12.1, OS=Windows 10.0.18363.1379 (1909/November2018Update/19H2)
Intel Core i7-2600 CPU 3.40GHz (Sandy Bridge), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.201
  [Host]     : .NET Core 5.0.4 (CoreCLR 5.0.421.11614, CoreFX 5.0.421.11614), X64 RyuJIT
  DefaultJob : .NET Core 5.0.4 (CoreCLR 5.0.421.11614, CoreFX 5.0.421.11614), X64 RyuJIT


```
|                                                         Method |     Mean |    Error |   StdDev |   Gen 0 |  Gen 1 | Gen 2 | Allocated |
|--------------------------------------------------------------- |---------:|---------:|---------:|--------:|-------:|------:|----------:|
|          TestTwoBuddiesToNetworkConverter_Parallel_EnumerableT | 49.65 μs | 0.827 μs | 0.691 μs | 16.2964 | 0.1221 |     - |  63.49 KB |
|       TestTwoBuddiesToNetworkConverter_NonParallel_EnumerableT | 15.56 μs | 0.311 μs | 0.484 μs |  1.1292 |      - |     - |   4.68 KB |
|           TestTwoBuddiesToNetworkConverter_ForEach_EnumerableT | 15.48 μs | 0.299 μs | 0.734 μs |  1.0986 |      - |     - |    4.5 KB |
|    TestTwoBuddiesToNetworkConverter_Parallel_EnumerableStrings | 52.33 μs | 1.037 μs | 1.064 μs | 16.2964 | 0.0610 |     - |  63.51 KB |
| TestTwoBuddiesToNetworkConverter_NonParallel_EnumerableStrings | 16.12 μs | 0.317 μs | 0.390 μs |  1.1292 |      - |     - |   4.68 KB |
|     TestTwoBuddiesToNetworkConverter_ForEach_EnumerableStrings | 14.88 μs | 0.291 μs | 0.346 μs |  1.0986 |      - |     - |    4.5 KB |
|    TestTwoBuddiesToNetworkConverter_Parallel_EnumerableObjects | 49.58 μs | 0.336 μs | 0.262 μs | 16.2964 | 0.1221 |     - |  63.51 KB |
| TestTwoBuddiesToNetworkConverter_NonParallel_EnumerableObjects | 15.30 μs | 0.247 μs | 0.193 μs |  1.1292 |      - |     - |   4.68 KB |
|     TestTwoBuddiesToNetworkConverter_ForEach_EnumerableObjects | 15.11 μs | 0.188 μs | 0.147 μs |  1.0986 |      - |     - |    4.5 KB |
|       TestTwoBuddiesToNetworkConverter_Parallel_ArrayOfStrings | 49.52 μs | 0.891 μs | 0.833 μs | 16.2964 | 0.0610 |     - |  63.59 KB |
|    TestTwoBuddiesToNetworkConverter_NonParallel_ArrayOfStrings | 14.98 μs | 0.285 μs | 0.267 μs |  1.1597 |      - |     - |   4.75 KB |
|        TestTwoBuddiesToNetworkConverter_ForEach_ArrayOfStrings | 14.54 μs | 0.289 μs | 0.310 μs |  1.1139 |      - |     - |   4.58 KB |
