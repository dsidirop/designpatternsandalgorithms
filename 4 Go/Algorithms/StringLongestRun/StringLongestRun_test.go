package StringLongestRun

import "testing"

func Test_StringLongestRun_ShouldReturnLongestString_GivenValidString(t *testing.T) {
	//Arrange
	var instance = SpawnStringLongestRun()
	var expectedResult = 4

	//Act
	var actualResult, err = instance.FindIndexOfLongestRun("aaaabbbbbbbbccc")

	//Assert
	if err == nil && actualResult == expectedResult {
		t.Logf("Passed!")
		return
	}

	t.Errorf("Expected '%v' but got '%v'", expectedResult, actualResult)
}
