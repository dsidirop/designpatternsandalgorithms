package SampleEnum

type EFoo int // inspired by https://stackoverflow.com/a/17989915/863651

const (
	A EFoo = iota
	C
	T
	G
)

type IEFoo interface {
	Get() EFoo
}

func (e EFoo) Get() EFoo { // every EFoo must fulfill the IEFoo interface
	return e
}

func (e EFoo) OtherMethod() { // "private"
	//some logic
}
