﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.SubarrayWithGivenSum
{
    [TestClass]
    public class CountTheTripletsTestbedGreen
    {
        [TestMethod]
        [DataRow(new[] { 1 }, 1, 0, 1)]
        [DataRow(new[] { 1, 2, 3 }, 1, 0, 1)]
        [DataRow(new[] { 1, 2, 3 }, 2, 1, 1)]
        [DataRow(new[] { 1, 2, 3 }, 3, 0, 2)]
        [DataRow(new[] { 2, 2, 3 }, 3, 2, 1)]
        [DataRow(new[] { 1, 2, 3 }, 5, 1, 2)]
        [DataRow(new[] { 1, 2, 3 }, 6, 0, 3)]
        [DataRow(new[] { 2, 2, 1 }, 1, 2, 1)]
        public void ShouldReturnFoundGivenMatchingParameters(in int[] input, in int targetSum, in int expectedSolutionIndex, in int expectedSolutionLength)
        {
            //Arrange
            //...

            //Act
            var results = Amazon.SubarrayWithGivenSum.Calculate(input, targetSum);

            //Assert
            results.Index.Should().Be(expectedSolutionIndex);
            results.Length.Should().Be(expectedSolutionLength);
        }

        [TestMethod]
        public void ShouldReturnFoundGivenMatchingParametersWithHeftyTargetSum()
        {
            //Arrange
            var input = new [] { int.MaxValue, int.MaxValue };
            var targetSum = 2 * (decimal) int.MaxValue;
            var expectedSolutionIndex = 0;
            var expectedSolutionLength = 2;

            //Act
            var results = Amazon.SubarrayWithGivenSum.Calculate(input, targetSum);

            //Assert
            results.Index.Should().Be(expectedSolutionIndex);
            results.Length.Should().Be(expectedSolutionLength);
        }

        [TestMethod]
        [DataRow(new int[] { 2 }, 1, -1, -1)]
        [DataRow(new int[] { 1, 2, 3 }, 7, -1, -1)]
        [DataRow(new int[] { 2, 2, 3 }, 1, -1, -1)]
        public void ShouldReturnNotFoundGivenNonMatchingParameters(in int[] input, in int sum, in int expectedSolutionIndex, in int expectedSolutionLength)
        {
            //Arrange
            //...

            //Act
            var results = Amazon.SubarrayWithGivenSum.Calculate(input, sum);

            //Assert
            results.Index.Should().Be(expectedSolutionIndex);
            results.Length.Should().Be(expectedSolutionLength);
        }
    }
}
