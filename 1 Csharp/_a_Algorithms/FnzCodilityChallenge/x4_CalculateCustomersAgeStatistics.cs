﻿// Provide a Solution that given a list of customers each of which has a name and a date of birth
//
// - calculates the average age (in years with 2-digit decimal precision) of all customers
// - calculates the average age (in years with 2-digit decimal precision) of all male customers
// - calculates the average age (in years with 2-digit decimal precision) of all female customers
//
// Landmine#1   The main caveat here lies on the calculation of the *exact* age of each customer
// Landmine#1   Its trickier than it sounds mainly because we have to project the month + day of
// Landmine#1   the birthday of each customer into the CURRENT year in order to see whether each
// Landmine#1   customer has had his birthday in the current year. If he hasn't then we must -1
// Landmine#1   his age.
// Landmine#1   
// Landmine#1   For instance if a customer was born in 1990/06/06 and we are in the year 2020 then
// Landmine#1   the customer:
// Landmine#1   
// Landmine#1   - if DateTime.UtcNow >= 2020/06/06 -> the customer is 30.xy years old 
// Landmine#1   - if DateTime.UtcNow < 2020/06/06  -> the customer is 29.xy years old (he didn't have his birthday yet for 2020! wow!)
//
// Landmine#2   Another gotcha here lies on calculating the average age of all customers *efficiently*
// Landmine#2   If we calculate the average age of all males (AvgM) and all females (AvgF) then we can
// Landmine#2   deduce the average age of *all* customers (males+females) without performing the entire
// Landmine#2   calculation all over again like so:
//
//              AvgAll = (AvgMales*MalesCount + AvgFemales*FemalesCount) / (MalesCount + FemalesCount)
//
// Landmine#3   Another caveat has to do with the calculation of the sum of all ages   it might overflow
//
// Landmine#4   This problem comes in many variations. For instance we can be asked to make a filtering
// Landmine#4   method which is meant to give us all of the log-entries of a log-system which are say
// Landmine#4   more than 3 years old (we obviously need to be very careful!)

// ReSharper disable UnusedVariable
// ReSharper disable UnusedMember.Global
// ReSharper disable NotAccessedVariable

using System;
using System.Collections.Generic;
using System.Linq;

namespace Testbed._a_Algorithms.FnzCodilityChallenge
{
    static public class x4_CalculateCustomersAgeStatistics
    {
        static public void Tester()
        {
            var success1 = false;
            var success2 = false;
            var success3a = false;
            var success3b = false;
            var success3c = false;
            var success4a = false;
            var success4b = false;
            var success4c = false;
            var success5a = false;
            var success5b = false;
            var success5c = false;
            var success6a = false;
            var success6b = false;
            var success6c = false;
            var success7a = false;
            var success7b = false;
            var success7c = false;
            var success8a = false;
            var success8b = false;
            var success8c = false;
            var success9a = false;
            var success9b = false;
            var success9c = false;
            var success10a = false;
            var success10b = false;
            var success10c = false;
            var success11a = false;
            var success11b = false;
            var success11c = false;

            {
                var result1 = new DateTime(1984, 2, 2).GetHumanAgeInYears(new DateTime(1985, 2, 2));
                success1 = Math.Abs(result1 - 1m) < 0.1m;
            }

            {
                var result2 = new DateTime(1984, 2, 2).GetHumanAgeInYears(new DateTime(1985, 2, 1));
                success2 = result2 - 1m < 0;
            }

            {
                var result3 = CalculateAgeStatistics(new Customer[] { }, new DateTime(1985, 2, 2));
                success3a = result3.TotalAverage == 0;
                success3b = result3.MaleAverage == 0;
                success3c = result3.FemaleAverage == 0;
            }

            {
                var result4 = CalculateAgeStatistics(
                    new[]
                    {
                        new Customer
                        {
                            EGender = EGender.Male,
                            Name = "John Smith",
                            DateOfBirth = new DateTime(1984, 2, 2)
                        }
                    },
                    new DateTime(1985, 2, 2)
                );
                success4a = Math.Abs(result4.TotalAverage - 1m) < 0.01m;
                success4b = Math.Abs(result4.MaleAverage - 1m) < 0.01m;
                success4c = result4.FemaleAverage == 0;
            }

            {
                var result5 = CalculateAgeStatistics(
                    new[]
                    {
                        new Customer
                        {
                            EGender = EGender.Female,
                            Name = "Anne Green",
                            DateOfBirth = new DateTime(1984, 2, 2)
                        }
                    },
                    new DateTime(1985, 2, 2)
                );
                success5a = Math.Abs(result5.TotalAverage - 1m) < 0.01m;
                success5b = result5.MaleAverage == 0;
                success5c = Math.Abs(result5.FemaleAverage - 1m) < 0.01m;
            }

            {
                var result6 = CalculateAgeStatistics(
                    new[]
                    {
                        new Customer
                        {
                            EGender = EGender.Male,
                            Name = "John Smith",
                            DateOfBirth = new DateTime(1984, 2, 2)
                        },
                        new Customer
                        {
                            EGender = EGender.Female,
                            Name = "Anne Green",
                            DateOfBirth = new DateTime(1984, 2, 2)
                        }
                    },
                    new DateTime(1985, 2, 2)
                );
                success6a = Math.Abs(result6.TotalAverage - 1m) < 0.01m;
                success6b = Math.Abs(result6.MaleAverage - 1m) < 0.01m;
                success6c = Math.Abs(result6.FemaleAverage - 1m) < 0.01m;
            }

            {
                var result7 = CalculateAgeStatistics(
                    new[]
                    {
                        new Customer
                        {
                            EGender = EGender.Male,
                            Name = "John Smith",
                            DateOfBirth = new DateTime(1984, 2, 2)
                        },
                        new Customer
                        {
                            EGender = EGender.Female,
                            Name = "Anne Green",
                            DateOfBirth = new DateTime(1984, 2, 2)
                        },
                        new Customer
                        {
                            EGender = EGender.Female,
                            Name = "Eva Singer",
                            DateOfBirth = new DateTime(1983, 2, 2)
                        }
                    },
                    new DateTime(1985, 2, 2)
                );
                success7a = Math.Abs(result7.TotalAverage - 1.33m) < 0.01m;
                success7b = Math.Abs(result7.MaleAverage - 1m) < 0.01m;
                success7c = Math.Abs(result7.FemaleAverage - 1.5m) < 0.01m;
            }

            {
                var result8 = CalculateAgeStatistics( //tricky scenario
                    new[]
                    {
                        new Customer
                        {
                            EGender = EGender.Male,
                            Name = "John Smith",
                            DateOfBirth = new DateTime(1984, 2, 29)
                            // ^-- leap years are divisible by 4 and can afford an extra day of february on nonleap years
                            //      we consider the customer to have aged by one year only when the day turns to march 1st
                        }
                    },
                    new DateTime(1985, 2, 28)
                );
                success8a = Math.Abs(result8.TotalAverage - 1m) < 0;
                success8b = Math.Abs(result8.MaleAverage - 1m) < 0;
                success8c = result8.FemaleAverage == 0;
            }

            {
                var result9 = CalculateAgeStatistics( //tricky scenario
                    new[]
                    {
                        new Customer
                        {
                            EGender = EGender.Male,
                            Name = "John Smith",
                            DateOfBirth = new DateTime(1984, 2, 29)
                            // ^-- leap years are divisible by 4 and can afford an extra day of february on nonleap years
                            //      we consider the customer to have aged by one year only when the day turns to march 1st
                        }
                    },
                    new DateTime(1985, 3, 1)
                );
                success9a = Math.Abs(result9.TotalAverage - 1m) < 0.01m;
                success9b = Math.Abs(result9.MaleAverage - 1m) < 0.01m;
                success9c = result9.FemaleAverage == 0;
            }

            {
                var result10 = CalculateAgeStatistics( //tricky scenario
                    new[]
                    {
                        new Customer
                        {
                            EGender = EGender.Male,
                            Name = "John Smith",
                            DateOfBirth = new DateTime(1984, 2, 29)
                            // ^-- leap years are divisible by 4 and can afford an extra day of february on nonleap years
                            //      we consider the customer to have aged by one year only when the day turns to march 1st
                        }
                    },
                    new DateTime(1984, 2, 29)
                );
                success10a = result10.TotalAverage == 0;
                success10b = result10.MaleAverage == 0;
                success10c = result10.FemaleAverage == 0;
            }

            {
                var result11 = CalculateAgeStatistics( //tricky scenario
                    new[]
                    {
                        new Customer
                        {
                            EGender = EGender.Male,
                            Name = "John Smith",
                            DateOfBirth = new DateTime(1984, 1, 1)
                            // ^-- leap years are divisible by 4 and can afford an extra day of february on nonleap years
                            //      we consider the customer to have aged by one year only when the day turns to march 1st
                        }
                    },
                    new DateTime(1984, 1, 1).AddMonths(6)
                );
                success11a = Math.Abs(result11.TotalAverage - 0.5m) <= 0.01m;
                success11b = Math.Abs(result11.MaleAverage - 0.5m) <= 0.01m;
                success11c = result11.FemaleAverage == 0;
            }
        }

        static public (decimal TotalAverage, decimal MaleAverage, decimal FemaleAverage) CalculateAgeStatistics(IEnumerable<Customer> customers, DateTime now)
        {
            if (!(customers?.Any() ?? false))
                return (TotalAverage: 0, MaleAverage: 0, FemaleAverage: 0);

            var malesCount = 0L;
            var maleAverageAge = customers
                .Where(x =>
                {
                    if (x.EGender != EGender.Male)
                        return false;

                    malesCount++;
                    return true;
                })
                .Select(x => x.DateOfBirth.GetHumanAgeInYears(currentDate: now))
                .DefaultIfEmpty(0m)
                .Average();

            var femalesCount = 0L;
            var femaleAverageAge = customers
                .Where(x =>
                {
                    if (x.EGender != EGender.Female)
                        return false;

                    femalesCount++;
                    return true;
                })
                .Select(x => x.DateOfBirth.GetHumanAgeInYears(currentDate: now))
                .DefaultIfEmpty(0m)
                .Average();

            var totalAverage = (maleAverageAge * malesCount + femaleAverageAge * femalesCount) / (malesCount + femalesCount); //optimization

            return (
                TotalAverage: Math.Round(totalAverage, 2),
                MaleAverage: Math.Round(maleAverageAge, 2),
                FemaleAverage: Math.Round(femaleAverageAge, 2)
            );
        }

        static public decimal GetHumanAgeInYears(this DateTime birthday, DateTime currentDate)
        {
            if (birthday > currentDate)
                throw new ArgumentException(nameof(birthday));

            var result = currentDate.GetExactDateDifference(birthday);
            var totalNumberOfDaysCoveredForNextBirthday = result.Months * 30 + result.Days;

            return result.Years + (totalNumberOfDaysCoveredForNextBirthday / NumberOfDaysInYearOnAverage);
        }
        private const decimal NumberOfDaysInYearOnAverage = 365.25m;

        static public (int Years, int Months, int Days) GetExactDateDifference(this DateTime date1, DateTime date2) //0
        {
            var age = default((int Years, int Months, int Days));
            var positiveDiff = true;

            if (
                date1.Year - date2.Year <= 0
                && (
                    date1.Year - date2.Year != 0
                    || (date2.Month >= date1.Month && (date2.Month != date1.Month || date2.Day > date1.Day))
                )
            )
            {
                var tmp = date2;
                date2 = date1;
                date1 = tmp;
                positiveDiff = false;
            }

            var daysInBirthdayMonth = DateTime.DaysInMonth(date2.Year, date2.Month);
            var daysRemaining = date1.Day + (daysInBirthdayMonth - date2.Day);

            if (date1.Month > date2.Month)
            {
                age.Days = (daysRemaining % daysInBirthdayMonth + daysInBirthdayMonth) % daysInBirthdayMonth;
                age.Years = date1.Year - date2.Year;
                age.Months = date1.Month - (date2.Month + 1) + Math.Abs(daysRemaining / daysInBirthdayMonth);
            }
            else if (date1.Month == date2.Month)
            {
                if (date1.Day >= date2.Day)
                {
                    age.Days = date1.Day - date2.Day;
                    age.Years = date1.Year - date2.Year;
                    age.Months = 0;
                }
                else
                {
                    age.Days = DateTime.DaysInMonth(date2.Year, date2.Month) - (date2.Day - date1.Day);
                    age.Years = (date1.Year - 1) - date2.Year;
                    age.Months = 11;
                }
            }
            else // date1.Month < date2.Month
            {
                age.Days = (daysRemaining % daysInBirthdayMonth + daysInBirthdayMonth) % daysInBirthdayMonth;
                age.Years = date1.Year - date2.Year - 1;
                age.Months = date1.Month + (11 - date2.Month) + Math.Abs(daysRemaining / daysInBirthdayMonth);
            }

            age.Days = positiveDiff ? age.Days : -age.Days;
            age.Years = positiveDiff ? age.Years : -age.Years;
            age.Months = positiveDiff ? age.Months : -age.Months;

            return age;

            //0 based off on https://github.com/faisalman/age-calc-cs/blob/master/Age.cs
        }

        public sealed class Customer
        {
            public EGender EGender { get; set; }

            public string Name { get; set; }
            public DateTime DateOfBirth { get; set; }
        }

        public enum EGender : byte
        {
            Male = 0,
            Female = 1
        }
    }
}


