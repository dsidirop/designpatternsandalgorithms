﻿// Write a function that finds the zero-based index of the longest run in a string    A run is a consecutive sequence of the same
// character  If there is more than one run with the same length, return the index of the first one.
//
// For example, IndexOfLongestRun("abbcccddddcccbba") should return 6 as the longest run is dddd and it first appears on index 6.

// ReSharper disable UnusedMember.Global
namespace Testbed._a_Algorithms.StringLongestRun
{
    static public class StringLongestRun
    {
        static public void Tester()
        {
            var result1 = IndexOfLongestRun("a");
            var result2 = IndexOfLongestRun("ab");
            var result3 = IndexOfLongestRun("aa");
            var result4 = IndexOfLongestRun("aabb");
            var result5 = IndexOfLongestRun("abbcccddddcccc");
        }

        static public int IndexOfLongestRun(string str)
        {
            if (string.IsNullOrEmpty(str)) return -1;
            if (str.Length <= 2) return 0;

            var c = str[0];
            var maxLength = 0;
            var currentLength = 0;
            var sequenceStartIndex = 0;
            var maxLengthStartIndex = 0;
            for (var i = 0; i <= str.Length; i++) //notice the use of <=
            {
                var t = i == str.Length ? (char?) null : str[i];
                if (c == t)
                {
                    currentLength++;
                    if (currentLength > maxLength && str.Length - i /*charsremaining*/ <= currentLength)
                    {
                        maxLengthStartIndex = sequenceStartIndex; //order
                        break;
                    }
                    continue;
                }

                if (maxLength < currentLength)
                {
                    maxLength = currentLength;
                    maxLengthStartIndex = sequenceStartIndex; //order
                }

                if (str.Length - i <= maxLength) //order
                {
                    break;
                }

                c = t ?? '\0';
                currentLength = 0;
                sequenceStartIndex = i; //order
            }

            return maxLengthStartIndex;
        }
    }
}
