﻿// ReSharper disable UnusedVariable
// ReSharper disable UnusedMember.Global

using System;

namespace Testbed._a_Algorithms.MaritimeOcean
{
    static public class MaritimeOcean7
    {
        static public void Tester()
        {
            var result = 10.0.Log10();
        }

        static public double Log10(this double input)
        {
            return Math.Log10(input);
        }
    }
}

