﻿// https://www.youtube.com/watch?v=xlvhyfcoQa4&ab_channel=KevinNaughtonJr.
//
// Maximum sum such that no two elements are adjacent   Also known as the house robber problem
//
// Given an array of positive numbers find the maximum sum of a subsequence with the constraint
// that no 2 numbers in the sequence should be adjacent in the array
//
// So 3 2 7 10 should return 13 (sum of 3 and 10) or 3 2 5 10 7 should return 15 (sum of 3 5
// and 7)  Answer the question in most efficient way
//
// This is a dp problem

using System;
using System.Linq;
using Ardalis.GuardClauses;

namespace Testbed._a_Algorithms.Amazon
{
    public class MaxSumNonAdjacent
    {
        static public int Calculate(int[] input)
        {
            Guard.Against.Null(input, nameof(input));

            if (!input.Any())
                return 0;

            var length = input.Length;
            if (length == 1)
                return input[0];

            if (length == 2)
                return Math.Max(input[0], input[1]);

            var excl = 0;
            var incl = input[0];
            for (var i = 1; i < length; i++)
            {
                var tempNewExcl = Math.Max(incl, excl); // current max excluding i
                incl = excl + input[i]; // current max including i
                excl = tempNewExcl;
            }

            return Math.Max(incl, excl); // return max of incl and excl
        }
    }
}
