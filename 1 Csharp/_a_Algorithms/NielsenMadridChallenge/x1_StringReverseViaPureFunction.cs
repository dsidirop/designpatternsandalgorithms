﻿//                Write a recursive *pure* function which given an input string S will reverse it
//
// Note:    the purpose of this coding challenge is to test whether the candidate can properly implement a pure function
// Note:
// Note:    in that sense the given string must be *cloned* wherever we pass it into the recursive function so that the original
// Note:    string will remain intact   in other words we shouldnt operate on the array of chars in the original string

// ReSharper disable UnusedVariable
// ReSharper disable UnusedMember.Global
// ReSharper disable NotAccessedVariable

namespace Testbed._a_Algorithms.NielsenMadridChallenge
{
    static public class x1_StringReverseViaPureFunction
    {
        static public void Tester()
        {
            var success0 = false;
            var success1 = false;
            var success2 = false;
            var success3 = false;
            var success4 = false;

            {
                var result = StringReverseViaPureFunction(@"");
                success0 = result == @"";
            }

            {
                var result = StringReverseViaPureFunction(@"a");
                success1 = result == @"a";
            }

            {
                var result = StringReverseViaPureFunction(@"ab");
                success2 = result == @"ba";
            }

            {
                var result = StringReverseViaPureFunction(@"abc");
                success3 = result == @"cba";
            }

            {
                var result = StringReverseViaPureFunction(@"abcd");
                success4 = result == @"dcba";
            }
        }

        static public string StringReverseViaPureFunction(string input)
        {
            if (input.Length <= 1)
                return input;

            var charStart = input[0];
            var charEnd   = input[input.Length - 1];
            var substring = input.Substring(startIndex: 1, length: input.Length - 2); //0 vital

            return charEnd + StringReverseViaPureFunction(substring) + charStart; //1 trailing recursion

            //0 returning a subclone of the original string is vital for the recursion to be pure
            //1 its worth noting that trailing recursion gets optimized by the csharp compiler so as to perform better than plain old recursion
        }
    }
}