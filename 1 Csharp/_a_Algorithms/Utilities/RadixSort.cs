﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Genumerics;
// ReSharper disable TooWideLocalVariableScope

// RadixShort generic implementation thanks to the gennumerics nuget package from
//
//                  https://github.com/TylerBrinkley/Genumerics
//
// 

namespace Testbed._a_Algorithms.Utilities
{
    [TestClass]
    public class RadixSortImplementation
    {
        static public void RadixSort<T>(T[] arr) where T : unmanaged //vital
        {
            if (arr.Length <= 1)
                return;

            var numericByteSize = 0;
            unsafe
            {
                numericByteSize = sizeof(T);
            }

            int i, j; //keep here

            var tmp = new T[arr.Length];
            var zero = Number.Zero<T>();
            for (var shift = numericByteSize * 8 - 1; shift > -1; --shift)
            {
                j = 0;
                for (i = 0; i < arr.Length; ++i)
                {
                    var move = Number.GreaterThanOrEqual(Number.LeftShift(arr[i], shift), zero);
                    if (shift == 0 ? !move : move)
                    {
                        arr[i - j] = arr[i];
                    }
                    else
                    {
                        tmp[j++] = arr[i];
                    }
                }
                Array.Copy(tmp, 0, arr, arr.Length - j, j);
            }
        }

        [TestMethod]
        public void ShouldMatchQuicksortGivenArrayOfInts()
        {
            //Arrange
            var array = new[] { 2, 5, -4, 11, 0, 18, 22, 67, 51, 6 };
            var expectedResult = new List<int>(array).OrderBy(x => x).ToArray();

            //Act
            RadixSort(array);

            //Assert
            array.Should().BeEquivalentTo(expectedResult, options => options.WithStrictOrdering());
        }

        [TestMethod]
        public void ShouldMatchQuicksortGivenArrayOfLongs()
        {
            //Arrange
            var array = new long[] { 2, 5, -4, 11, 0, 18, 22, 67, 51, 6 };
            var expectedResult = new List<long>(array).OrderBy(x => x).ToArray();

            //Act
            RadixSort(array);

            //Assert
            array.Should().BeEquivalentTo(expectedResult, options => options.WithStrictOrdering());
        }

        [TestMethod]
        public void ShouldMatchQuicksortGivenArrayOfSignedBytes()
        {
            //Arrange
            var array = new sbyte[] { 2, 5, -4, 11, 0, 18, 22, 67, 51, 6 };
            var expectedResult = new List<sbyte>(array).OrderBy(x => x).ToArray();

            //Act
            RadixSort(array);

            //Assert
            array.Should().BeEquivalentTo(expectedResult, options => options.WithStrictOrdering());
        }
    }
}
