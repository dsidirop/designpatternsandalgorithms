﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
// ReSharper disable UnusedVariable

namespace Testbed._a_Algorithms.Palindrome
{
    static public class Palindrome_Simple
    {
        static public void Tester()
        {
            var result = IsPalindrome("Νίψον ανομήματα μη μονάν Όψιν");
        }

        static public bool IsPalindrome(string str)
        {
            if (str == null || str.Length <= 1) return true;

            str = NonAlphaSpotter
                .Replace(str, "")
                .RemoveDiacritics()
                .ToUpperInvariant(); //0 toupper

            return StringComparer.InvariantCulture.Equals(str.ReverseString(), str);

            //0 comparing the two strings in uppercase mode is more efficient
        }

        static private string RemoveDiacritics(this string text) //0
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);

            //0 https://stackoverflow.com/a/249126/863651
        }

        static public string ReverseString(this string text)
        {
            if (text == null) return null;
            
            var array = text.ToCharArray();
            Array.Reverse(array);
            return new string(array);
        }

        static private readonly Regex NonAlphaSpotter = new Regex(@"[^\p{L}]", RegexOptions.IgnoreCase); // \p{L} stands for any letter in any language
    }

    //much more memory efficient
    static public class Palindrome_Optimal
    {
        static public void Tester()
        {
            var result1 = IsPalindrome("Νίψον ανομήματα μη μονάν Όψιν");
            var result2 = IsPalindrome("abc");
            var result3 = IsPalindrome("a b cba"); //true
            var result4 = IsPalindrome("a bc cb a"); //true
            var result5 = IsPalindrome("a b c cb X"); //false
        }

        static public bool IsPalindrome(string input)
        {
            var initialLength = input?.Length; //keep first
            if (input == null || initialLength <= 1) return true;

            var sanitizedInput = input
                .Select(x => x.ToString())
                .Where(x => AlphaSpotter.IsMatch(x))
                .Select(x => x.RemoveDiacritics().ToUpperInvariant()); //0

            var atLeast50PercentOfCharacters = (int) (initialLength / 2); //1
            return sanitizedInput
                .Take(atLeast50PercentOfCharacters)
                .Batch(batchSize: BATCH_SIZE)
                .Zip(
                    second: sanitizedInput.Reverse().Take(atLeast50PercentOfCharacters).Batch(batchSize: BATCH_SIZE), //2
                    resultSelector: (x, y) => (string.Join("", x), string.Join("", y))
                )
                .All(pair => pair.Item1 == pair.Item2);

            //0 comparing the two strings in uppercase mode is more efficient
            //1 we only need to check half the characters to know that the string is a palindrome
            //2 using ienumerable all over the place should guarantee that we are applying every operation on a pipeline which
            //  operates on the original characters of the original string array   that is without any extra memory overhead
        }
        private const int BATCH_SIZE = 2;

        static private string RemoveDiacritics(this string text) //0
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);

            //0 https://stackoverflow.com/a/249126/863651
        }

        static private readonly Regex AlphaSpotter = new Regex(@"[\p{L}]", RegexOptions.IgnoreCase); // \p{L} stands for any letter in any language
    }

    static class Ux
    {
        static public IEnumerable<IEnumerable<T>> Batch<T>(this IEnumerable<T> source, int batchSize)
        {
            using (var enumerator = source.GetEnumerator())
                while (enumerator.MoveNext())
                    yield return YieldBatchElements(enumerator, batchSize - 1);
        }

        static private IEnumerable<T> YieldBatchElements<T>(IEnumerator<T> source, int batchSize)
        {
            yield return source.Current;
            for (int i = 0; i < batchSize && source.MoveNext(); i++)
                yield return source.Current;
        }
    }
}
