﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Concurrent;

namespace Testbed._a_Algorithms.ATCom
{
    [TestClass]
    public class Entry
    {
        private readonly ConcurrentQueue<string> _queue = new ConcurrentQueue<string>();

        public string Leave() => _queue.TryDequeue(out var result) ? result : null;
        public void Enter(string passportNumber) => _queue.Enqueue(passportNumber);

        [TestMethod]
        public void Testbed()
        {
            var entry = new Entry();
            entry.Enter("AB54321");
            entry.Enter("UK32032");
        
            entry.Leave().Should().Be("AB54321");
            entry.Leave().Should().Be("UK32032");
            entry.Leave().Should().Be(null);
        }
    }
}