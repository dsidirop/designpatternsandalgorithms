﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Globalization;
using TrainArrivalAndDeparture = Testbed._a_Algorithms.Amazon.MinimumNumberOfPlatforms.TrainArrivalAndDeparture;

namespace Testbed._a_Algorithms.Amazon.Tests.MinimumNumberOfPlatforms.TrainArrivalAndDepartureTestbed
{
    [TestClass]
    public class TrainArrivalAndDepartureTestbedGreen
    {
        [TestMethod]
        public void ShouldConstructInstanceWithoutSecsGivenDatesWithSecs()
        {
            //Arrange
            //...

            //Act
            var trainArrivalAndDeparture = new TrainArrivalAndDeparture(ParseDate(@"2010/01/10 10:00:20"), ParseDate(@"2010/01/10 10:30:13"));

            //Assert
            trainArrivalAndDeparture.Arrival.Should().Be(ParseDate(@"2010/01/10 10:00"));
            trainArrivalAndDeparture.Departure.Should().Be(ParseDate(@"2010/01/10 10:30"));
        }

        private const string DateFormat = @"yyyy'/'MM'/'dd HH':'mm";
        private const string DateFormatWithSeconds = @"yyyy'/'MM'/'dd HH':'mm':'ss";
        static private DateTime ParseDate(in string dateString)
        {
            var success1 = DateTime.TryParseExact(dateString, DateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out var date1);
            if (success1) return date1;

            var success2 = DateTime.TryParseExact(dateString, DateFormatWithSeconds, CultureInfo.InvariantCulture, DateTimeStyles.None, out var date2);
            if (success2) return date2;

            throw new ArgumentException(nameof(dateString));
        }
    }
}
