﻿// ** Codility Challenge for Verizon Connect   2018/11/12 **
// 
// Assuming a week upon which various non-overlapping meetings are being held make a function which reports how much
// the maximum interval lasts (in minutes) among all possible intervals that separate any two adjacent meetings
//
// ReSharper disable UnusedVariable

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Testbed._a_Algorithms.Verizon
{
    static public class MaxSleepTimeBetweenMeetings
    {
        static public void Tester()
        {
            var s = new Solution();

            var res0 = s.solution(@"
                Mon 01:00-23:00
                Tue 01:00-23:00
                Wed 01:00-23:00
                Thu 01:00-23:00
                Fri 01:00-23:00
                Sat 01:00-23:00
                Sun 01:00-21:00
            ");

            var res1 = s.solution(@"
                Sun 10:00 - 20:00
                Fri 05:00 - 10:00
                Fri 16:30 - 23:50
                Sat 10:00 - 24:00
                Sun 01:00 - 04:00
                Sat 02:00 - 06:00
                Tue 03:30 - 18:15
                Tue 19:00 - 20:00
                Wed 04:25 - 15:14
                Wed 15:14 - 22:40
                Thu 00:00 - 23:59
                Mon 05:00 - 13:00
                Mon 15:00 - 21:00
            ");

            var res3 = s.solution(@"");
        }

        public class Solution
        {
            public int solution(string S)
            {
                var firstMondayOfTheYear = GetFirstMondayOfYear(DateTime.Now.Year);

                var dateSpanPairs = S.Trim()
                    .Split(new[] {"\r\n", "\n"}, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x =>
                    {
                        x = x.Trim();
                        var parts = x.Split('-').Select(y => y.Trim()).ToArray();
                        var day = x.Substring(0, 3);

                        var exactDate = firstMondayOfTheYear.AddDays(DaysOfWeek.IndexOf(day)).ToString("yyyy/MM/dd");

                        var start = $"{exactDate} {parts.First().Substring(4)}";
                        var startShouldEmploy2400workaround = start.EndsWith(@"24:00");
                        if (startShouldEmploy2400workaround)
                        {
                            start = start.Replace(@"24:00", @"23:59");
                        }

                        var end = $"{exactDate} {parts.Skip(1).First()}";
                        var endShouldEmploy2400workaround = end.EndsWith(@"24:00");
                        if (endShouldEmploy2400workaround)
                        {
                            end = end.Replace(@"24:00", @"23:59");
                        }

                        var startDate = DateTime.ParseExact(start, "yyyy'/'MM'/'dd HH':'mm", CultureInfo.InvariantCulture);
                        if (startShouldEmploy2400workaround)
                        {
                            startDate = startDate.AddMinutes(1);
                        }

                        var endDate = DateTime.ParseExact(end, "yyyy'/'MM'/'dd HH':'mm", CultureInfo.InvariantCulture);
                        if (endShouldEmploy2400workaround)
                        {
                            endDate = endDate.AddMinutes(1);
                        }

                        return Tuple.Create(startDate, endDate);
                    })
                    .ToList();

                dateSpanPairs.Insert(0, Tuple.Create(firstMondayOfTheYear, firstMondayOfTheYear));
                dateSpanPairs.Add(Tuple.Create(firstMondayOfTheYear.AddDays(7), firstMondayOfTheYear.AddDays(7)));
                dateSpanPairs = dateSpanPairs.OrderBy(x => x.Item1).ToList();

                //new DateTime().ToString("")

                return (int) dateSpanPairs
                    .Select((x, i) =>
                    {
                        if (i == dateSpanPairs.Count - 1)
                            return TimeSpan.Zero;

                        return dateSpanPairs[i + 1].Item1 - x.Item2;
                    })
                    .Max() //.OrderByDescending(x => x).FirstOrDefault()
                    .TotalMinutes;
            }

            static private readonly List<string> DaysOfWeek = new List<string>(7) {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};

            static private DateTime GetFirstMondayOfYear(int year)
            {
                var dt = new DateTime(year, 1, 1);

                while (dt.DayOfWeek != DayOfWeek.Monday)
                {
                    dt = dt.AddDays(1);
                }

                return dt;
            }
        }
    }
}


