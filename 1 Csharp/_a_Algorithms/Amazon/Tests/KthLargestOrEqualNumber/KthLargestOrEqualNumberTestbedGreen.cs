﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.KthLargestOrEqualNumber
{
    [TestClass]
    public class KthLargestOrEqualNumberTestbedGreen
    {
        [TestMethod]
        [DataRow(new[] { 1 }, 1, 1)]
        [DataRow(new[] { 1, 2 }, 1, 2)]
        [DataRow(new[] { 1, 2 }, 2, 1)]
        [DataRow(new[] { 2, 1 }, 1, 2)]
        [DataRow(new[] { 2, 1 }, 2, 1)]
        [DataRow(new[] { 2, 3, 1 }, 2, 2)]
        [DataRow(new[] { 1, 2, 1, 2, 1, 2 }, 1, 2)]
        [DataRow(new[] { 1, 2, 1, 2, 1, 2 }, 1, 2)]
        [DataRow(new[] { 1, 2, 1, 2, 1, 2 }, 2, 2)]
        [DataRow(new[] { 1, 2, 1, 2, 1, 2 }, 3, 2)]
        [DataRow(new[] { 1, 2, 1, 2, 1, 2 }, 4, 1)]
        [DataRow(new[] { 1, 2, 1, 2, 1, 2 }, 5, 1)]
        [DataRow(new[] { 1, 2, 1, 2, 1, 2 }, 6, 1)]
        [DataRow(new[] { 5, 3, 7, 10, 11, 7, 6 }, 3, 7)]
        public void ShouldReturnCorrectElementGivenValidArrayOfNumbers(in int[] numbers, in int kthLargestElement, in int expectedResult)
        {
            //Arrange
            //...

            //Act
            var result = Amazon.KthLargestOrEqualNumber.Calculate(numbers, kthLargestElement);

            //Assert
            result.Should().Be(expectedResult);
        }
    }
}

