﻿// ReSharper disable UnusedVariable
// ReSharper disable UnusedMember.Global

using System;
using System.Collections.Generic;
using System.Linq;

namespace Testbed._a_Algorithms.PathManipulator
{
    static public class PathManipulator
    {
        static public void Tester()
        {
            var success1 = false;
            var success2 = false;
            var success3 = false;
            var success4 = false;
            var success5 = false;
            var success6 = false;
            var success7 = false;
            var success8 = false;
            var success9 = false;
            var success10 = false;
            var success11 = false;

            {
                try
                {
                    var path = new Path(null);
                }
                catch (ArgumentException)
                {
                    success1 = true;
                }
                catch
                {
                    success1 = false;
                }
            }

            {
                try
                {
                    var path = new Path("");
                }
                catch (ArgumentException)
                {
                    success2 = true;
                }
                catch
                {
                    success2 = false;
                }
            }

            {
                try
                {
                    var path = new Path("123");
                }
                catch (ArgumentException)
                {
                    success3 = true;
                }
                catch
                {
                    success3 = false;
                }
            }

            //cd test
            {
                try
                {
                    var path = new Path("/");
                    path.Cd(null);
                }
                catch (ArgumentException)
                {
                    success4 = true;
                }
                catch
                {
                    success4 = false;
                }
            }

            //cd test
            {
                try
                {
                    var path = new Path("/");
                    path.Cd("");
                }
                catch (ArgumentException)
                {
                    success5 = true;
                }
                catch
                {
                    success5 = false;
                }
            }

            //cd test
            {
                try
                {
                    var path = new Path("/");
                    path.Cd("123");
                }
                catch (ArgumentException)
                {
                    success6 = true;
                }
                catch
                {
                    success6 = false;
                }
            }

            //cd test
            {
                try
                {
                    var path = new Path("/");
                    path.Cd(".");
                }
                catch (ArgumentException)
                {
                    success7 = true;
                }
                catch
                {
                    success7 = false;
                }
            }

            //core tests
            {
                var path = new Path("/a/b/c/d");
                path.Cd("../x");

                var result = path.CurrentPath;
                success8 = result == "/a/b/c/x";
            }

            {
                var path = new Path("/");
                path.Cd("..");

                var result = path.CurrentPath;
                success9 = result == "/";
            }

            {
                var path = new Path("/");
                path.Cd("../../..");

                var result = path.CurrentPath;
                success10 = result == "/";
            }

            {
                var path = new Path("/a/b/c");
                path.Cd("/");

                var result = path.CurrentPath;
                success10 = result == "/";
            }

            {
                var path = new Path("/a/b/c");
                path.Cd("/d/c/e");

                var result = path.CurrentPath;
                success11 = result == "/d/c/e";
            }
        }

        public class Path
        {
            public string CurrentPath => PathSeparator + string.Join(PathSeparator.ToString(), _pathComponents.Skip(1));

            private List<string> _pathComponents = null;

            public Path(string path)
            {
                _pathComponents = GeneratePathComponents(path, mustBeAbsolutePath: true);
            }

            public void Cd(string newPath)
            {
                var newPathComponents = GeneratePathComponents(newPath, mustBeAbsolutePath: false);
                if (newPathComponents.First() == PathSeparator.ToString()) //absolute path
                {
                    _pathComponents = newPathComponents;
                    return;
                }

                _pathComponents = NormalizePathComponents(_pathComponents.Concat(newPathComponents).ToArray());
            }

            static private List<string> NormalizePathComponents(string[] pathComponents)
            {
                var stack = new Stack<string>(pathComponents.Length);
                foreach (var x in pathComponents)
                {
                    if (x == "..")
                    {
                        if (stack.Any() && stack.Peek() != PathSeparator.ToString())
                        {
                            stack.Pop();
                        }
                        continue;
                    }

                    stack.Push(x);
                }

                return stack.Reverse().ToList();
            }

            public List<string> GeneratePathComponents(string path, bool mustBeAbsolutePath)
            {
                path = (path ?? "").Trim();

                if (string.IsNullOrWhiteSpace(path))
                    throw new ArgumentException(nameof(path));

                var isAbsolutePath = path.First() == PathSeparator;
                if (mustBeAbsolutePath && !isAbsolutePath)
                    throw new ArgumentException(nameof(path));

                if (path.Any(c => !(
                    c == PathSeparator
                    || c == '.'
                    || (c >= 'A' && c <= 'Z')
                    || (c >= 'a' && c <= 'z')
                ))) throw new ArgumentException(nameof(path));

                var pathComponents = path.Split(new[] { PathSeparator }, StringSplitOptions.RemoveEmptyEntries).ToList();
                if (pathComponents.Any(x => x.Contains(".") && x != ".."))
                    throw new ArgumentException(nameof(path));

                if (isAbsolutePath)
                {
                    pathComponents.Insert(0, "/");
                }

                return pathComponents;
            }

            private const char PathSeparator = '/';
        }
    }
}


