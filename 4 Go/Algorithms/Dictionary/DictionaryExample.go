package main

import (
	"fmt"
	"reflect"
)

func main() {
	m := make(map[string]int)

	m["k1"] = 7
	m["k2"] = 13

	fmt.Println("map:", m)

	v1 := m["k1"]
	fmt.Println("v1: ", v1)

	fmt.Println("len:", len(m))

	value, found := m["k2"]
	fmt.Printf("found: %v - value: %v\n", found, value)

	delete(m, "k2")
	fmt.Println("map:", m)

	//delete(m, 3) //this is a compilation error
	delete(m, "something_non_existant") //we can delete twice no problem
	fmt.Println("map:", m)

	value2, found2 := m["foobar"]
	fmt.Printf("found2: %v - value2: %v\n", found2, value2)

	value3, found3 := m["k2"]
	fmt.Printf("found3: %v - value3: %v\n", found3, value3)

	fmt.Printf("map-type through reflection is %q\n", reflect.TypeOf(m))
	fmt.Printf("map-key-type through reflection is %q\n", reflect.TypeOf(m).Key())
	fmt.Printf("map-key-elem through reflection is %q\n", reflect.TypeOf(m).Elem())

	n := map[string]int{"foo": 1, "bar": 2}
	fmt.Println("map:", n)

}
