﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.RotateMatrixAntiClockwise90Degrees
{
    [TestClass]
    public class FlipTestbedGreen
    {
        [TestMethod]
        public void ShouldResultInEmptyMatrixGivenEmptyMatrix()
        {
            //Arrange
            var matrix = new int[0, 0];

            //Act
            RotateSquareMatrixAntiClockwise90Degrees.FlipVertically(ref matrix);

            //Assert
            matrix.Should().BeEquivalentTo(new int[0, 0]);
        }

        [TestMethod]
        public void ShouldResultInSingleItemMatrixGivenSingleItemMatrix()
        {
            //Arrange
            var matrix = new int[1, 1] { { 1 } };

            //Act
            RotateSquareMatrixAntiClockwise90Degrees.FlipVertically(ref matrix);

            //Assert
            matrix.Should().BeEquivalentTo(new int[1, 1] { { 1 } });
        }

        [TestMethod]
        public void ShouldResultIn2X2MatrixGiven2X2Matrix()
        {
            //Arrange
            var matrix = new int[2, 2]
            {
                { 1, 2 },
                { 3, 4 }
            };
            var expected = new int[2, 2]
            {
                { 3, 4 },
                { 1, 2 }
            };

            //Act
            RotateSquareMatrixAntiClockwise90Degrees.FlipVertically(ref matrix);

            //Assert
            matrix.Should().BeEquivalentTo(expected);
        }

        [TestMethod]
        public void ShouldResultIn3X3MatrixGiven3X3Matrix()
        {
            //Arrange
            var matrix = new int[3, 3]
            {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 7, 8, 9 }
            };
            var expected = new int[3, 3]
            {
                { 7, 8, 9 },
                { 4, 5, 6 },
                { 1, 2, 3 }
            };

            //Act
            RotateSquareMatrixAntiClockwise90Degrees.FlipVertically(ref matrix);

            //Assert
            matrix.Should().BeEquivalentTo(expected);
        }

        [TestMethod]
        public void ShouldResultIn4X4MatrixGiven4X4Matrix()
        {
            //Arrange
            var matrix = new int[4, 4]
            {
                { 01, 02, 03, 04 },
                { 05, 06, 07, 08 },
                { 09, 10, 11, 12 },
                { 13, 14, 15, 16 },
            };
            var expected = new int[4, 4]
            {
                { 13, 14, 15, 16 },
                { 09, 10, 11, 12 },
                { 05, 06, 07, 08 },
                { 01, 02, 03, 04 }
            };

            //Act
            RotateSquareMatrixAntiClockwise90Degrees.FlipVertically(ref matrix);

            //Assert
            matrix.Should().BeEquivalentTo(expected);
        }

        [TestMethod]
        public void ShouldResultIn4X1MatrixGiven4X1Matrix()
        {
            //Arrange
            var matrix = new int[4, 1]
            {
                { 1 },
                { 2 },
                { 3 },
                { 4 }
            };
            var expected = new int[4, 1]
            {
                { 4 },
                { 3 },
                { 2 },
                { 1 }
            };

            //Act
            RotateSquareMatrixAntiClockwise90Degrees.FlipVertically(ref matrix);

            //Assert
            matrix.Should().BeEquivalentTo(expected);
        }
    }
}
