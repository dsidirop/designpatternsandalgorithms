﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.FirstBadVersion
{
    [TestClass]
    public class FirstBadVersionTestbedGreen
    {
        [TestMethod]
        [DataRow(0, 0, -1)]
        [DataRow(1, 1, 1)]
        [DataRow(2, 1, 1)]
        [DataRow(10, 1, 1)]
        [DataRow(2, 1, 1)]
        [DataRow(3, 2, 2)]
        [DataRow(4, 3, 3)]
        [DataRow(2, 3, -1)]
        public void ShouldReturnFoundGivenMatchingParameters(in int maxVersion, in int badVersionThreshold, in int expectedResult)
        {
            //Arrange
            //...

            //Act
            var threshold = badVersionThreshold;
            var result = Amazon.FirstBadVersion.SpawnFinder(version => version >= threshold)(maxVersion);

            //Assert
            result.Should().Be(expectedResult);
        }
    }
}
