﻿// ** Codility Challenge for Verizon Connect   2018/11/12 **
//
// Alice and Bob work in a beautiful orchard. There are N apple trees in the orchard. The apple trees are arranged in a row and they are numbered from 1 to N.
// 
// Alice is planning to collect all the apples from K consecutive trees and Bob is planning to collect all the apples from L consecutive trees.
// 
// They want to choose two disjoint segments (one consisting of K trees for Alice and the other consisting of L trees for Bob) so as not to disturb each other.
// What is the maximum number of apples that they can collect? Write a function that given an array A consisting of N integers denoting the number of apples
// on each apple tree in the row, and integers K and L denoting, respectively, the number of trees that Alice and Bob can choose when collecting, returns the
// maximum number of apples that can be collected by them, or -1 if there are no such intervals.
//
// For example, given
//
// A = [6,1,4,6,3,2,7,4], K=3, L=2
//
// your function should return 24, because Alice can choose trees 3 to 5 and collect 4 + 6 + 3 = 13 apples, and Bob can choose trees 7 to 8 and collect 7 + 4 = 11
// apples. Thus, they will collect 13 + 11 = 24 apples in total, and that is the maximum number that can be achieved.
//
// Given:
//
// A = [10, 19, 15], K = 2, L = 2
//
// your function should return -1, because it is not possible for Alice and Bob to choose two disjoint intervals.
//
// Assume that: N is an integer within the range[2..600];
// 
// - K and L are integers within the range[1..N - 1];
// - Each element of array A is an integer within the range[1..500]
//
// In your Solution focus on correctness.
//
// The performance of your Solution will not be the focus of the assessment.

using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable UnusedVariable

namespace Testbed._a_Algorithms.Verizon
{
    static public class MaxAppleGatheringAliceAndBob
    {
        static public void Tester()
        {
            var applePicker = new ApplePicker();

            var res0 = applePicker.Harvest(new[] {6, 1}, 3, 2);
            var suc0 = res0 == -1;

            var res1 = applePicker.Harvest(new[] {6, 1, 4, 8, 9}, 3, 2);
            var suc1 = res1 == 28;

            var res2 = applePicker.Harvest(new[] {6, 1, 4, 8, 9}, 2, 2);
            var suc2 = res2 == 24;

            var res3 = applePicker.Harvest(new[] {6, 1, 4, 6, 3, 2, 7, 4}, 3, 2);
            var suc3 = res3 == 24;

            var res4 = applePicker.Harvest(new[] { 6, 1, 4, 6, 3, 2, 7, 4 }, 3, 2);
            var suc4 = res3 == 24;

            var res5 = applePicker.Harvest(new[] { 6, 1 }, 1, 1);
            var suc5 = res5 == 7;
        }

        public class ApplePicker
        {
            public int Harvest(int[] A, int K, int L)
            {
                var totalCount = A.Length;
                if (K + L > totalCount) //length -> [2, 600]
                    return -1;

                var results = GetAllPossibleSegmentPairsPermutations(A, K, L)
                    .Select(x =>
                    {
                        var kStartIndex = x.Item1.Item1;
                        var kCount = x.Item1.Item2;

                        var lStartIndex = x.Item2.Item1;
                        var lCount = x.Item2.Item2;

                        //the maximum possible amount is 500*600 = 300000 so an int should suffice  HOWEVER in the general case scenario
                        //where larger array-sizes and weights are used using decimal is vital to immunize ourselves against overflow scenarios
                        //we consciously prefer to fortify our code and make it futureproof   it cant hurt
                        
                        return A.Skip(kStartIndex).Take(kCount).Aggregate(seed: (decimal) 0, func: (current, appleCount) => current + appleCount)
                               + A.Skip(lStartIndex).Take(lCount).Aggregate(seed: (decimal) 0, func: (current, appleCount) => current + appleCount);
                    })
                    .Max(); //equivalent to but more shorthandish than .OrderByDescending(x => x).FirstOrDefault() //.OrderByDescending(x => x.Item3).FirstOrDefault()

                return (int) results;
            }

            static private IEnumerable<Tuple<Tuple<int, int>, Tuple<int, int>>> GetAllPossibleSegmentPairsPermutations(int[] A, int K, int L)
            {
                var totalCount = A.Length;
                for (var i = 0; i + K <= totalCount; i++)
                {
                    for (var j = 0; j + L <= i; j++) //behind
                    {
                        yield return Tuple.Create(
                            Tuple.Create(i, K),
                            Tuple.Create(j, L)
                        );
                    }

                    for (var j = i + K; j + L <= totalCount; j++) //ahead
                    {
                        yield return Tuple.Create(
                            Tuple.Create(i, K),
                            Tuple.Create(j, L)
                        );
                    }
                }
            }
        }
    }
}


