﻿//
//      https://practice.geeksforgeeks.org/problems/missing-number-in-array/0
//      https://www.youtube.com/watch?v=lBk6fVhuCyw
//
// Given an array C of size N-1 and given that there are integer numbers from 1 to N with one element missing
// the missing number is to be found
//
// Notes
//
//       Elligible for parallelization
//
// Complexities
//
//       Time   O(N)
//       Space  O(1)
//

using Ardalis.GuardClauses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Testbed._a_Algorithms.Amazon
{
    public class FindMissingNumberInArray
    {
        static public uint Calculate(uint[] input)
        {
            Guard.Against.Null(input, nameof(input));

            var length = input.Length;
            if (length == 0)
                return 1;

            if (length < 10_000) //simple and straightforward
                return X1(input, length) ^ X2(length);

            var xx1 = (uint)0;
            var xx2 = (uint)0;
            Parallel.Invoke( //parallelization for huge arrays
                () => xx1 = X1(input, length),
                () => xx2 = X2(length)
            );

            return xx1 ^ xx2;

            static uint X1(IReadOnlyList<uint> @in, in int length)
            {
                var u = (uint)0;
                var current = (uint)0;
                for (var i = 0; i < length; i++)
                {
                    current = @in[i];
                    if (current == 0)
                        throw new ArgumentException("input");

                    u ^= current;
                }

                return u;
            }

            static uint X2(in int length)
            {
                var x2 = (uint)1;
                var lengthPlus1 = length + 1;
                for (var j = (uint)2; j <= lengthPlus1; j++)
                {
                    x2 ^= j;
                }

                return x2;
            }
        }
    }
}
