// task    create a generic dictionary

package main

import "fmt"

type GenericDictionary struct {
	Data map[interface{}][]interface{}
}

func NewGenericDictionary() (d *GenericDictionary) {
	return &GenericDictionary{
		Data: make(map[interface{}][]interface{}),
	}
}

func (d *GenericDictionary) Add(key interface{}, value interface{}) *GenericDictionary {
	slice, alreadyExists := d.Data[key]
	if !alreadyExists {
		slice = make([]interface{}, 0)
	}

	d.Data[key] = append(slice, value)
	return d
}

func (d *GenericDictionary) Get(key interface{}) interface{} {
	return d.Data[key]
}

type ComplexKey struct {
	a int
	b int
}

func main() {
	dict := NewGenericDictionary()

	foo := NewGenericDictionary()

	dict.Add("Foo", 1)
	dict.Add(123, NewGenericDictionary())
	dict.Add(foo, 123)

	fmt.Println(dict.Get("Foo"))
	fmt.Println(dict.Get(123))
	fmt.Println(dict.Get(foo))
}
