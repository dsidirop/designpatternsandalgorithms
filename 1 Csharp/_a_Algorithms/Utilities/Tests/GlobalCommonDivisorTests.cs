﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Testbed._a_Algorithms.Utilities.Tests
{
    [TestClass]
    public class GlobalCommonDivisorTests
    {
        [TestMethod]
        [DataRow(null, DisplayName = "Array")]
        [DataRow(new int[] { }, DisplayName = "Empty Array")]
        public void ShouldThrowIllegalArgumentExceptionGivenEmptyArray(int[] array)
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => array.FindGcd());

            //Assert
            action.Should().ThrowExactly<ArgumentException>();
        }

        [TestMethod]
        public void ShouldReturnSingleElementGivenArrayWithOnlyOneElement()
        {
            //Arrange
            var array = new[] { 3 };

            //Act
            var result = array.FindGcd();

            //Assert
            result.Should().Be(array[0]);
        }

        [TestMethod]
        [DataRow(new[] { 0 }, 0, DisplayName = "[0] => 0")]
        [DataRow(new[] { 0, 2 }, 2, DisplayName = "[0, 2] => 2")]
        [DataRow(new[] { 3, 2 }, 1, DisplayName = "[3, 2] => 1")]
        [DataRow(new[] { 1, 2, 3 }, 1, DisplayName = "[1, 2, 3] => 1")]
        [DataRow(new[] { 3, 2, 1 }, 1, DisplayName = "[3, 2, 1] => 1")]
        [DataRow(new[] { 3, 3, 3 }, 3, DisplayName = "[3, 3, 3] => 3")]
        [DataRow(new[] { 2, 4, 6, 8 }, 2, DisplayName = "[2, 4, 6, 8] => 2")]
        public void ShouldReturnCorrectGcdGivenArrayWithMoreThanOneElement(int[] array, int expectedGcd)
        {
            //Arrange
            //var array = new[] { 3 };

            //Act
            var result = array.FindGcd();

            //Assert
            result.Should().Be(expectedGcd);
        }
    }
}
