package com.mytestbed.java.treeserder;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

// https://youtu.be/suj1ro8TIVY?t=430

public class TreeSerializationDeserialization
{
    public static void main(String[] args)
    {
        TreeNode node = spawnTree();

        String result = serialize(node);

        System.out.println(result);

        TreeNode deserialized = deserialize(result);

        String result2 = serialize(deserialized);

        System.out.println(result2);
    }

    public static TreeNode spawnTree()
    {
        TreeNode root = new TreeNode(4);

        root.left = new TreeNode(2);
        root.left.left = new TreeNode(1);
        root.left.right = new TreeNode(3);

        root.right = new TreeNode(6);
        root.right.left = new TreeNode(5);
        root.right.right = new TreeNode(7);

        return root;
    }


    static class TreeNode
    {
        Integer value;
        TreeNode left;
        TreeNode right;

        TreeNode(Integer value)
        {
            this.value = value;
        }
    }

    public static String serialize(TreeNode root)
    {
        if (root == null)
            return "X,";

        String leftSerialized = serialize(root.left);
        String rightSerialized = serialize(root.right);

        return root.value + "," + leftSerialized + rightSerialized;
    }

    public static TreeNode deserialize(String serializedTree)
    {
        Queue<String> remainingNodes = new LinkedList<String>()
        {{
            addAll(Arrays.asList(serializedTree.split(",")));
        }};

        return deserializeHelper(remainingNodes);
    }

    private static TreeNode deserializeHelper(Queue<String> remainingNodes)
    {
        String nodeSerialized = remainingNodes.poll();
        if (nodeSerialized == null || nodeSerialized.equals("X")) return null;

        TreeNode node = new TreeNode(Integer.valueOf(nodeSerialized));
        node.left = deserializeHelper(remainingNodes);
        node.right = deserializeHelper(remainingNodes);

        return node;
    }
}
