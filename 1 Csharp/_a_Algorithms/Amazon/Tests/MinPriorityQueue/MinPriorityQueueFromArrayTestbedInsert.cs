﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.MinPriorityQueue
{
    [TestClass]
    public class MinPriorityQueueFromArrayTestbedInsert
    {
        [TestMethod]
        public void ShouldWorkGivenEmptyHeap()
        {
            //Arrange
            var queue = new MinPriorityQueueFromArray<int>();

            //Act
            queue.Insert(1);

            //Assert
            queue.Peek().Should().Be(1);
        }

        [TestMethod]
        public void ShouldReturnCorrectItemGivenInsertionToSingleItemHeap()
        {
            //Arrange
            var queue = new MinPriorityQueueFromArray<int>();
            queue.Insert(1);

            //Act
            queue.Insert(2);

            //Assert
            queue.Peek().Should().Be(1);
        }

        [TestMethod]
        public void ShouldReturnCorrectItemGivenInsertionToSingleItemHeap2()
        {
            //Arrange
            var queue = new MinPriorityQueueFromArray<int>();
            queue.Insert(2);

            //Act
            queue.Insert(1);

            //Assert
            queue.Peek().Should().Be(1);
        }
    }
}
