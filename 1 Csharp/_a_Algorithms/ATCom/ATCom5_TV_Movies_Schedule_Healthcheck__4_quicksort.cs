﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Testbed._a_Algorithms.Utilities;

namespace Testbed._a_Algorithms.ATCom
{
    [TestClass]
    public class MovieNight4Quicksort
    {
        public class Movie
        {
            public DateTime End { get; private set; }
            public DateTime Start { get; private set; }

            public Movie(DateTime start, DateTime end)
            {
                End = end;
                Start = start;
            }
        }

        public class MovieSpecialComparer : IComparer<Movie>
        {
            public int Compare(Movie x, Movie y)
            {
                if (ReferenceEquals(x, y)) return 0;
                if (ReferenceEquals(y, null)) return 1;
                if (ReferenceEquals(x, null)) return -1;

                var xStartVsYEnd = x.Start.CompareTo(y.End);
                var xStartVsYStart = x.Start.CompareTo(y.Start);

                var xStartContainedInY = xStartVsYStart >= 0 && xStartVsYEnd < 0; //   its perfectly ok if   y.End == x.Start
                if (xStartContainedInY) throw new MovieOverlapDetectedException();

                var xEndVsYEnd = x.End.CompareTo(y.End);
                var xEndVsYStart = x.End.CompareTo(y.Start);

                var xEndContainedInY = xEndVsYStart > 0 && xEndVsYEnd <= 0; //         its perfectly ok if   y.Start == x.End
                if (xEndContainedInY) throw new MovieOverlapDetectedException();

                var yStartContainedInX = xStartVsYStart <= 0 && xEndVsYEnd > 0; //     its perfectly ok if   x.End == y.Start
                if (yStartContainedInX) throw new MovieOverlapDetectedException();

                var yEndContainedInX = xStartVsYEnd < 0 && xEndVsYEnd >= 0; //         its perfectly ok if   x.Start == y.End
                if (yEndContainedInX) throw new MovieOverlapDetectedException();

                return xStartVsYStart;
            }
        }

        public class MovieOverlapDetectedException : Exception
        {
        }

        static public bool CanViewAll(Movie[] movies)
        {
            try
            {
                PQuickSort.ParallelQuickSort(movies, new MovieSpecialComparer()); //0
                return true;
            }
            catch (Exception ex)
            {
                for (; ex != null; ex = ex.InnerException)
                {
                    if (ex is MovieOverlapDetectedException)
                        return false;
                }

                throw;
            }

            //0 we prefer direct sort over orderby despite the fact that the original array gets mutated
            //  thats because we thusly avoid occupying O(n) extra memory just to sort the array
        }




        [TestMethod]
        public void CanViewAllShouldReturnTrueGivenEmptyListOfMovies()
        {
            //Arrange
            var movies1 = new Movie[] { };

            //Act
            var verdict = CanViewAll(movies1);

            //Assert
            verdict.Should().BeTrue();
        }

        [TestMethod]
        public void CanViewAllShouldReturnTrueGivenSingleItemList()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;
            var movies1 = new[]
            {
                new Movie(DateTime.Parse("1/1/2015 20:00", format), DateTime.Parse("1/1/2015 21:30", format))
            };

            //Act
            var verdict = CanViewAll(movies1);

            //Assert
            verdict.Should().BeTrue();
        }

        [TestMethod]
        public void CanViewAllShouldReturnTrueGivenNonOverlappingMovies()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;
            var movies1 = new[]
            {
                new Movie(DateTime.Parse("1/1/2015 20:00", format), DateTime.Parse("1/1/2015 21:30", format)),
                new Movie(DateTime.Parse("1/1/2015 23:10", format), DateTime.Parse("1/1/2015 23:30", format)),
                new Movie(DateTime.Parse("1/1/2015 21:30", format), DateTime.Parse("1/1/2015 23:00", format))
            };

            //Act
            var verdict = CanViewAll(movies1);

            //Assert
            verdict.Should().BeTrue();
        }

        [TestMethod]
        public void CanViewAllShouldReturnFalseGivenOverlappingMovies1()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;
            var movies2 = new[]
            {
                new Movie(DateTime.Parse("1/1/2015 20:00", format), DateTime.Parse("1/1/2015 21:30", format)),
                new Movie(DateTime.Parse("1/1/2015 21:15", format), DateTime.Parse("1/1/2015 21:45", format)), //overlap
                new Movie(DateTime.Parse("1/1/2015 21:30", format), DateTime.Parse("1/1/2015 23:00", format))
            };

            //Act
            var verdict = CanViewAll(movies2);

            //Assert
            verdict.Should().BeFalse();
        }

        [TestMethod]
        public void CanViewAllShouldReturnFalseGivenOverlappingMovies2()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;
            var movies3 = new[]
            {
                new Movie(DateTime.Parse("1/1/2015 21:15", format), DateTime.Parse("1/1/2015 21:45", format)),
                new Movie(DateTime.Parse("1/1/2015 20:00", format), DateTime.Parse("1/1/2015 21:30", format)), //overlap
                new Movie(DateTime.Parse("1/1/2015 21:30", format), DateTime.Parse("1/1/2015 23:00", format))
            };

            //Act
            var verdict = CanViewAll(movies3);

            //Assert
            verdict.Should().BeFalse();
        }

        [TestMethod]
        public void CanViewAllShouldReturnTrueGivenMillionsOfNonOverlappingMovies()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;
            var current = DateTime.Parse("1/1/2015 21:15", format);
            var movies = Enumerable.Range(0, 1_000_000).Select(x =>
            {
                var start = current;
                current = current.AddMinutes(5);
                return new Movie(start, current);
            }).ToArray();

            //Act
            var verdict = CanViewAll(movies);

            //Assert
            verdict.Should().BeTrue();
        }

        [TestMethod]
        public void CanViewAllShouldReturnFalseGivenMillionsOfOverlappingMovies()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;

            var current = DateTime.Parse("1/1/2015 21:15", format);
            var movies = Enumerable.Range(0, 1_000_000).Select(x =>
            {
                if (x == 650_000)
                {
                    return new Movie(DateTime.Parse("1/1/2015 21:15", format), DateTime.Parse("1/1/2015 21:30", format));
                }

                var start = current;
                current = current.AddMinutes(5);
                return new Movie(start, current);
            }).ToArray();

            //Act
            var verdict = CanViewAll(movies);

            //Assert
            verdict.Should().BeFalse();
        }
    }
}
