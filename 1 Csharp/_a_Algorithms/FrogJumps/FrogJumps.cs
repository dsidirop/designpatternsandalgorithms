﻿// A small frog wants to get to the other side of the road. The frog is currently located at position X and wants to
// get to a position greater than or equal to Y. The small frog always jumps a fixed distance, D.
// 
// Count the minimal number of jumps that the small frog must perform to reach its target.
// 
// Write a function:
// 
//   class Solution { public int Solution(int X, int Y, int D); }
// 
// that, given three integers X, Y and D, returns the minimal number of jumps from position X to a position equal to or greater than Y.
// 
// For example, given:
// 
// X = 10
// Y = 85
// D = 30
//
// the function should return 3, because the frog will be positioned as follows:
// 
// after the first jump, at position 10 + 30 = 40
// after the second jump, at position 10 + 30 + 30 = 70
// after the third jump, at position 10 + 30 + 30 + 30 = 100
//
// Write an efficient algorithm for the following assumptions:
// 
// X, Y and D are integers within the range[1..1, 000, 000, 000];
// X ≤ Y.

namespace Testbed._a_Algorithms.FrogJumps
{
    static public class FrogJumps
    {
        static public void Tester()
        {
            var s = new Solution();

            var r1 = s.solution(1, 1, 100);

            var r2 = s.solution(10, 20, 10);

            var r3 = s.solution(10, 20, 15);

            var r4 = s.solution(100, 200, 15);
        }

        public class Solution
        {
            public int solution(int X, int Y, int D)
            {
                if (X == Y)
                    return 0;
                var div = ((Y - X) / D);
                var mod = ((Y - X) % D);
                return div + (mod == 0 ? 0 : 1);
            }
        }
    }
}

