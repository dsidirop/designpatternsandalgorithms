﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.KadanesAlgorithm
{
    [TestClass]
    public class KadanesAlgorithmTestbedRed
    {
        [TestMethod]
        public void ShouldThrowNullArgumentExceptionGivenNullInputArray()
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => Amazon.KadanesAlgorithm.Calculate(null));

            //Assert
            action.Should().ThrowExactly<ArgumentNullException>().WithMessage("*'input'*");
        }

        [TestMethod]
        public void ShouldThrowArgumentExceptionGivenEmptyArray()
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => Amazon.KadanesAlgorithm.Calculate(new int[] {}));

            //Assert
            action.Should().ThrowExactly<ArgumentException>().WithMessage("*'input'*");
        }
    }
}
