﻿using System.Collections.Generic;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Testbed._a_Algorithms.LogEntriesStatisticsAnalyzer.Tests
{
    [TestClass]
    public class LogEntriesStatisticsGivenGreenEntriesShould : LogEntriesStatisticsTestbedBase
    {
        [TestMethod]
        public async Task ReturnZeroedOutResultsGivenEmptyLogFile()
        {
            var tempFilePath = Path.GetTempFileName();
            try
            {
                //Arrange
                var expectedStatsPerHour = Enumerable.Range(start: 0, count: 24).ToDictionary(x => (byte)x, y => new HourlyStats());
                var expectedGarbledLinesCount = 0;

                await File.WriteAllTextAsync(tempFilePath, @"", Encoding.ASCII);

                //Act
                var analysis = await new LogEntriesStatistics(new FileSystem()).CalculateErrorRatesAsync(tempFilePath);

                //Assert
                analysis.Should().BeEquivalentTo((expectedStatsPerHour, expectedGarbledLinesCount));
            }
            finally //Cleanup
            {
                if (!string.IsNullOrWhiteSpace(tempFilePath))
                {
                    File.Delete(tempFilePath);
                }
            }
        }

        [TestMethod]
        public async Task ReturnZeroErrorsAndOnlyOneEntryGivenAllGreenLogFile()
        {
            var tempFilePath = Path.GetTempFileName();
            try
            {
                //Arrange
                var expectedGarbledLinesCount = 0;
                var expectedStatsPerHour = Enumerable.Range(start: 0, count: 24).ToDictionary(x => (byte)x, y => new HourlyStats());
                expectedStatsPerHour[1].ErrorRate = 0;
                expectedStatsPerHour[1].ErrorsCount = 0;
                expectedStatsPerHour[1].EntriesCount = 1;

                await File.WriteAllTextAsync(tempFilePath, SpawnLogEntry(hour: 1, statusCode: HttpStatusCode.OK), Encoding.ASCII);

                //Act
                var analysis = await new LogEntriesStatistics(new FileSystem()).CalculateErrorRatesAsync(tempFilePath);

                //Assert
                analysis.Should().BeEquivalentTo((expectedStatsPerHour, expectedGarbledLinesCount));
            }
            finally //Cleanup
            {
                if (!string.IsNullOrWhiteSpace(tempFilePath))
                {
                    File.Delete(tempFilePath);
                }
            }
        }

        [TestMethod]
        public async Task ReturnProperAnalysisGivenHugeAllGreenLogFile()
        {
            var tempFilePath = Path.GetTempFileName();
            try
            {
                //Arrange
                var totalLogEntries = 1_000_000ul;

                await using (var fileStream = new FileStream(tempFilePath, FileMode.Create))
                await using (var streamWriter = new StreamWriter(fileStream, Encoding.ASCII))
                {
                    for (var i = 0ul; i < totalLogEntries; i++)
                    {
                        await streamWriter.WriteLineAsync(SpawnLogEntry(hour: 1, statusCode: HttpStatusCode.OK));

                        if (i % 10_000 == 0)
                        {
                            await streamWriter.FlushAsync();
                        }
                    }

                    await streamWriter.FlushAsync();
                }

                //Act
                var tokenSource = new CancellationTokenSource(50);
                var analysis = await new LogEntriesStatistics(new FileSystem()).CalculateErrorRatesAsync(tempFilePath, tokenSource: tokenSource);

                //Assert
                analysis.Should().BeNull();
            }
            finally //Cleanup
            {
                if (!string.IsNullOrWhiteSpace(tempFilePath))
                {
                    File.Delete(tempFilePath);
                }
            }
        }
    }
}
