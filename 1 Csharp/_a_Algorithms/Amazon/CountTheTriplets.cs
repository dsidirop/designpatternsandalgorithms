﻿//
//      https://practice.geeksforgeeks.org/problems/count-the-triplets/0
//      https://www.geeksforgeeks.org/count-triplets-such-that-one-of-the-numbers-can-be-written-as-sum-of-the-other-two/
//      https://www.youtube.com/watch?v=ZD7WxJ3O2XE
//
// Given an array of distinct integers count all the triplets for which the sum of
// any two elements in the array is equal to the value of another element in the array
//
// For each test case print the number of such triplets discovered
//
// Constraints
//
//       1 <=   T  <= 100
//       3 <=   N  <= 105
//       1 <= A[i] <= 106
//
// Complexities
//
//       Time   O(N^2)
//       Space  O(N)
//

using Ardalis.GuardClauses;
using System.Linq;

namespace Testbed._a_Algorithms.Amazon
{
    public class CountTheTriplets
    {
        static public int Calculate(int[] input)
        {
            Guard.Against.Null(input, nameof(input));

            var set = input.ToHashSet(); //for fast lookups later on
            Guard.Against.Negative(set.Count - input.Length, nameof(input)); //0

            if (input.Length <= 2)
                return 0;

            return (
                from xi in input.Select((x, i) => (x, i))
                from yi in input.Select((y, i) => (y, i))
                where xi.i < yi.i //guarantees that duplicate combinations get eradicated
                      && set.Contains(xi.x + yi.y)
                select 1
            ).Count();

            //0 the input is supposed to be comprised of unique integers
        }
    }
}
