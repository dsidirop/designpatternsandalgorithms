﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.RotateMatrixAntiClockwise90Degrees
{
    [TestClass]
    public class RotateTestbedGreen
    {
        [TestMethod]
        public void ShouldResultInEmptyMatrixGivenEmptyMatrix()
        {
            //Arrange
            var matrix = new int[0, 0];

            //Act
            Amazon.RotateSquareMatrixAntiClockwise90Degrees.Rotate(ref matrix);

            //Assert
            matrix.Should().BeEquivalentTo(new int[0, 0]);
        }

        [TestMethod]
        public void ShouldResultInSingleItemMatrixGivenSingleItemMatrix()
        {
            //Arrange
            var matrix = new int[1, 1] { { 1 } };

            //Act
            Amazon.RotateSquareMatrixAntiClockwise90Degrees.Rotate(ref matrix);

            //Assert
            matrix.Should().BeEquivalentTo(new int[1, 1] { { 1 } });
        }

        [TestMethod]
        public void ShouldResultIn2X2MatrixGiven2X2Matrix()
        {
            //Arrange
            var matrix = new int[2, 2]
            {
                { 1, 2 },
                { 3, 4 }
            };
            var expected = new int[2, 2]
            {
                { 2, 4 },
                { 1, 3 }
            };

            //Act
            Amazon.RotateSquareMatrixAntiClockwise90Degrees.Rotate(ref matrix);

            //Assert
            matrix.Should().BeEquivalentTo(expected);
        }

        [TestMethod]
        public void ShouldResultIn3X3MatrixGiven3X3Matrix()
        {
            //Arrange
            var matrix = new int[3, 3]
            {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 7, 8, 9 }
            };
            var expected = new int[3, 3]
            {
                { 3, 6, 9 },
                { 2, 5, 8 },
                { 1, 4, 7 }
            };

            //Act
            Amazon.RotateSquareMatrixAntiClockwise90Degrees.Rotate(ref matrix);

            //Assert
            matrix.Should().BeEquivalentTo(expected);
        }
    }
}
