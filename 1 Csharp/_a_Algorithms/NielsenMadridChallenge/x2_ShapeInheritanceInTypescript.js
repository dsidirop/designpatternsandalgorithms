"use strict";
// Show how we can implement a Shape interface in typescript which includes a single method GetNumberOfEdges()
// which as the name implies returns the number of edges in a shape and then implement this interface in two
// classes Pentagon and Triangle
Object.defineProperty(exports, "__esModule", { value: true });
var Pentagon = /** @class */ (function () {
    function Pentagon() {
    }
    Pentagon.prototype.GetNumberOfEdges = function () {
        return 5;
    };
    return Pentagon;
}());
exports.default = Pentagon;
var Triangle = /** @class */ (function () {
    function Triangle() {
    }
    Triangle.prototype.GetNumberOfEdges = function () {
        return 3;
    };
    return Triangle;
}());
exports.Triangle = Triangle;
//# sourceMappingURL=x2_ShapeInheritanceInTypescript.js.map