﻿// https://manski.net/2013/05/the-visitor-pattern-explained/

// ReSharper disable UnusedVariable
// ReSharper disable UnusedMember.Global
// ReSharper disable NotAccessedVariable

using System;
using System.Collections.Generic;

namespace Testbed._a_Algorithms.MaritimeOcean
{
    static public class FriendPathDetectorVisitorChallenge
    {
        static public void Tester()
        {
            var success1 = false;
            var success2 = false;
            var success3 = false;
            var success4 = false;

            {
                var a = new Friend("A");
                var b = new Friend("B");
                var c = new Friend("C");

                a.AddFriendship(b);
                b.AddFriendship(c);

                success1 = a.CanBeConnected(c);
            }

            {
                var a = new Friend("A");
                var b = new Friend("B");
                var c = new Friend("C");

                a.AddFriendship(b);
                //b.AddFriendship(c);

                success2 = !a.CanBeConnected(c);
            }

            {
                var a = new Friend("A");

                success3 = !a.CanBeConnected(a);
            }

            {
                var a = new Friend("A");
                var b = new Friend("B");
                var c = new Friend("C");
                var d = new Friend("D");

                a.AddFriendship(b);
                b.AddFriendship(c);
                d.AddFriendship(c);

                success4 = a.CanBeConnected(c);
            }
        }

        public class Friend : IVisitable
        {
            public string Email { get; private set; }

            public ICollection<Friend> Friends { get; private set; }

            public Friend(string email)
            {
                Email = email.Trim();
                Friends = new List<Friend>(128);
            }

            public void AddFriendship(Friend friend)
            {
                Friends.Add(friend);
                friend.Friends.Add(this);
            }

            public bool CanBeConnected(Friend target)
            {
                if (StringComparer.InvariantCultureIgnoreCase.Compare(Email, target.Email) == 0)
                    return false; //0

                var visitor = new FriendPathDetectorVisitor(target);
                Accept(visitor);

                return visitor.PathFound;

                //0 we obviously cant allow a person to link up with his/herself
            }

            public void Accept(IVisitor visitor)
            {
                visitor.Visit(this);
            }
        }

        public class FriendPathDetectorVisitor : IVisitor
        {
            public bool PathFound { get; private set; }

            private readonly Friend _target;

            public FriendPathDetectorVisitor(Friend target)
            {
                _target = target ?? throw new ArgumentException(nameof(target));
            }

            public void Visit<T>(T visitable) where T : Friend
            {
                if (PathFound)
                    return;

                if (StringComparer.InvariantCultureIgnoreCase.Compare(_target.Email, visitable.Email) == 0)
                {
                    PathFound = true;
                    return;
                }

                var newProspects = visitable.Friends;

                foreach (var friends in newProspects)
                {
                    friends.Accept(this);
                }
            }
        }

        public interface IVisitor
        {
            void Visit<T>(T visitable) where T : Friend;
        }

        public interface IVisitable
        {
            void Accept(IVisitor visitor);
        }
    }
}

