﻿// ReSharper disable UnusedMember.Global
//
// https://www.youtube.com/watch?v=QGVCnjXmrNg
//
// Given an array of numbers find the kth largest number (k is one based)
//
// Complexities
//
//    Time    
//    Space   
//

using Ardalis.GuardClauses;

namespace Testbed._a_Algorithms.Amazon
{
    public class KthLargestOrEqualNumber
    {
        static public int Calculate(in int[] numbers, in int k)
        {
            Guard.Against.Null(numbers, nameof(numbers)); //order
            Guard.Against.OutOfRange(numbers.Length, nameof(numbers), 1, int.MaxValue); //order
            Guard.Against.NegativeOrZero(k, nameof(k)); //order
            Guard.Against.OutOfRange(k, nameof(k), 1, numbers.Length); //order

            var length = numbers.Length;
            if (length == 1)
                return numbers[0];

            var left = 0;
            var right = length - 1;
            var desiredIndex = length - k; //k is one based so ...
            while (true)
            {
                var pos = PartitionSegmentAscending(numbers, left, right);
                if (Bingo(numbers, pos, desiredIndex)) return numbers[pos];
                
                if (pos < desiredIndex)
                {
                    left = pos + 1;
                }
                else
                {
                    right = pos - 1;
                }
            }
        }

        static private bool Bingo(in int[] numbers, int position, in int desiredIndex)
        {
            do
            {
                if (position == desiredIndex)
                    return true;
            } while (position > desiredIndex && numbers[position] == numbers[--position]);

            return false;
        }

        static private int PartitionSegmentAscending(in int[] input, in int leftStart, in int rightEnd)
        {
            var r = rightEnd;
            var l = leftStart + 1;
            var pivotValue = input[leftStart]; //we use the leftstart value as the pivot
            
            while (l <= r)
            {
                if (input[l] > pivotValue && input[r] <= pivotValue)
                    Swap(input, l++, r--);

                if (input[l] <= pivotValue)
                    l++;

                if (input[r] >= pivotValue)
                    r--;
            }

            Swap(input, leftStart, r); //0

            return r;

            //0 now that partitioning is complete l=r+1   on top of that we also know now that for the segment
            //
            //                                     [leftStart+1,rightEnd]
            //
            //  that we were given to partition the following is now true
            //  
            //        numbers[leftStart+1, leftStart+2, ..., l]   < pivotValue <   numbers[l+1, l+2, ..., rightEnd]
            //
            //  so to bring the pivotvalue in the middle of [leftStart+1,rightEnd] all we have to do is swap
            //  the initial pivot index (aka leftStart) with l and we are all done
        }

        static private void Swap(in int[] input, in int j, in int k)
        {
            if (j == k)
                return;

            var temp = input[j];
            input[j] = input[k];
            input[k] = temp;
        }
    }
}
