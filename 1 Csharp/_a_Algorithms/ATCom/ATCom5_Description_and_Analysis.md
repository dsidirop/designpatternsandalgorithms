Construct a VALIDATION mechanism for a list of movie schedules

The mechanism is given a list of movies  Each movie is coupled with
a datetime-range which denotes when the movie will be displayed on tv

If the mechanism detects even one pair of movies that overlap then it
returns false (=unhealthy movie-schedule) otherwise true (=healthy
movie schedule)

Emphasis is placed on making the algorithm performant in terms of
memory footprint and computation time

# Analysis

All approaches imply sorting the given list by start-datetime  In this
sense we should contemplate what kind of mechanism to employ to make
sorting efficient

From https://stackoverflow.com/a/2037401/863651  on SortedList vS SortedDictionary vs QuickSort

" SortedList:

Inserting an item requires a binary search (O(log(n)) to find the insertion point, then a List.Insert (O(n)) to insert the item.
The Insert() dominates, populating the list requires O(n^2). If the input items are already sorted then the Insert collapses to
O(1) but doesn't affect the search. Populating is now O(nlog(n)). You don't worry how big the Oh is, sorting first is always more
efficient. Assuming you can afford the doubled storage requirement.

SortedDictionary: Thats different it uses a red-black tree. Finding the insertion point requires O(log(n)). Rebalancing the tree
might be required afterwards, that also takes O(log(n)). Populating the dictionary thus takes O(nlog(n)). Using sorted input does
not change the effort to find the insertion point or rebalancing, it is still O(nlog(n)). Now the Oh matters though, inserting
sorted input requires the tree to constant rebalance itself. It works better if the input is random, you don't want sorted input.

So populating SortedList with sorted input and populating SortedDictionary with unsorted input is both O(nlog(n)). Ignoring the cost
of providing sorted input, the Oh of SortedList is smaller than the Oh of SortedDictionary. That's an implementation detail due to
the way List allocates memory. It only has to do so O(log(n)) times, a red-black tree has to allocate O(n) times. Very small Oh btw.

**Notable is that neither one compares favorably over simply populating a List, then calling Sort(). That's also O(nlog(n)).**
**In fact, if input is already accidentally sorted you can bypass the Sort() call, this collapses to O(n). The cost analysis now needs**
**to move to the effort it takes to get the input sorted. It is hard to bypass the fundamental complexity of Sort(), O(nlog(n)).**
**It might not be readily visible, you might get the input sorted by, say, a SQL query. It will just take longer to complete.**

Moral: The point of using either SortedList or SortedDictonary is to keep the collection sorted after inserts. **If you only worry about populating but not mutating then you shouldn't use those collections.** "

The closing sentence is an observation that's key to our decision making. Indeed we don't need to change
the resulting sorted collection.

## Approach 0   Tree-Dictionary (from Standard Lib) with Bespoke Comparator

An alternative approach would be to replace quicksort with a treebased
autosorted data structure like SortedDictionary   if we add nodes
one by one we will be able to tell whether we have a collision and thus
failfast   using this approach allocating double the memory becomes the
worst case scenario instead of being a baseline phenomenon like in scenario 0
but still this approach is always worst memory wise compared to case 1

    pros -> memory footprint   O(n/2) on average
    pros -> computational cost O(n*log(n)) -> blazing fast

    cons -> doesnt capitalize on multicpu setups (bah)


## Approach 1   Tree-Dictionary (from the nuget of the C5-lib) with Bespoke Comparator

This improves a bit upon Approach 2 in the sense that C5 SortedDictionary
supports adding groups of say 5 sorted keys in one go thus reducing the need
to rebalance the underlying red black tree in each and every insertion

However this still doesnt achieve parity with approach #3


## Approach 1a  Multi-CPU Tree-Dictionary (from the nuget of the C5-lib) with Bespoke Comparator

A two stage approach

Stage 1   Like approach 1 but now we essentially partition the given dataset to N regions
Stage 1   (where N is the number of cpus) and then search for collisions in parallel by
Stage 1   feeding the given periods into sorted-trees

If stage 1 doesnt uncover any collisions then and only then we move to stage 2

Stage 2   We cross check the trees for collisions  We can speed this up by taking into account
Stage 2   the minimums and maximums of the trees   Stage 2 is obviously fully parallelizable as
Stage 2   well which is always good

## Approach 2   Straightforward Single-Thread List.OrderBy(new BespokeComparatorHere()).ToArray()

A straightforward approach would be to .sort by starting-datetime property
while making sure that the comparer will throw a tantrum during sorting
by means of an exception when it detects that two periods do in fact overlap

The orderby implementation at the time of this writing resorts to
allocating an entire array and applies a special quicksort on it

This is the approach shown below

    pros -> computational cost O(n*log(n)) -> blazing fast

    cons -> doesnt capitalize on multicpu setups (bah)
    cons -> memory footprint   O(n)        -> temporarily doubles the memory footprint
                                           -> due to the shallow cloning of the original array

## Approach 3   Straightforward Single-Thread List/Array.Sort(new BespokeComparatorHere())

This is like approach #0 above with all its pros and only one of its con because sorting
is applied directly on the original list so we don't double our memory footprint

    pros -> memory footprint     almost O(1)
    pros -> computational cost   O(n*log(n)) -> blazing fast

    cons -> doesnt capitalize on multicpu setups (bah)


## Approach 4   CPU-Parallelized variant of List/Array.Sort(new BespokeComparatorHere())

https://gist.github.com/wieslawsoltes/6592526
http://www.jiweix.com/parallel-quicksort-in-c/

Internet wisdom suggests that this approach practically increases performance by 1.7x+


## Approach 5   GPU-parallelized variant of Sort (probably Radix sort)

This is probably the best approach of all for hefty datasets with 1million records or more

Internet wisdom suggests that the performance boost is around 10x that of multicpu as far
as sorting is concerned

We will need a C++ library for this to work like for instance

        https://github.com/mark-poscablo/gpu-radix-sort/tree/master/radix_sort

Or something closer to C# like Hybridizer

        https://devblogs.nvidia.com/hybridizer-csharp/

A question here is how we could break early in the case that we detect overlapping date ranges
But that's the least of our problems  Getting these libraries to even sort in the first place
will be a pain in its own right


## Approach 6   Using a prefix-sum array   [ mem: O((max-min)/gcd)  /  cpu: O(max-min+n) ]

https://www.geeksforgeeks.org/check-if-any-two-intervals-overlap-among-a-given-set-of-intervals/

1. Find the overall maximum element    Let it be max_ele
2. Initialize an array of size max_ele with 0
3. For every interval [start, end] increment the value at index start
   ie arr[start]++ and decrement the value at index (end + 1) ie arr[end + 1]--
4. Compute the prefix sum of this array (arr[])
5. Every index i of this prefix sum array will tell how many times i has occurred
   in all the intervals taken together   If this value is greater than 1 then it
   occurs in 2 or more intervals
6. So simply initialize the result variable as false and while traversing the prefix
   sum array change the result variable to true whenever the value at that index is
   greater than 1

Time Complexity   O(max-min + n)

This method is more efficient than the quicksort method if:
- the number of intervals is in the range of millions entries
- and at the same time max-min value among all intervals is low

since time complexity is directly proportional to O(max-min).

       public class Interval  
       {
           public int start { get; set; }
           public int end { get; set; }

           public Interval(int start, int end)  
           {
                this.start = start;
                this.end = end;
           }
       }

       static bool isOverlap(Interval[] arr, int n)
       {
           int min_ele = 0;
           int max_ele = 0;
           for (int i = 0; i < n; i++) // Find the overall maximum element
           {
               min_ele = Math.Min(min_ele, arr[i].start);
               max_ele = Math.Max(max_ele, arr[i].end);
           }  

           int[] aux = new int[max_ele - min_ele + 1]; // Initialize an array of size max_ele
           for (int i = 0; i < n; i++)
           {
               int x = arr[i].start - min_ele; // starting point of the interval
               int y = arr[i].end - min_ele;   // end point of the interval
               aux[x]++;
               aux[y]--;
           }  

           for (int i = 1; i < aux.length; i++) 
           {
               aux[i] += aux[i - 1]; // Calculating the prefix sum
               if (aux[i] > 1) // Overlap
                   return true;
           }  

           return false; // If we reach here then no overlap
       }


# Verdict

Ideally we would have liked to employ Approach 5 (GPU-Parallelized Radix Sort) for Amazon-scale
projects.

For all practical applications we can settle for Approach 6w.


