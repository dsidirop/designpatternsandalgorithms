﻿// ReSharper disable UnusedVariable
// ReSharper disable UnusedMember.Global

using System;
using System.Linq;

namespace Testbed._a_Algorithms.FnzCodilityChallenge
{
    static public class x2_FindSentenceWithGreatestWordCount
    {
        static public void Tester()
        {
            var success0 = false;
            var success1 = false;
            var success2 = false;
            var success3 = false;
            var success4 = false;

            {
                var result = solution(@"We test coders. Give us a try?");
                success0 = result == 4;
            }

            {
                var result = solution(@"Forget CVs..Save time . x x");
                success1 = result == 2;
            }

            {
                var result = solution(@" . ? ! .... !!!! ???");
                success2 = result == 0;
            }

            {
                var result = solution(@"");
                success3 = result == 0;
            }

            {
                var result = solution(@"         ");
                success4 = result == 0;
            }
        }

        static public int solution(string input)
        {
            return input
                .Split(new[] {'?', '!', '.'}, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries).Length)
                .DefaultIfEmpty(0) //vital for properly handling empty text   otherwise max() throws an exception
                .Max();
        }
    }
}


