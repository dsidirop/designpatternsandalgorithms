﻿// Implement a function FolderNames which accepts a string containing an XML file that specifies folder structure and
// returns all folder names that start with startingLetter. The XML format is given in the example below.
// For example, for the letter 'u' and XML file:

//<?xml version = "1.0" encoding= "UTF-8" ?>
//< folder name = "c" >
//    < folder name= "program files" >
//        < folder name= "uninstall information" />
//    </ folder >
//    < folder name= "users" />
//</ folder >

//the function should return "uninstall information" and "users" (in any order).


using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace Testbed._a_Algorithms.XmlFolderNames
{
    static public class XmlFolderNames
    {
        static public void Tester()
        {
            const string xml =
                @"<?xml version=""1.0"" encoding=""UTF-8""?>" +
                @"<folder name=""c"">" +
                @"       <folder name=""program files"">" +
                @"          <folder name=""uninstall information"" />" +
                @"       </folder>" +
                @"       <folder name=""users"" />" +
                @"</folder>";

            foreach (var name in FolderNames(xml, 'u'))
            {
                Console.WriteLine(name);
            }
        }

        static public IEnumerable<string> FolderNames(string xml, char startingLetter)
        {
            var xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.LoadXml(xml);
            }
            catch
            {
                yield break;
            }

            var s = startingLetter.ToString();
            var extractedNodes = xmlDoc.SelectNodes("//folder");
            if (extractedNodes == null) yield break;

            foreach (var extractedNode in extractedNodes.Cast<XmlElement>())
            {
                var folderName = extractedNode.GetAttribute("name");
                if (folderName.StartsWith(s))
                {
                    yield return folderName;
                }
            }
        }
    }
}