﻿// Friend Groups
//
// Write a function that given a list of relationships outputs the contents of each friend group
// A relationship is a ‘friendship’ between two people
//
// example input:
//
//    {"rob", "nick"}
//    {"tyler", "scott"}
//    {"rob", "katia"}
//    {"aaliyara", "katia"}
//    {"james", "scott"}
//
// would output:
//
//    {"rob", "nick", "katia", "aaliyara"}
//    {"james", "scott", "tyler"}
//
// canon solution derived from:
//
//    https://www.geeksforgeeks.org/connected-components-in-an-undirected-graph/
//

using System;
using System.Collections.Generic;
using System.Linq;

namespace Testbed._a_Algorithms.DiscoverAllDisconnectedFriendNetworkGraphs
{
    public class FriendsNetworkGraphs_RecursiveDfsTraversal
    {
        private readonly Dictionary<string, HashSet<string>> _nodes;
 
        public FriendsNetworkGraphs_RecursiveDfsTraversal()
        {
            _nodes = new Dictionary<string, HashSet<string>>(CaseInsensitiveComparer);
        }

        public FriendsNetworkGraphs_RecursiveDfsTraversal InitGraph(IEnumerable<(string friend1, string friend2)> friendRelations)
        {
            foreach (var (friend1, friend2) in friendRelations)
            {
                AddOneWayFriendshipImpl(friend1, friend2);
                AddOneWayFriendshipImpl(friend2, friend1);
            }

            return this;
        }

        public List<HashSet<string>> GetDistinctNetworks()
        {
            var visited = new HashSet<string>(CaseInsensitiveComparer);

            var results = _nodes
                .Keys
                .Where(x => !visited.Contains(x))
                .Select(x => DfsGraphCrawlerStream(x, visited).ToHashSet(CaseInsensitiveComparer))
                .ToList();

            return results;
        }

        private void AddOneWayFriendshipImpl(string friend1, string friend2)
        {
            if (_nodes.TryGetValue(friend1, out var srcFriends))
            {
                srcFriends.Add(friend2);
            }
            else
            {
                _nodes[friend1] = new HashSet<string> {friend2};
            }
        }

        private IEnumerable<string> DfsGraphCrawlerStream(string friend, ISet<string> visited)
        {
            visited.Add(friend);

            yield return friend;

            var neighboors = _nodes[friend];

            var friendsOfFriendsStream = neighboors
                .Where(x => !visited.Contains(x))
                .SelectMany(x => DfsGraphCrawlerStream(x, visited));

            foreach (var f in friendsOfFriendsStream)
            {
                yield return f;
            }
        }

        static private readonly IEqualityComparer<string> CaseInsensitiveComparer = StringComparer.InvariantCultureIgnoreCase;
    }
}