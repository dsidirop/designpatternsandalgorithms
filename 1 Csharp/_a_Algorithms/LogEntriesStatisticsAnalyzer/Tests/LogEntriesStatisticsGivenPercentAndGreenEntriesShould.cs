﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Testbed._a_Algorithms.LogEntriesStatisticsAnalyzer.Tests
{
    [TestClass]
    public class LogEntriesStatisticsGivenPercentAndGreenEntriesShould : LogEntriesStatisticsTestbedBase
    {
        [TestMethod]
        public async Task ReturnZeroedOutResultsGivenEmptyLogFile()
        {
            var tempFilePath = Path.GetTempFileName();
            try
            {
                //Arrange
                var expectedStatsPerHour = Enumerable.Range(start: 0, count: 24).ToDictionary(x => (byte)x, y => new HourlyStats());
                var expectedGarbledLinesCount = 0;

                await File.WriteAllTextAsync(tempFilePath, @"", Encoding.ASCII);

                //Act
                var analysis = await new LogEntriesStatistics(new FileSystem()).CalculateErrorRatesAsync(tempFilePath, samplePercent: 1);

                //Assert
                analysis.Should().BeEquivalentTo((expectedStatsPerHour, expectedGarbledLinesCount));
            }
            finally //Cleanup
            {
                if (!string.IsNullOrWhiteSpace(tempFilePath))
                {
                    File.Delete(tempFilePath);
                }
            }
        }

        [DataTestMethod]
        [DataRow(0_001_000L, 0.5, 0.1, 2.00)]
        [DataRow(0_001_000L, 0.5, 0.2, 2.00)]
        [DataRow(0_001_000L, 0.5, 0.3, 2.00)]
        [DataRow(0_001_000L, 0.5, 0.4, 2.00)]
        [DataRow(0_001_000L, 0.5, 0.5, 2.00)]
        [DataRow(0_001_000L, 0.5, 0.6, 2.00)]
        [DataRow(0_001_000L, 0.5, 0.7, 2.00)]
        [DataRow(0_001_000L, 0.5, 0.8, 2.00)]
        [DataRow(0_001_000L, 0.5, 0.9, 2.00)] //note that error rates swing wildly once we get to around 1mil log entries
        //[DataRow(1_000_000L, 0.5, 1.0, 0.00, 20)]
        //[DataRow(1_000_000L, 0.5, null, 0.0, 20)]
        public async Task ReturnProperAnalysisGivenHugeAllGreenLogFile(
            long totalLogEntries,
            double errorRate,
            double? samplePercent,
            double deltaTolerance,
            int numberOfIterations = 50
        )
        {
            //Arrange
            var action = new Func<Task>(async () =>
                {
                    var tempFilePath = Path.GetTempFileName();
                    try
                    {
                        //Arrange
                        var targetHour = (byte)1;
                        var expectedStatsPerHour = Enumerable.Range(start: 0, count: 24).ToDictionary(x => (byte)x, y => new HourlyStats());
                        expectedStatsPerHour[targetHour].ErrorRate = errorRate;
                        expectedStatsPerHour[targetHour].ErrorsCount = (long)(errorRate * (samplePercent ?? 1) * totalLogEntries);
                        expectedStatsPerHour[targetHour].EntriesCount = (long)((samplePercent ?? 1) * totalLogEntries);

                        await using (var fileStream = new FileStream(tempFilePath, FileMode.Create))
                        await using (var streamWriter = new StreamWriter(fileStream, Encoding.ASCII))
                        {
                            for (var i = 0L; i < totalLogEntries; i++)
                            {
                                await streamWriter.WriteLineAsync(SpawnLogEntry(
                                    hour: targetHour,
                                    statusCode: i < expectedStatsPerHour[targetHour].ErrorsCount
                                        ? HttpStatusCode.OK
                                        : HttpStatusCode.InternalServerError
                                ));

                                if (i % 10_000 == 0)
                                {
                                    await streamWriter.FlushAsync();
                                }
                            }

                            await streamWriter.FlushAsync();
                        }

                        //Act
                        var analysis = await new LogEntriesStatistics(new FileSystem()).CalculateErrorRatesAsync(tempFilePath, samplePercent);

                        //Assert
                        analysis.Should().NotBeNull();

                        analysis.Value.GarbledLinesCount.Should().Be(0);

                        analysis.Value.Stats
                            .Where(x => x.Key != targetHour)
                            .Should()
                            .BeEquivalentTo(expectedStatsPerHour.Where(x => x.Key != targetHour));

                        var entriesCountDelta = expectedStatsPerHour[targetHour].EntriesCount - analysis.Value.Stats[targetHour].EntriesCount;
                        entriesCountDelta
                            .Should()
                            .BeInRange(-1000, +1000);

                        var errorsCountDelta = expectedStatsPerHour[targetHour].ErrorsCount - analysis.Value.Stats[targetHour].ErrorsCount;
                        errorsCountDelta
                            .Should()
                            .BeInRange(
                                -(long)(deltaTolerance * expectedStatsPerHour[targetHour].ErrorsCount),
                                +(long)(deltaTolerance * expectedStatsPerHour[targetHour].ErrorsCount)
                            );

                        var errorRateDelta = expectedStatsPerHour[targetHour].ErrorRate - analysis.Value.Stats[targetHour].ErrorRate;
                        errorRateDelta
                            .Should()
                            .BeInRange(-deltaTolerance, +deltaTolerance);
                    }
                    finally //Cleanup
                    {
                        if (!string.IsNullOrWhiteSpace(tempFilePath))
                        {
                            File.Delete(tempFilePath);
                        }
                    }
                }
            );

            //Act
            for (var i = 0; i < numberOfIterations; i++)
            {
                await action.Invoke();
            }

            //Assert
            //nothing to do
        }
    }
}