package Individual

type IIndividual interface {
	GetName() string
	EnsureFriendship(otherGuy IIndividual) IIndividual
	IsFriendsWith(name IIndividual) bool
}

type Individual struct {
	Name    string
	Friends map[string]IIndividual
}

func Spawn(name string) IIndividual {
	return &Individual{
		Name:    name,
		Friends: make(map[string]IIndividual, 0),
	}
}

func (i Individual) GetName() string {
	return i.Name
}

func (i *Individual) IsFriendsWith(otherDude IIndividual) bool {
	_, exists := i.Friends[otherDude.GetName()]
	return exists
}

func (i *Individual) EnsureFriendship(otherDude IIndividual) IIndividual {
	_, alreadyFriends := i.Friends[otherDude.GetName()]
	if alreadyFriends {
		return i
	}

	i.Friends[otherDude.GetName()] = otherDude
	return i
}
