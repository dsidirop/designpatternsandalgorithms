﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.NumberOfPerfectSquares
{
    [TestClass]
    public class KthLargestNumberTestbedGreen
    {
        [TestMethod]
        [DataRow(00, (uint) 1)]
        [DataRow(01, (uint) 1)]
        [DataRow(02, (uint) 2)]
        [DataRow(03, (uint) 3)]
        [DataRow(04, (uint) 1)]
        [DataRow(05, (uint) 2)]
        [DataRow(06, (uint) 3)]
        [DataRow(07, (uint) 4)]
        [DataRow(08, (uint) 2)]
        [DataRow(09, (uint) 1)]
        [DataRow(10, (uint) 2)]
        [DataRow(11, (uint) 3)]
        [DataRow(12, (uint) 3)]
        public void ShouldReturnEmptyArrayGivenEmptyStockPricesArray(in int number, in uint expectedResult)
        {
            //Arrange
            //...

            //Act
            var result = MinNumberOfPerfectSquares.Calculate(number);

            //Assert
            result.Should().Be(expectedResult);
        }
    }
}
