﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.MinimumNumberOfPlatforms
{
    [TestClass]
    public class MinimumNumberOfPlatforms
    {
        [TestMethod]
        public void ShouldThrowNullArgumentExceptionGivenNullSchedules()
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => Amazon.MinimumNumberOfPlatforms.Calculate(null));

            //Assert
            action.Should().ThrowExactly<ArgumentNullException>().WithMessage("*'schedule'*");
        }
    }
}
