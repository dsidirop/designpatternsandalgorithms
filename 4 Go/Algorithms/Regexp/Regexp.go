package main

import (
	"fmt"
	"regexp"
)

func main() {
	// Compile the expression once usually at init time
	// Use `` verbatim-strings to avoid having to quote the backslashes
	var validID = regexp.MustCompile(`^[a-z]+\[[0-9]+]$`)

	fmt.Println(validID.MatchString("adam[23]"))
	fmt.Println(validID.MatchString("eve[7]"))
	fmt.Println(validID.MatchString("Job[48]"))
	fmt.Println(validID.MatchString("snakey"))

	//matching groups
	compiledRegexp := regexp.MustCompile(`(?P<Year>\d{4})-(?P<Month>\d{2})-(?P<Day>\d{2})`)
	fmt.Printf("** FindStringSubmatch: %#v\n", compiledRegexp.FindStringSubmatch(`2015-05-27`))
	fmt.Printf("** SubexpNames: %#v\n", compiledRegexp.SubexpNames())

	matchingGroups := getRegexpGroups(compiledRegexp, `2015-05-27`)
	fmt.Printf("** MatchingGroups: %#v\n", matchingGroups)
}

func getRegexpGroups(compiledRegEx *regexp.Regexp, input string) (matchingGroups map[string]string) {
	match := compiledRegEx.FindStringSubmatch(input)
	lenMatch := len(match)

	matchingGroups = make(map[string]string)
	for i, name := range compiledRegEx.SubexpNames() {
		if i > 0 && i <= lenMatch {
			matchingGroups[name] = match[i]
		}
	}
	return matchingGroups
}
