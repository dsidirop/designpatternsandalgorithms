﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// https://gist.github.com/wieslawsoltes/6592526

namespace Testbed._a_Algorithms.Utilities
{
    [TestClass]
    public class PQuickSort
    {
        [TestMethod]
        public void ShouldSortGivenShortArrayOfNumbers()
        {
            //Arrange
            var arr = new[] { 3, 2, 1 };
            var expected = new List<int>(arr).OrderBy(x => x).ToArray();

            //Act
            ParallelQuickSort(arr);

            //Assert
            arr.Should().BeEquivalentTo(expected, options => options.WithStrictOrdering());
        }

        [TestMethod]
        public void ShouldSortWithExplicitComparatorGivenShortArrayOfNumbers()
        {
            //Arrange
            var arr = new[] { 3, 2, 1 };
            var comparer = Comparer<int>.Default;
            var expected = new List<int>(arr).OrderBy(x => x).ToArray();

            //Act
            ParallelQuickSort(arr, comparer);

            //Assert
            arr.Should().BeEquivalentTo(expected, options => options.WithStrictOrdering());
        }

        [TestMethod]
        public void ShouldSortDescComparatorGivenShortArrayOfNumbers()
        {
            //Arrange
            var arr = new[] { 3, 2, 1 };
            var expected = new List<int>(arr).OrderByDescending(x => x).ToArray();

            //Act
            ParallelQuickSort(arr, ascOrDesc: false);

            //Assert
            arr.Should().BeEquivalentTo(expected, options => options.WithStrictOrdering());
        }

        [TestMethod]
        public void ShouldSortDescComparatorGivenLongArrayOfNumbers()
        {
            //Arrange
            var arr = Enumerable.Range(0, 1_000_000).Reverse().ToArray();
            var expected = new List<int>(arr).OrderByDescending(x => x).ToArray();

            //Act
            ParallelQuickSort(arr, ascOrDesc: false);

            //Assert
            arr.Should().BeEquivalentTo(expected, options => options.WithStrictOrdering());
        }

        [TestMethod]
        public void ShouldSortGivenLongArrayOfNumbers()
        {
            //Arrange
            var arr = Enumerable.Range(0, 1_000_000).Reverse().ToArray();
            var expected = new List<int>(arr).OrderBy(x => x).ToArray();

            //Act
            ParallelQuickSort(arr);

            //Assert
            arr.Should().BeEquivalentTo(expected, options => options.WithStrictOrdering());
        }

        [TestMethod]
        public void ShouldBubbleExceptionGivenErroneousComparator()
        {
            //Arrange
            var arr = Enumerable.Range(0, 1_000_000).Reverse().ToArray();

            //Act
            var action = new Action(() => ParallelQuickSort(arr, new DumComparator((int) (1_000_000 * 1.5))));

            //Assert
            action.Should().Throw<DumException>();
        }

        public class DumComparator : IComparer<int>
        {
            private int _callsCount;
            private readonly int _exceptionThreshold;

            public DumComparator(int exceptionThreshold)
            {
                _exceptionThreshold = exceptionThreshold;
            }

            public int Compare(int x, int y)
            {
                if (++_callsCount == _exceptionThreshold)
                    throw new DumException();

                return x.CompareTo(y);
            }
        }

        public class DumException : Exception
        {
        }

        #region public

        static public void InsertionSort<T>(T[] array, int from, int to, IComparer<T> comparer, sbyte ascOrDescValue) //0 insertionsort is better than quicksort for small arrays!
        {
            // ReSharper disable TooWideLocalVariableScope
            var j = (int)0;
            var a = default(T);
            for (var i = from + 1; i < to; i++)
            {
                j = i - 1;
                a = array[i];
                while (j >= from && comparer.Compare(array[j], a) == ascOrDescValue)
                {
                    array[j + 1] = array[j];
                    j--;
                }

                array[j + 1] = a;
            }

            //0 insertion sort is faster for small n because quicksort has extra overhead from the recursive function
            //  calls insertion sort is also more stable than quicksort and requires less memory   unbelievable but true
            //  ReSharper restore TooWideLocalVariableScope
        }

        static public void ParallelQuickSort<T>(T[] array, IComparer<T> comparer = null, bool ascOrDesc = true) => ParallelQuickSort(
            to: array.Length,
            from: 0,
            array: array,
            comparer: comparer ?? Comparer<T>.Default,
            ascOrDescValue: (sbyte)(ascOrDesc ? 1 : -1),
            depthRemaining: (int)Math.Log(Environment.ProcessorCount, 2) + 4
        );

        #endregion

        #region private

        static private void ParallelQuickSort<T>(T[] array, int from, int to, int depthRemaining, IComparer<T> comparer, sbyte ascOrDescValue)
        {
            if (to - from <= Threshold)
            {
                InsertionSort(array, from, to, comparer, ascOrDescValue);
                return;
            }

            var pivot = from + ((to - from) / 2); //0
            pivot = Partition(array, from, to, pivot, comparer, ascOrDescValue);
            if (depthRemaining > 0)
            {
                Parallel.Invoke(
                    () => ParallelQuickSort(array, from, pivot, depthRemaining - 1, comparer, ascOrDescValue),
                    () => ParallelQuickSort(array, pivot + 1, to, depthRemaining - 1, comparer, ascOrDescValue)
                );
                return;
            }

            ParallelQuickSort(array, from, pivot, 0, comparer, ascOrDescValue);
            ParallelQuickSort(array, pivot + 1, to, 0, comparer, ascOrDescValue);

            //0 could be anything use middle
        }

        static private int Partition<T>(IList<T> array, int from, int to, int pivot, IComparer<T> comparer, sbyte ascOrDescValue) //0
        {
            var arrayPivot = array[pivot]; //1
            Swap(array, pivot, to - 1); //2

            var newPivot = from; //3
            for (var i = from; i < to - 1; i++) //4
            {
                if (comparer.Compare(array[i], arrayPivot) == ascOrDescValue) //5
                    continue;

                Swap(array, newPivot, i); //6
                newPivot++;
            }

            Swap(array, newPivot, to - 1); //7
            return newPivot; //8

            //0 precondition  from <= pivot < to   other than that pivot is arbitrary
            //1 pivot value
            //2 move pivot value to end for now   after this pivot not used
            //3 new pivot
            //4 be careful to leave pivot value at the end
            //
            //5 invariant
            //
            //                         from <= newpivot <= i < to - 1
            //                && forall newpivot < j <= i, array[j] > arraypivot
            //                && forall from <= j <= newpivot, array[j] <= arraypivot
            //
            //6 move value smaller than arraypivot down to newpivot
            //7 move pivot value to its final place
            //
            //8 new pivot
            //
            //  postcondition    forall i <= newpivot, array[i] <= array[newpivot] && forall i > ...
        }

        static private void Swap<T>(IList<T> array, int i, int j)
        {
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }

        private const int Threshold = 128; // array length to use InsertionSort instead of SequentialQuickSort

        #endregion
    }
}