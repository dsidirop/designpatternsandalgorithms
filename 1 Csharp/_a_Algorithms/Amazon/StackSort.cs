﻿// ReSharper disable UnusedMember.Global
//
// Sort a stack   You can use another stack as a buffer
//
// Negotiation
//
// - When sorted should the largest element be at the top or bottom? Top
// - Can we have duplicate values like 5, 5?  Yes
// - Can we assume we already have a stack class that can be used for this problem? Yes
// - Can we assume this fits memory? Yes
//
// Our buffer will hold elements in reverse sorted order smallest at the top
// Store the current top element in a temp variable
// While stack is not empty
// While buffer is not empty or buffer top is > than temp
// Move buffer top to stack
// Move temp to top of buffer
// Return buffer
//
// Complexity:
//    Time: O(n^2)
//    Space: O(n)

using System.Collections.Generic;
using System.Linq;

namespace Testbed._a_Algorithms.Amazon
{
    public class StackSort<T> : Stack<T>
    {
        private readonly Comparer<T> _comparer;

        public StackSort()
        {
            _comparer = Comparer<T>.Default;
        }

        public IEnumerable<T> Sort()
        {
            var buffer = new Stack<T>();
            for (var temp = default(T); this.Any(); buffer.Push(temp))
            {
                temp = Pop();
                while (buffer.Any() && _comparer.Compare(temp, buffer.Peek()) == -1)
                {
                    Push(buffer.Pop());
                }
            }

            return buffer;
        }
    }
}