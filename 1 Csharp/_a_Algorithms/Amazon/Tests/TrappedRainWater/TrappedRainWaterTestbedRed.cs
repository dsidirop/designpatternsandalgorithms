﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.TrappedRainWater
{
    [TestClass]
    public class TreeContentsEqualTestbedRed
    {
        [TestMethod]
        public void ShouldThrowNullArgumentExceptionGivenNullInputArray()
        {
            //Arrange
            //...

            //Act
            var action = new Action(() => FindMissingNumberInArray.Calculate(null));

            //Assert
            action.Should().ThrowExactly<ArgumentNullException>().WithMessage("*'input'*");
        }
    }
}
