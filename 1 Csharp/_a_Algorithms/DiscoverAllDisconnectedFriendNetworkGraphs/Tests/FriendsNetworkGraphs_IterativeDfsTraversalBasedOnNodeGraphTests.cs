﻿using System;
using System.Collections.Generic;
using System.Linq;

using FluentAssertions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.DiscoverAllDisconnectedFriendNetworkGraphs.Tests
{
    [TestClass]
    public class FriendsNetworkGraphs_IterativeDfsTraversalBasedOnNodeGraphBasedOnNodeGraph_Tests
    {
        [TestMethod]
        [DataRow(1)]
        [DataRow(null)]
        public void ShouldReturnEmptyResults_GivenEmptyInput(int? workersCount)
        {
            //Arrange
            var results = (List<HashSet<string>>) null;
            var emptyFriends = Enumerable.Empty<(string friend1, string friend2)>().ToList(); // (friend1: "", friend2: "")
            var graphExplorer = new FriendsNetworkGraphs_IterativeDfsTraversalBasedOnNodeGraph().InitGraph(emptyFriends);

            //Act
            var action = new Action(() => results = graphExplorer.GetDistinctNetworks(workersCount: workersCount));

            //Assert
            action
                .Should()
                .NotThrow();

            results
                .Should()
                .BeEmpty();
        }

        [TestMethod]
        [DataRow(1)]
        [DataRow(null)]
        public void ShouldReturnSingleResult_GivenSimpleRelationship(int? workersCount)
        {
            //Arrange
            var results = (List<HashSet<string>>) null;
            var expected = new List<HashSet<string>> {new[] {"rob", "nick"}.ToHashSet()};
            var connections = new List<(string friend1, string friend2)>
            {
                (friend1: "rob", friend2: "nick"),
            };
            var graphExplorer = new FriendsNetworkGraphs_IterativeDfsTraversalBasedOnNodeGraph().InitGraph(connections);

            //Act
            var action = new Action(() => results = graphExplorer.GetDistinctNetworks(workersCount: workersCount));

            //Assert
            action
                .Should()
                .NotThrow();

            results
                .Should()
                .BeEquivalentTo(expected);
        }

        [TestMethod]
        [DataRow(1)]
        [DataRow(null)]
        public void ShouldReturnTwoResults_GivenTwoSeparateSimpleRelationships(int? workersCount)
        {
            //Arrange
            var results = (List<HashSet<string>>) null;
            var expected = new List<HashSet<string>>
            {
                new[] {"rob", "nick"}.ToHashSet(),
                new[] {"foo", "bar"}.ToHashSet(),
            };
            var connections = new List<(string friend1, string friend2)>
            {
                (friend1: "rob", friend2: "nick"),
                (friend1: "foo", friend2: "bar"),
            };
            var graphExplorer = new FriendsNetworkGraphs_IterativeDfsTraversalBasedOnNodeGraph().InitGraph(connections);

            //Act
            var action = new Action(() => results = graphExplorer.GetDistinctNetworks(workersCount: workersCount));

            //Assert
            action
                .Should()
                .NotThrow();

            results
                .Should()
                .BeEquivalentTo(expected);
        }

        [TestMethod]
        [DataRow(1)]
        [DataRow(null)]
        public void ShouldReturnSingleResult_GivenTwoJoinableSimpleRelationships(int? workersCount)
        {
            //Arrange
            var results = (List<HashSet<string>>) null;
            var expected = new List<HashSet<string>>
            {
                new[] {"rob", "nick", "foo"}.ToHashSet(),
            };
            var connections = new List<(string friend1, string friend2)>
            {
                (friend1: "rob", friend2: "nick"),
                (friend1: "nick", friend2: "foo"),
            };
            var graphExplorer = new FriendsNetworkGraphs_IterativeDfsTraversalBasedOnNodeGraph().InitGraph(connections);

            //Act
            var action = new Action(() => results = graphExplorer.GetDistinctNetworks(workersCount: workersCount));

            //Assert
            action
                .Should()
                .NotThrow();

            results
                .Should()
                .BeEquivalentTo(expected);
        }

        [TestMethod]
        [DataRow(1)]
        [DataRow(null)]
        public void ShouldReturnSingleResult_GivenTwoIdenticalRelationships(int? workersCount)
        {
            //Arrange
            var results = (List<HashSet<string>>) null;
            var expected = new List<HashSet<string>>
            {
                new[] {"rob", "nick"}.ToHashSet(),
            };
            var connections = new List<(string friend1, string friend2)>
            {
                (friend1: "rob", friend2: "nick"),
                (friend1: "rob", friend2: "nick"),
            };
            var graphExplorer = new FriendsNetworkGraphs_IterativeDfsTraversalBasedOnNodeGraph().InitGraph(connections);

            //Act
            var action = new Action(() => results = graphExplorer.GetDistinctNetworks(workersCount: workersCount));

            //Assert
            action
                .Should()
                .NotThrow();

            results
                .Should()
                .BeEquivalentTo(expected);
        }

        [TestMethod]
        [DataRow(1)]
        [DataRow(null)]
        public void ShouldReturnSingleResult_GivenTwoIdenticalRelationshipsInversed(int? workersCount)
        {
            //Arrange
            var results = (List<HashSet<string>>) null;
            var expected = new List<HashSet<string>>
            {
                new[] {"rob", "nick"}.ToHashSet(),
            };
            var connections = new List<(string friend1, string friend2)>
            {
                (friend1: "rob", friend2: "nick"),
                (friend1: "nick", friend2: "rob"),
            };
            var graphExplorer = new FriendsNetworkGraphs_IterativeDfsTraversalBasedOnNodeGraph().InitGraph(connections);

            //Act
            var action = new Action(() => results = graphExplorer.GetDistinctNetworks(workersCount: workersCount));

            //Assert
            action
                .Should()
                .NotThrow();

            results
                .Should()
                .BeEquivalentTo(expected);
        }

        [TestMethod]
        [DataRow(1)]
        [DataRow(null)]
        public void ShouldReturnSingleResult_GivenSingleRelationshipOfAGuyWithHimself(int? workersCount)
        {
            //Arrange
            var results = (List<HashSet<string>>) null;
            var expected = new List<HashSet<string>>
            {
                new[] {"rob"}.ToHashSet(),
            };
            var connections = new List<(string friend1, string friend2)>
            {
                (friend1: "rob", friend2: "rob"),
            };
            var graphExplorer = new FriendsNetworkGraphs_IterativeDfsTraversalBasedOnNodeGraph().InitGraph(connections);

            //Act
            var action = new Action(() => results = graphExplorer.GetDistinctNetworks(workersCount: workersCount));

            //Assert
            action
                .Should()
                .NotThrow();

            results
                .Should()
                .BeEquivalentTo(expected);
        }

        [TestMethod]
        [DataRow(1)]
        [DataRow(null)]
        public void ShouldReturnTwosResults_GivenDisjointedRelationships(int? workersCount)
        {
            //Arrange
            var results = (List<HashSet<string>>) null;
            var expected = new List<HashSet<string>>
            {
                new[] {"rob"}.ToHashSet(),
                new[] {"nick"}.ToHashSet(),
            };
            var connections = new List<(string friend1, string friend2)>
            {
                (friend1: "rob", friend2: "rob"),
                (friend1: "nick", friend2: "nick"),
            };
            var graphExplorer = new FriendsNetworkGraphs_IterativeDfsTraversalBasedOnNodeGraph().InitGraph(connections);

            //Act
            var action = new Action(() => results = graphExplorer.GetDistinctNetworks(workersCount: workersCount));

            //Assert
            action
                .Should()
                .NotThrow();

            results
                .Should()
                .BeEquivalentTo(expected);
        }

        [TestMethod]
        [DataRow(1)]
        [DataRow(null)]
        public void ShouldReturnDisjoinedNetworks_GivenMultipleRelationships(int? workersCount)
        {
            //Arrange
            var results = (List<HashSet<string>>) null;
            var expected = new List<HashSet<string>>
            {
                new[] {"rob", "nick", "katia", "aaliyara" }.ToHashSet(),
                new[] {"james", "tyler", "scott"}.ToHashSet(),
            };
            var connections = new List<(string friend1, string friend2)>
            {
                (friend1: "rob", friend2: "nick"),
                (friend1: "tyler", friend2: "scott"),
                (friend1: "rob", friend2: "katia"),
                (friend1: "aaliyara", friend2: "katia"),
                (friend1: "james", friend2: "scott"),
            };
            var graphExplorer = new FriendsNetworkGraphs_IterativeDfsTraversalBasedOnNodeGraph().InitGraph(connections);

            //Act
            var action = new Action(() => results = graphExplorer.GetDistinctNetworks(workersCount: workersCount));

            //Assert
            action
                .Should()
                .NotThrow();

            results
                .Should()
                .BeEquivalentTo(expected);
        }

        [TestMethod]
        [DataRow(1)]
        [DataRow(null)]
        public void ShouldReturnTwoDisjoinedNetworks_GivenTwoHugeDisjoinedGroups(int? workersCount)
        {
            //Arrange
            const int count = 2000;
            var results = (List<HashSet<string>>) null;
            var expected = new List<HashSet<string>>
            {
                Enumerable.Range(start: 00001, count: count).Select(x => x.ToString()).ToHashSet(),
                Enumerable.Range(start: 20000, count: count).Select(x => x.ToString()).ToHashSet(),
            };
            var connections = Enumerable
                .Empty<(string friend1, string friend2)>()
                .Concat(
                    Enumerable
                        .Range(start: 00001, count: count - 1)
                        .Select(x => (
                            friend1: x.ToString(),
                            friend2: (x + 1).ToString())
                        )
                )
                .Concat(
                    Enumerable
                        .Range(start: 20000, count: count - 1)
                        .Select(x => (
                            friend1: x.ToString(),
                            friend2: (x + 1).ToString())
                        )
                )
                .Shuffle()
                .ToList();
            var graphExplorer = new FriendsNetworkGraphs_IterativeDfsTraversalBasedOnNodeGraph().InitGraph(connections);

            //Act
            var action = new Action(() => results = graphExplorer.GetDistinctNetworks(workersCount: workersCount));

            //Assert
            action
                .Should()
                .NotThrow();

            results
                .Should()
                .BeEquivalentTo(expected);
        }
    }
}

