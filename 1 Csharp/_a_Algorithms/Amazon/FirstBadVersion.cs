﻿// ReSharper disable UnusedMember.Global
//
// https://youtu.be/NWWPbKC4qV4?t=185
//
// You are given an array of integer versions of a software product your company is developing
//
//         [1, 2, 3, ..., n - 1, n]
//
// At some point one of these versions becomes bad and this causes all subsequent versions to
// also become bad as well
//
// Make a method that returns a method (aka higher order function) which finds the first version
// (one based) that became bad  If no version is found to be bad then it should return -1
//
// Complexities
//
//    Time    O(log(N))
//    Space   O(1)
//

using System;
using Ardalis.GuardClauses;

namespace Testbed._a_Algorithms.Amazon
{
    public class FirstBadVersion
    {
        static public Func<int, int> SpawnFinder(Func<int, bool> isBadVersion)
        {
            Guard.Against.Null(isBadVersion, nameof(isBadVersion));

            return maxVersion =>
            {
                if (maxVersion <= 0) //noversions
                    return -1;

                var min = 1;
                var max = maxVersion;
                while (min <= maxVersion)
                {
                    var version = (int)Math.Floor(((float)max + min) / 2);
                    if (isBadVersion(version))
                    {
                        if (min == version) //bingo!
                            return version;

                        max = version;
                    }
                    else
                    {
                        min = version + 1;
                    }
                }

                return -1; //no bad versions
            };
        }
    }
}
