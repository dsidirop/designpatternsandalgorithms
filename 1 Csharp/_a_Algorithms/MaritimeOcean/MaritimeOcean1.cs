﻿// ReSharper disable UnusedVariable
// ReSharper disable UnusedMember.Global

using System.Text;

namespace Testbed._a_Algorithms.MaritimeOcean
{
    static public class MaritimeOcean1
    {
        static public void Tester()
        {
            var success1 = false;
            var success2 = false;
            var success3 = false;

            {
                var input = new TextInput();

                var result = input.GetValue();
                success1 = result == "";
            }
        }

        public class TextInput
        {
            private readonly StringBuilder _buffer = new StringBuilder(128);

            public virtual void Add(char c)
            {
                _buffer.Append(c);
            }

            public virtual string GetValue()
            {
                return _buffer.ToString();
            }
        }

        public class NumericInput : TextInput
        {
            public override void Add(char c)
            {
                if (!char.IsDigit(c))
                    return;

                base.Add(c);
            }
        }
    }
}


