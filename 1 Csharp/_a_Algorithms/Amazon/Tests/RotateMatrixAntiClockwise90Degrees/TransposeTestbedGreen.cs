﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.RotateMatrixAntiClockwise90Degrees
{
    [TestClass]
    public class TransposeTestbedGreen
    {
        [TestMethod]
        public void ShouldResultInEmptyMatrixGivenEmptyMatrix()
        {
            //Arrange
            var matrix = new int[0, 0];

            //Act
            RotateSquareMatrixAntiClockwise90Degrees.Transpose(ref matrix);

            //Assert
            matrix.Should().BeEquivalentTo(new int[0, 0]);
        }

        [TestMethod]
        public void ShouldResultInSingleItemMatrixGivenSingleItemMatrix()
        {
            //Arrange
            var matrix = new int[1, 1] { { 1 } };

            //Act
            RotateSquareMatrixAntiClockwise90Degrees.Transpose(ref matrix);

            //Assert
            matrix.Should().BeEquivalentTo(new int[1, 1] { { 1 } });
        }

        [TestMethod]
        public void ShouldResultIn2X2MatrixGiven2X2Matrix()
        {
            //Arrange
            var matrix = new int[2, 2]
            {
                { 1, 2 },
                { 3, 4 }
            };
            var expected = new int[2, 2]
            {
                { 1, 3 },
                { 2, 4 }
            };

            //Act
            RotateSquareMatrixAntiClockwise90Degrees.Transpose(ref matrix);

            //Assert
            matrix.Should().BeEquivalentTo(expected);
        }

        [TestMethod]
        public void ShouldResultIn3X3MatrixGiven3X3Matrix()
        {
            //Arrange
            var matrix = new int[3, 3]
            {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 7, 8, 9 }
            };
            var expected = new int[3, 3]
            {
                { 1, 4, 7 },
                { 2, 5, 8 },
                { 3, 6, 9 }
            };

            //Act
            RotateSquareMatrixAntiClockwise90Degrees.Transpose(ref matrix);

            //Assert
            matrix.Should().BeEquivalentTo(expected);
        }

        [TestMethod]
        public void ShouldResultIn4X4MatrixGiven4X4Matrix()
        {
            //Arrange
            var matrix = new int[4, 4]
            {
                { 01, 02, 03, 04 },
                { 05, 06, 07, 08 },
                { 09, 10, 11, 12 },
                { 13, 14, 15, 16 },
            };
            var expected = new int[4, 4]
            {
                { 01, 05, 09, 13 },
                { 02, 06, 10, 14 },
                { 03, 07, 11, 15 },
                { 04, 08, 12, 16 }
            };

            //Act
            RotateSquareMatrixAntiClockwise90Degrees.Transpose(ref matrix);

            //Assert
            matrix.Should().BeEquivalentTo(expected);
        }
    }
}
