﻿// Assume the following HTML code:
//
// <body>
//     In my life, I used the following web search engines:<br />
//     <a href="//www.yahoo.com">Yahoo!</a><br />
//     <a href="//www.altavista.com">AltaVista</a><br />
//     <a href="//www.google.com">Google</a><br />
// </body>
//
// We've written the following code to print the index of the <a> element clicked
//
//    function registerHandlers() {
//        var as = document.getElementsByTagName('a');
//        for (var i = 0; i < as.length; i++) {
//            as[i].onclick = function () {
//                alert(i);
//                return false;
//            }
//        }
//    }
//
// Fix the problems with this code
//

function registerHandlers() {
    var as = document.getElementsByTagName("a");
    for (var i = 0; i < as.length; i++) {
        as[i].onclick = (function (index) { // <-- closure time
            var _index = index;
            return function () {
                alert(_index);
                return false;
            }
        })(i);  // <--- we need to snapshot the index by placing it inside a js closure
    }
}
