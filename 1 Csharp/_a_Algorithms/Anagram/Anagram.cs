﻿using System.Linq;

namespace Testbed._a_Algorithms.Anagram
{
    static public class AreAnagrams
    {
        static public void Tester()
        {
            // Console.WriteLine(AreStringsAnagrams("neural", "unreal"));
            var result = AreStringsAnagrams("neurdfwfal", "unreal");
        }

        static public bool AreStringsAnagrams(string a, string b)
        {
            if (a == null && b == null) return true;
            if (a == null || b == null || a.Length != b.Length) return false;

            return b.All(x => b.Count(c => c == x) == a.Count(c => c == x));
        }
    }
}
 