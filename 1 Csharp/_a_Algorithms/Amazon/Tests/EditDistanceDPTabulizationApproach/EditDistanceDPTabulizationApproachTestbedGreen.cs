﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.EditDistanceDPTabulizationApproach
{
    [TestClass]
    public class EditDistanceDPSmartTabulizationApproachTestbedGreen
    {
        [TestMethod]
        [DataRow("", "", 0)]
        [DataRow("a", "a", 0)]
        [DataRow("abc", "abc", 0)]
        [DataRow("a", "b", 1)]
        [DataRow("a", "", 1)]
        [DataRow("", "a", 1)]
        [DataRow("a", "aa", 1)]
        [DataRow("aa", "a", 1)]
        [DataRow("abc", "aac", 1)]
        [DataRow("abc", "aaa", 2)]
        [DataRow("abc", "aaaa", 3)]
        public void ShouldReturnCorrectDistanceGivenValidStrings(string s1, string s2, int expectedDistance)
        {
            //Arrange
            //...

            //Act
            var distance = EditDistanceDpTabulizationApproach.Calculate(s1, s2);

            //Assert
            distance.Should().Be(expectedDistance);
        }
    }
}
