package main

import (
	"errors"
	"fmt"
)

type WrappedError struct {
	Context     string
	ParentError error
}

func (w *WrappedError) Error() string {
	return fmt.Sprintf("%s: %v", w.Context, w.ParentError)
}

func Wrap(err error, info string) *WrappedError {
	return &WrappedError{
		Context:     info,
		ParentError: err,
	}
}

func main() {
	err := errors.New("boom")
	err = Wrap(err, "main")

	fmt.Println(err)
}
