﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Testbed._a_Algorithms.LogEntriesStatisticsAnalyzer.Tests
{
    [TestClass]
    public class LogEntriesStatisticsGivenRedEntriesShould : LogEntriesStatisticsTestbedBase
    {
        [TestMethod]
        public async Task Return100PercentErrorRateGivenSingleErrorLogFile()
        {
            var tempFilePath = Path.GetTempFileName();
            try
            {
                //Arrange
                var expectedGarbledLinesCount = 0;
                var expectedStatsPerHour = Enumerable.Range(start: 0, count: 24).ToDictionary(x => (byte)x, y => new HourlyStats());
                expectedStatsPerHour[1].ErrorRate = 1;
                expectedStatsPerHour[1].ErrorsCount = 1;
                expectedStatsPerHour[1].EntriesCount = 1;

                await File.WriteAllTextAsync(tempFilePath, SpawnLogEntry(hour: 1, statusCode: HttpStatusCode.BadRequest), Encoding.ASCII);

                //Act
                var analysis = await new LogEntriesStatistics(new FileSystem()).CalculateErrorRatesAsync(tempFilePath);

                //Assert
                analysis.Should().BeEquivalentTo((expectedStatsPerHour, expectedGarbledLinesCount));
            }
            finally //Cleanup
            {
                if (!string.IsNullOrWhiteSpace(tempFilePath))
                {
                    File.Delete(tempFilePath);
                }
            }
        }

        [TestMethod]
        public async Task Return50PercentErrorRateGivenRedGreenLogFile()
        {
            var tempFilePath = Path.GetTempFileName();
            try
            {
                //Arrange
                var expectedGarbledLinesCount = 0;
                var expectedStatsPerHour = Enumerable.Range(start: 0, count: 24).ToDictionary(x => (byte)x, y => new HourlyStats());
                expectedStatsPerHour[1].ErrorRate = 0.5;
                expectedStatsPerHour[1].ErrorsCount = 1;
                expectedStatsPerHour[1].EntriesCount = 2;

                var logContent = string.Join(
                    Environment.NewLine,
                    SpawnLogEntry(hour: 1, statusCode: HttpStatusCode.OK),
                    SpawnLogEntry(hour: 1, statusCode: HttpStatusCode.BadRequest)
                );
                await File.WriteAllTextAsync(tempFilePath, logContent, Encoding.ASCII);

                //Act
                var analysis = await new LogEntriesStatistics(new FileSystem()).CalculateErrorRatesAsync(tempFilePath);

                //Assert
                analysis.Should().BeEquivalentTo((expectedStatsPerHour, expectedGarbledLinesCount));
            }
            finally //Cleanup
            {
                if (!string.IsNullOrWhiteSpace(tempFilePath))
                {
                    File.Delete(tempFilePath);
                }
            }
        }
    }
}
