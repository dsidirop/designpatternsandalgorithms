﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.BinaryTreeLeastCommonAncestor
{
    [TestClass]
    public class BinaryTreeLeastCommonAncestorTestbed
    {
        [TestMethod]
        public void ShouldReturnNullGivenEmptyTree()
        {
            //Arrange
            var tree = new BinaryTreeLeastCommonAncestor<int>();

            //Act
            var lca = tree.FindLca(1, 2);

            //Assert
            lca.Should().Be(null);
        }

        [TestMethod]
        public void ShouldReturnNullGivenNonExistentData()
        {
            //Arrange
            var tree = new BinaryTreeLeastCommonAncestor<int>
            {
                Root = new Node<int>(1)
            };

            //Act
            var lca = tree.FindLca(1, 2);

            //Assert
            lca.Should().Be(null);
        }

        [TestMethod]
        public void ShouldReturnRootGivenRootData()
        {
            //Arrange
            var tree = new BinaryTreeLeastCommonAncestor<int>
            {
                Root = new Node<int>(1)
            };

            //Act
            var lca = tree.FindLca(1, 1);

            //Assert
            lca.Should().Be(tree.Root);
        }

        [TestMethod]
        [DataRow(2, 3)]
        [DataRow(4, 6)]
        [DataRow(5, 7)]
        public void ShouldReturnRootGivenDataOnOppositeSidesOfTheRoot(int key1, int key2)
        {
            //Arrange
            var tree = new BinaryTreeLeastCommonAncestor<int> { Root = new Node<int>(1) };
            tree.Root.Left = new Node<int>(2);
            tree.Root.Right = new Node<int>(3);
            tree.Root.Left.Left = new Node<int>(4);
            tree.Root.Left.Right = new Node<int>(5);
            tree.Root.Right.Left = new Node<int>(6);
            tree.Root.Right.Right = new Node<int>(7);
            
            //Act
            var lca = tree.FindLca(key1, key2);

            //Assert
            lca.Should().Be(tree.Root);
        }

        [TestMethod]
        [DataRow(2)]
        [DataRow(3)]
        [DataRow(4)]
        [DataRow(5)]
        [DataRow(6)]
        [DataRow(7)]
        public void ShouldReturnRootGivenRootDataPlusAnyOtherNode(int key)
        {
            //Arrange
            var tree = new BinaryTreeLeastCommonAncestor<int> { Root = new Node<int>(1) };
            tree.Root.Left = new Node<int>(2);
            tree.Root.Right = new Node<int>(3);
            tree.Root.Left.Left = new Node<int>(4);
            tree.Root.Left.Right = new Node<int>(5);
            tree.Root.Right.Left = new Node<int>(6);
            tree.Root.Right.Right = new Node<int>(7);

            //Act
            var lca = tree.FindLca(1, key);

            //Assert
            lca.Should().Be(tree.Root);
        }

        [TestMethod]
        [DataRow(2, 4, 5)]
        [DataRow(2, 8, 13)]
        [DataRow(3, 6, 7)]
        [DataRow(3, 10, 15)]
        public void ShouldReturnSubNodeGivenTwoExistingNodes(int expectedLca, int key1, int key2)
        {
            //Arrange
            var tree = new BinaryTreeLeastCommonAncestor<int> { Root = new Node<int>(1) };
            tree.Root.Left = new Node<int>(2);
            tree.Root.Left.Left = new Node<int>(4);
            tree.Root.Left.Right = new Node<int>(5);
            tree.Root.Left.Left.Left = new Node<int>(8);
            tree.Root.Left.Left.Right = new Node<int>(9);
            tree.Root.Left.Right.Left = new Node<int>(12);
            tree.Root.Left.Right.Right = new Node<int>(13);

            tree.Root.Right = new Node<int>(3);
            tree.Root.Right.Left = new Node<int>(6);
            tree.Root.Right.Right = new Node<int>(7);
            tree.Root.Right.Left.Left = new Node<int>(10);
            tree.Root.Right.Left.Right = new Node<int>(11);
            tree.Root.Right.Right.Left = new Node<int>(14);
            tree.Root.Right.Right.Right = new Node<int>(15);

            //Act
            var lca = tree.FindLca(key1, key2);

            //Assert
            lca.Should().NotBe(null);
            lca.Value.Should().Be(expectedLca);
        }
    }
}

