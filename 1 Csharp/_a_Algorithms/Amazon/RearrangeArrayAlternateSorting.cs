﻿//
//      https://practice.geeksforgeeks.org/problems/-rearrange-array-alternately/0/
//      https://www.youtube.com/watch?v=KOglcclYgXI <- the Solution is completely out of this world
//
// Given a sorted array of positive integers  Your task is to rearrange the array elements alternatively
// ie first element should be max value second should be min value third should be second max fourth
// should be second min and so on
//
// Note
//
//       O(1) extra space is allowed  You can modify the original input array as required
//
// Complexities
//
//       Time   O(N)
//       Space  O(1)
//

using System;

namespace Testbed._a_Algorithms.Amazon
{
    public class RearrangeArrayAlternateSorting
    {
        static public void Calculate(ref int[] input)
        {
            // Guard.Against.Null(input, nameof(input));
            
            throw new NotImplementedException();
        }
    }
}
