﻿// Friend Groups
//
// Write a function that given a list of relationships and a list of names it outputs the friend-networks
// which contain at least one of the names given
//
// example input:
//
//    {"rob", "nick"}
//    {"tyler", "scott"}
//    {"rob", "katia"}
//    {"aaliyara", "katia"}
//    {"james", "scott"}
//
// if we pass in the names "julia", "rob", "tyler" then we should get:
//
//    {"rob",   "nick",  "katia", "aaliyara"}
//    {"james", "scott", "tyler"            }
//
// if we pass no names then the program must assume we mean to pass in all names available
//
// canon solution derived from:
//
//    https://www.geeksforgeeks.org/connected-components-in-an-undirected-graph/
//

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Ardalis.GuardClauses;

namespace Testbed._a_Algorithms.DiscoverAllDisconnectedFriendNetworkGraphs
{
    public class FriendsNetworkGraphs_IterativeDfsTraversalBasedOnNodeGraph
    {
        private readonly Dictionary<string, Node> _personsRegistry = new(0, CaseInsensitiveComparer);
 
        public sealed class Node
        {
            public string Name { get; set; }

            public readonly Dictionary<string, Node> Friends = new(4, CaseInsensitiveComparer);
        }

        public FriendsNetworkGraphs_IterativeDfsTraversalBasedOnNodeGraph InitGraph(IEnumerable<(string friend1, string friend2)> friendRelations)
        {
            foreach (var (friend1, friend2) in friendRelations)
            {
                if (!_personsRegistry.TryGetValue(friend1, out var friend1Node))
                {
                    friend1Node = new Node {Name = friend1};
                    _personsRegistry.Add(friend1, friend1Node);
                }

                if (!_personsRegistry.TryGetValue(friend2, out var friend2Node))
                {
                    friend2Node = new Node {Name = friend2};
                    _personsRegistry.Add(friend2, friend2Node);
                }

                EnsureLinkedUp(friend1Node, friend2Node);
            }

            return this;
        }

        static private void EnsureLinkedUp(Node friend1Node, Node friend2Node)
        {
            if (friend1Node.Friends.ContainsKey(friend2Node.Name)) //already friends
            {
                return;
            }

            friend1Node.Friends[friend2Node.Name] = friend2Node;
            friend2Node.Friends[friend1Node.Name] = friend1Node;
        }

        public List<HashSet<string>> GetDistinctNetworks(IEnumerable<string> names = null, int? workersCount = null)
        {
            Guard.Against.OutOfRange(workersCount ?? 1, nameof(workersCount), rangeFrom: 1, rangeTo: 512); // 512 = Scheduling.MAX_SUPPORTED_DOP

            if (!_personsRegistry.Any())
            {
                return new List<HashSet<string>>(0);
            }

            var visitedNodes = new ConcurrentDictionary<string, HashSet<string>>(CaseInsensitiveComparer); // [{ nodeName, listOfCrawlersThatVisitedTheNode }]

            var jumpstartNodes = GetJumpstartNodes(names);

            var jumpstartNodesWorkloads = workersCount switch
            {
                1 => jumpstartNodes,
                null => jumpstartNodes.AsParallel(),
                _ => jumpstartNodes.AsParallel().WithDegreeOfParallelism(workersCount.Value)
            };

            var subnetworksProducedByEachCrawler = jumpstartNodesWorkloads
                .Select(x =>
                {
                    var network = DfsGraphCrawlerStream(x, visitedNodes).ToHashSet(CaseInsensitiveComparer);

                    return new
                    {
                        networkNodes = network,
                        networkNickname = x.Name,
                    };
                })
                .ToDictionary(
                    p => p.networkNickname,
                    p => p.networkNodes
                );

            var linkedNetworks = GetBridgedNetworks(visitedNodes.Values.ToList());

            var distinctNetworks = linkedNetworks
                .AsParallel()
                .Select(network =>
                {
                    var distinctNetwork = network
                        .SelectMany(c => subnetworksProducedByEachCrawler[c])
                        .ToHashSet(CaseInsensitiveComparer);

                    return distinctNetwork;
                })
                .ToList();

            return distinctNetworks;
        }

        static private IEnumerable<IEnumerable<string>> GetBridgedNetworks(List<HashSet<string>> visited)
        {
            if (visited.Count <= 1)
                return new List<HashSet<string>>(visited);

            var coagulated = new List<HashSet<string>>(visited.Count / 2);

            while (true) //todo  optimize this further
            {
                var atLeastOneCoagulationHappened = false;
                foreach (var v in visited)
                {
                    var intersectingNetwork = coagulated.FirstOrDefault(c => c.Any(x => v.Contains(x)));
                    if (intersectingNetwork != null)
                    {
                        atLeastOneCoagulationHappened = true;
                        intersectingNetwork.AddRange(v);
                    }
                    else
                    {
                        coagulated.Add(v.ToHashSet(CaseInsensitiveComparer));
                    }
                }

                if (!atLeastOneCoagulationHappened)
                    break;

                visited = coagulated;
                coagulated = new List<HashSet<string>>(visited.Count / 2);
            }

            return coagulated;
        }

        private IEnumerable<string> DfsGraphCrawlerStream(Node person, ConcurrentDictionary<string, HashSet<string>> visitedNodes)
        {
            var graphCralwerId = person.Name;

            if (!TryEnrichVisitedNodes(visitedNodes, person, graphCralwerId))
            {
                yield break;
            }

            var discovered = new Queue<Node>(128);
            discovered.Enqueue(person);
            while (discovered.TryDequeue(out var f))
            {
                yield return f.Name;

                var unvisitedFriendsOfFriend = _personsRegistry[f.Name]
                    .Friends
                    .Values
                    .Where(x =>
                    {
                        var newDiscovery = TryEnrichVisitedNodes(visitedNodes, x, graphCralwerId);

                        return newDiscovery;
                    });

                foreach (var x in unvisitedFriendsOfFriend)
                {
                    discovered.Enqueue(x);
                }
            }
        }

        static private bool TryEnrichVisitedNodes(ConcurrentDictionary<string, HashSet<string>> visited, Node node, string graphCralwerId)
        {
            var newDiscovery = false;
            visited.AddOrUpdate(
                key: node.Name,
                addValueFactory: _ =>
                {
                    newDiscovery = true;
                    return new HashSet<string>(CaseInsensitiveComparer) {graphCralwerId};
                },
                updateValueFactory: (_, crawlersThatVisitedThisPerson) =>
                {
                    newDiscovery = false;
                    crawlersThatVisitedThisPerson.Add(graphCralwerId);
                    return crawlersThatVisitedThisPerson;
                }
            );
            return newDiscovery;
        }

        private IEnumerable<Node> GetJumpstartNodes(IEnumerable<string> names)
        {
            if (names == null)
            {
                return _personsRegistry.Values;
            }

            var jumpstartNodes = names
                .Select(
                    x => _personsRegistry.TryGetValue(x, out var node)
                        ? node
                        : null
                )
                .Where(x => x != null);

            return jumpstartNodes;
        }

        static private readonly IEqualityComparer<string> CaseInsensitiveComparer = StringComparer.InvariantCultureIgnoreCase;
    }
}
