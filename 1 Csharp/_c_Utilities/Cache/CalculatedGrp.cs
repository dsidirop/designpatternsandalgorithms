namespace Testbed._c_Utilities.Cache
{
    public sealed class CalculatedGrp
    {
        public int Result { get; set; }
        public string Key { get; set; }
    }
}