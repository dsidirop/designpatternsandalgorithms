﻿// ReSharper disable UnusedVariable
// ReSharper disable UnusedMember.Global

using System;

namespace Testbed._a_Algorithms.ATCom
{
    static public class ATCom3 //same as maritime
    {
        static public void Tester()
        {
            var success1 = false;
            var success2 = false;
            var success3 = false;

            {
                var chicken = new Chicken();
                var egg = chicken.Lay();
                var childBird = egg.Hatch();
                success1 = childBird is Chicken;
            }

            {
                var eagle = new Eagle();
                var egg = eagle.Lay();
                var childBird = egg.Hatch();
                success2 = childBird is Eagle;
            }

            {
                var chicken = new Chicken();
                var egg = chicken.Lay();
                try
                {
                    egg.Hatch();
                    egg.Hatch();
                }
                catch (InvalidOperationException)
                {
                    success3 = true;
                }
            }
        }

        public interface IBird
        {
            Egg Lay();
        }

        public class Chicken : IBird
        {
            public Egg Lay() => new Egg(() => new Chicken());
        }

        public class Eagle : IBird
        {
            public Egg Lay() => new Egg(() => new Eagle());
        }

        public class Egg
        {
            private Func<IBird> _createBird;
            private readonly object _lock = new object();

            public Egg(Func<IBird> createBird)
            {
                _createBird = createBird;
            }

            public IBird Hatch()
            {
                if (_createBird == null)
                    throw new InvalidOperationException($@"MO.E01 [BUG] Attempt to hatch an egg a second time!");

                lock (_lock)
                {
                    if (_createBird == null)
                        throw new InvalidOperationException($@"MO.E01 [BUG] Attempt to hatch an egg a second time!");

                    var newBird = _createBird(); //order
                    _createBird = null; //order

                    return newBird;
                }
            }
        }
    }
}


