package main

import (
	"errors"
	"fmt"
	"os"
)

func CheckForSpecificError() {
	_, err := os.Open("I_DO_NOT_EXIST.TXT") // Generate the error

	fmt.Println(err)

	var pathError *os.PathError
	if errors.As(err, &pathError) { //this is how we check for a specific exception
		// errors.Is(err, pathError) == true in here

		fmt.Println(pathError)
		return
	}
}
