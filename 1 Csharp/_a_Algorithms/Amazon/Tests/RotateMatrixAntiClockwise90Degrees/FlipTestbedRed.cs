﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.RotateMatrixAntiClockwise90Degrees
{
    [TestClass]
    public class FlipTestbedRed
    {
        [TestMethod]
        public void ShouldResultInArgumentExceptionGivenNullMatrix()
        {
            //Arrange
            var matrix = (int[,])null;

            //Act
            var action = new Action(() => RotateSquareMatrixAntiClockwise90Degrees.FlipVertically(ref matrix));

            //Assert
            action.Should().ThrowExactly<ArgumentException>().WithMessage("matrix");
        }
    }
}
