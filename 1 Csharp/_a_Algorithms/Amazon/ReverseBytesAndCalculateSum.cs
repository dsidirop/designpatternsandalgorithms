﻿// ReSharper disable UnusedMember.Global
//
// Write a function accepts an array of up to two bytes
//
// - Both bytes must converted to integers
// - The second byte must be shifted by 8 positions to the left
// - Then the two integers must be added
//

using System;
using System.Linq;

namespace Testbed._a_Algorithms.Amazon
{
    static public class ReverseBytesAndCalculateSum
    {
        static public int GetManufacturerId(byte[] manufacturerIdBytes)
        {
            var firstByteAsInt32 = manufacturerIdBytes.FirstOrDefault() & 0xFF; //this trick converts it over to an int32
            var secondByteAsInt32 = manufacturerIdBytes.Skip(1).FirstOrDefault() & 0xFF;

            var secondByteAsInt32ShiftedBy8Left = secondByteAsInt32 << 8;

            return firstByteAsInt32 + secondByteAsInt32ShiftedBy8Left;
        }
    }
}
