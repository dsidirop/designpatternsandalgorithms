﻿// https://www.geeksforgeeks.org/lowest-common-ancestor-binary-tree-set-1/
//
// http://en.wikipedia.org/wiki/Lowest_common_ancestor
//
// Let T be a rooted tree  The lowest common ancestor between two nodes n1 and n2 is defined as the lowest
// node in T that has both n1 and n2 as descendants  We allow a node to be a descendant of itself
//
// The LCA of n1 and n2 in T is the shared ancestor of n1 and n2 that is located farthest from the root
//
// Notice that the tree here is a simple binarytree   Its not a binary search tree   Its not sorted and for
// that matter we cannot optimize the search mechanics   We have to perform full tree scanning
//
// Complexities
//
//    Space 0(N)   the recursive function can and does indeed take space in memory   we cant ignore that
//    Time  O(N)   the cost should be O(p*N) where p->[0, 1] which gets simplified to O(N)
//
// Sidenote   Computation of lowest common ancestors may be useful for instance as part of a procedure for
// determining the distance between pairs of nodes in a tree  The distance from n1 to n2 can be computed as
// the distance from the root to n1 plus the distance from the root to n2 minus twice the distance from the
// root to their lowest common ancestor

using System.Collections.Generic;

namespace Testbed._a_Algorithms.Amazon
{
    public class BinaryTreeLeastCommonAncestor<T>
    {
        public Node<T> Root { get; set; }

        public Node<T> FindLca(T key1, T key2)
        {
            var results = FindLcaImpl(Root, Comparer<T>.Default, key1, key2);

            return results != null && results.IsFoundN1 && results.IsFoundN2
                ? results.Node
                : null;
        }

        static private Results<T> FindLcaImpl(Node<T> node, IComparer<T> comparer, T n1, T n2)
        {
            if (node == null)
                return null;

            var isFoundN1 = false;
            var isFoundN2 = false;

            var outputDataFromLeft = FindLcaImpl(node.Left, comparer, n1, n2);
            if (outputDataFromLeft != null)
            {
                if (outputDataFromLeft.IsFoundN1 && outputDataFromLeft.IsFoundN2)
                    return outputDataFromLeft;

                isFoundN1 = outputDataFromLeft.IsFoundN1;
                isFoundN2 = outputDataFromLeft.IsFoundN2;
            }

            var outputDataFromRight = FindLcaImpl(node.Right, comparer, n1, n2);
            if (outputDataFromRight != null)
            {
                if (outputDataFromRight.IsFoundN1 && outputDataFromRight.IsFoundN2)
                    return outputDataFromRight;

                isFoundN1 = isFoundN1 || outputDataFromRight.IsFoundN1;
                isFoundN2 = isFoundN2 || outputDataFromRight.IsFoundN2;
            }

            isFoundN1 = isFoundN1 || comparer.Compare(node.Value, n1) == 0;
            isFoundN2 = isFoundN2 || comparer.Compare(node.Value, n2) == 0;

            return isFoundN1 || isFoundN2
                ? new Results<T>(node, isFoundN1, isFoundN2)
                : null;
        }

        private class Results<TT>
        {
            public Node<TT> Node { get; private set; }
            public bool IsFoundN1 { get; private set; }
            public bool IsFoundN2 { get; private set; }

            public Results(Node<TT> node, bool foundN1, bool foundN2)
            {
                Node = node;
                IsFoundN1 = foundN1;
                IsFoundN2 = foundN2;
            }
        }
    }

    public class Node<TData>
    {
        public TData Value { get; }
        public Node<TData> Left { get; set; }
        public Node<TData> Right { get; set; }

        public Node(TData value)
        {
            Value = value;
        }
    }
}
