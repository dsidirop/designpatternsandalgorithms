package main

import "fmt"

type Key struct { // https://stackoverflow.com/a/29663169/863651
	a *int
}

func (k *Key) HashKey() int {
	return *(*k).a
}

func (k *Key) HashKey2() string {
	return fmt.Sprintf("%v", k)
}

func main() {
	one := 1
	two := 2

	k1, k2 := Key{a: &one}, Key{a: &two}
	m := map[int]string{}
	m[k1.HashKey()] = "one"
	m[k2.HashKey()] = "two" // m = map[int]string{1:"one", 2:"two"}

	fmt.Println(m[k1.HashKey()]) // => "one"

	m2 := map[string]string{}
	m2[k1.HashKey2()] = "foobar"

	fmt.Println(m2[k1.HashKey2()]) // => "foobar"
}
