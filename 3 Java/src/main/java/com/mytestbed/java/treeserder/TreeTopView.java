package com.mytestbed.java.treeserder;

import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

// https://www.geeksforgeeks.org/print-nodes-top-view-binary-tree/
//
// Top view of a binary tree is the set of nodes visible when the tree is viewed from the top
// Given a binary tree print the top view of it   The output nodes can be printed in any order
//
// Expected time complexity is O(n)
//
// A node x is there in output if x is the topmost node at its horizontal distance   Horizontal
// distance of left child of a node x is equal to horizontal distance of x minus 1 and that of
// right child is horizontal distance of x plus 1

public class TreeTopView
{
    public static void main(String[] args)
    {
        TreeNode root = spawnTree();

        Map<Integer, TreeNode> results = topView2(root);

        System.out.println("The top view of the tree is:");
        for (Map.Entry<Integer, TreeNode> x : results.entrySet())
        {
            System.out.print(" " + x.getValue().value);
        }
    }

    public static TreeNode spawnTree()
    {
        TreeNode root = new TreeNode(4);

        root.left = new TreeNode(2);
        root.left.left = new TreeNode(1);
        root.left.right = new TreeNode(3);

        root.right = new TreeNode(6);
        root.right.left = new TreeNode(5);
        root.right.right = new TreeNode(7);

        return root;
    }

    static class TreeNode
    {
        Integer value;
        TreeNode left;
        TreeNode right;

        TreeNode(Integer value)
        {
            this.value = value;
        }
    }

    public static Map<Integer, TreeNode> topView2(TreeNode root)
    {
        class TreeNodeWithDistancePair
        {
            TreeNode node;
            int horizontalDistance;

            TreeNodeWithDistancePair(int horizontalDistance, TreeNode node)
            {
                this.node = node;
                this.horizontalDistance = horizontalDistance;
            }
        }

        Map<Integer, TreeNode> results = new TreeMap<Integer, TreeNode>();
        if (root == null)
            return results;

        Queue<TreeNodeWithDistancePair> queue = new LinkedList<TreeNodeWithDistancePair>()
        {{
            add(new TreeNodeWithDistancePair(0, root));
        }};
        while (!queue.isEmpty())
        {
            TreeNodeWithDistancePair current = queue.poll();
            if (current.node.left != null)
            {
                queue.add(new TreeNodeWithDistancePair(current.horizontalDistance - 1, current.node.left));
            }

            if (current.node.right != null)
            {
                queue.add(new TreeNodeWithDistancePair(current.horizontalDistance + 1, current.node.right));
            }

            if (!results.containsKey(current.horizontalDistance))
            {
                results.put(current.horizontalDistance, current.node);
            }
        }

        return results;
    }
}
