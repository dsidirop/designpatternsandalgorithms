﻿// ReSharper disable UnusedMember.Global
//
// Given an array print the Next Greater Element (NGE) for every element   The Next greater element
// for an element x is the first greater element on the right side of x in array   Elements for which
// no greater element exist consider next greater element as null
//
// Examples
// 
// - For any array rightmost element always has next greater element as null
// - For an array which is sorted in decreasing order all elements have next greater element as null
// - For the input array
//
//          [4, 5, 2, 25]
//
//   the next greater elements for each element are as follows
//
//     Element            NGE
//     4          -->       5
//     5          -->      25
//     2          -->      25
//     25         -->      -1
//
//   The resulting array should be (with the elements in any order):
//
//      [
//          { index: 0, value: 4, gneIndex:    1, gne:    5 },
//          { index: 1, value: 5, gneIndex:    3, gne:   25 },
//          { index: 2, value: 2, gneIndex:    3, gne:   25 },
//          { index: 3, value: 4, gneIndex: null, gne: null },
//      ]
//
// Footnote
//
//   This problem is transformationally-equivalent to the stock-price-span problem
//

using Ardalis.GuardClauses;
using System.Collections.Generic;
using System.Linq;

namespace Testbed._a_Algorithms.Amazon
{
    static public class NextGreaterElement
    {
        static public Result[] Calculate(int[] input)
        {
            Guard.Against.Null(input, nameof(input));

            if (!input.Any())
                return new Result[] { };

            if (input.Length == 1)
                return new Result[] { new Result { Index = 0, Value = input[0], GneIndex = null, GneValue = null } };

            var stack = new Stack<(int index, int value)>();
            stack.Push((0, input[0]));

            var results = new List<Result>(input.Length);
            foreach (var (i, current) in input.Skip(1).Select((value, i) => (i + 1, value)))
            {
                for (
                    var peek = default((int index, int value));
                    stack.Any() && (peek = stack.Peek()).value < current;
                    stack.Pop()
                )
                {
                    results.Add(new Result
                    {
                        Index = peek.index,
                        Value = peek.value,
                        GneIndex = i,
                        GneValue = current
                    });
                }

                stack.Push((i, current));
            }

            results.AddRange(stack.Select(x =>
                new Result
                {
                    Index = x.index,
                    Value = x.value,
                    GneIndex = null,
                    GneValue = null
                })
            );

            results.Sort((x, y) => x.Index.CompareTo(y.Index));

            return results.ToArray();
        }

        public struct Result
        {
            public int Index { get; set; }
            public int Value { get; set; }

            public int? GneIndex { get; set; }
            public int? GneValue { get; set; }
        }
    }
}

