﻿// ReSharper disable UnusedMember.Global
//
// Write a function which reports whether a string is a permutation of
// another string
//
// Problem Negotiation:
//
// - Can we assume the strings are ascii only? No
// - Can we use additional data structures? Yes
// - Are whitespace characters taken into account? Yes
// - Can we assume the two strings fit in memory? Yes
// - Should the mechanism be case-sensitive? Yes

using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

namespace Testbed._a_Algorithms.Amazon
{
    [TestClass]
    public class StringPermutationDetector
    {
        public class CharCounter
        {
            public int Counter { get; private set; }

            public CharCounter(int initialValue)
            {
                Counter = initialValue;
            }

            public CharCounter ChainTweak(int delta)
            {
                Counter += delta;
                return this;
            }
        }

        public bool IsPermutation(string a, string b)
        {
            if (ReferenceEquals(a, b)) return true;

            a ??= "";
            b ??= "";
            if (a.Length != b.Length) return false; //order

            var sumA = (decimal)0;
            var sumB = (decimal)0;
            Parallel.Invoke( //this approach clocks in at 275ms while the run of the mil approach clocks in at around 800ms wow!
                () => { sumA = a.Select(x => (long) x).AsParallel().Sum(); },
                () => { sumB = b.Select(x => (long) x).AsParallel().Sum(); }
            );
            if (sumA != sumB) return false; //order

            //if (a == b) return true; //nopoint

            var charsCounterDictionary = new ConcurrentDictionary<char, CharCounter>();

            Parallel.Invoke(
                () => ProcessString(a, charsCounterDictionary, incrOrDecr: true),
                () => ProcessString(b, charsCounterDictionary, incrOrDecr: false)
            );

            return charsCounterDictionary.Values.All(charCounter => charCounter.Counter == 0);

            static void ProcessString(string s, ConcurrentDictionary<char, CharCounter> charsCounter, bool incrOrDecr)
            {
                var delta = incrOrDecr ? +1 : -1;
                foreach (var c in s)
                {
                    lock (charsCounter) //vital
                    {
                        charsCounter.AddOrUpdate( //0
                            key: c,
                            addValueFactory: key => new CharCounter(delta),
                            updateValueFactory: (key, value) => value.ChainTweak(delta)
                        );
                    }
                }

                //0 theoretically we could optimize this a bit further by cross checking between the two tasks
                //  to see if the letter pe 'z' has accumulated say 10 counts while the b string has only three
                //  characters left to provide   in this case it would make sense to bail out early   even though
                //  this would work its too much for too little gain
            }
        }

        [TestMethod]
        [DataRow(@"", @"")]
        [DataRow(null, @"")]
        [DataRow(@"", null)]
        [DataRow(null, null)]
        public void ShouldReturnTrueGivenEmptyOrNullStrings(string a, string b)
        {
            //Arrange

            //Act
            var verdict = new StringPermutationDetector().IsPermutation(a, b);

            //Assert
            verdict.Should().BeTrue();
        }

        [TestMethod]
        [DataRow(@"a", @"")]
        [DataRow(@"", @"a")]
        [DataRow(@"a", null)]
        [DataRow(null, @"a")]
        public void ShouldReturnFalseGivenStringsOfDifferentLengths(string a, string b)
        {
            //Arrange
            //...

            //Act
            var verdict = new StringPermutationDetector().IsPermutation(a, b);

            //Assert
            verdict.Should().BeFalse();
        }

        [TestMethod]
        [DataRow(@"a", @"b")]
        [DataRow(@"abc", @"aaa")]
        public void ShouldReturnFalseGivenDifferingStringsOfSameLength(string a, string b)
        {
            //Arrange
            //...

            //Act
            var verdict = new StringPermutationDetector().IsPermutation(a, b);

            //Assert
            verdict.Should().BeFalse();
        }

        [TestMethod]
        [DataRow(@"ab", @"ba")]
        [DataRow(@"aa", @"aa")]
        [DataRow(@"a a", @" aa")]
        [DataRow(@"ΝΙΨΩΝ ΑΝΩΜΙΜΑΤΑ ΜΗ ΜΟΝΑΝ ΟΨΙΝ", @"ΟΨΙΝ ΜΟΝΑΝ ΜΗ ΑΝΩΜΙΜΑΤΑ ΝΙΨΩΝ")]
        public void ShouldReturnTrueGivenAnagramStrings(string a, string b)
        {
            //Arrange
            //...

            //Act
            var verdict = new StringPermutationDetector().IsPermutation(a, b);

            //Assert
            verdict.Should().BeTrue();
        }

        [TestMethod]
        [DataRow(10_000_000)]
        //[DataRow(10_000_000)]
        //[DataRow(10_000_000)]
        //[DataRow(10_000_000)]
        public void ShouldReturnTrueGivenLengthyAnagramStrings(int length)
        {
            //Arrange
            var random = new Random();
            var a = RandomString(length, random);
            var b = a.Substring(10, a.Length - 10) + a.Substring(0, 10);

            //Act
            var verdict = new StringPermutationDetector().IsPermutation(a, b);

            //Assert
            verdict.Should().BeTrue();

            static string RandomString(int length, Random random)
            {
                const string chars = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789ςερτυθιοπασδφγηξκλζχψωβνμ ";
                return new string(Enumerable.Repeat(chars, length)
                    .Select(s => s[random.Next(s.Length)]).ToArray());
            }
        }
    }


}