using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;

namespace Testbed._c_Utilities.Cache
{
    [TestClass]
    public class CacheTests
    {
        [TestMethod]
        public void Cache_Add_Item_Ok()
        {
            var c = new Cache();

            const string o1 = "foo";
            c.AddOrUpdate("test", o1);
            Assert.AreSame(c.Get("test"), o1);
        }

        [TestMethod]
        public void Cache_Update_Item_Ok()
        {
            var c = new Cache();

            var o1 = "foo";
            var o2 = "bar";

            c.AddOrUpdate("test", o1);
            c.AddOrUpdate("test", o2);
            Assert.AreSame(c.Get("test"), o2);
        }

        [TestMethod]
        public void Cache_Expire_Item_Ok()
        {
            var c = new Cache();

            c.AddOrUpdate("test", "foo", 1);
            Thread.Sleep(1050);
            Assert.AreSame(c.Get("test"), null);
        }

        [TestMethod]
        public void Cache_Restart_Timer_Ok()
        {
            var c = new Cache();

            var o1 = new object();

            c.AddOrUpdate("test", o1, 1);
            Thread.Sleep(800); // wait almost a second

            Assert.IsTrue(c.Exists("test")); // still exists

            c.AddOrUpdate("test", o1, 1, true); // update and refresh the timer
            Thread.Sleep(1000); // wait another second

            Assert.IsTrue(c.Exists("test")); // still exists

            c.AddOrUpdate("test", o1, 1, false); // default parameter 4: false - no refresh of the timer

            Thread.Sleep(500); // it should expire now

            Assert.IsNull(c.Get("test")); // no longer cached
        }

        [TestMethod]
        public void Cache_Indexer_Found_Ok()
        {
            var c = new Cache();

            var o1 = new object();

            c.AddOrUpdate("test", o1, 1);
            Assert.AreSame(c["test"], o1);
        }

        [TestMethod]
        public void Cache_Indexer_Not_Found_Ok()
        {
            var c = new Cache();
            Assert.IsNull(c["test2"]);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Cache_Timeout_Zero_Throws_Exception()
        {
            var c = new Cache();
            c.AddOrUpdate("test", new object(), 0);
        }

        [TestMethod]
        public void Cache_Clear_Ok()
        {
            var c = new Cache();
            c.AddOrUpdate("test", new object());
            Assert.IsTrue(c.Exists("test"));
            c.Clear();
            Assert.IsFalse(c.Exists("test"));
        }

        [TestMethod]
        public void Cache_Remove_By_Pattern_Ok()
        {
            var c = new Cache();
            c.AddOrUpdate("test1", new object());
            c.AddOrUpdate("test2", new object());
            c.AddOrUpdate("test3", new object());
            c.AddOrUpdate("Other", new object());
            Assert.IsTrue(c.Exists("test1"));
            Assert.IsTrue(c.Exists("Other"));

            c.Remove(k => k.StartsWith("test"));

            Assert.IsFalse(c.Exists("test1"));
            Assert.IsFalse(c.Exists("test2"));
            Assert.IsFalse(c.Exists("test3"));
            Assert.IsTrue(c.Exists("Other"));
        }
    }
}