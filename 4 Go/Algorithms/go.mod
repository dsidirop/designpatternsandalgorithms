module github.com/dsidirop/go-algorithms

go 1.16

require (
	github.com/cweill/gotests v1.6.0 // indirect
	github.com/davidrjenni/reftools v0.0.0-20210213085015-40322ffdc2e4 // indirect
	github.com/fatih/gomodifytags v1.13.0 // indirect
	github.com/josharian/impl v1.0.0 // indirect
	github.com/karrick/godirwalk v1.16.1 // indirect
	github.com/lukehoban/go-outline v0.0.0-20161011150102-e78556874252 // indirect
	github.com/newhook/go-symbols v0.0.0-20151212134530-b75dfefa0d23 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/uudashr/gopkgs/v2 v2.1.2 // indirect
	golang.org/x/sys v0.0.0-20210521203332-0cec03c779c1 // indirect
	golang.org/x/tools v0.1.1 // indirect
	golang.org/x/tools/gopls v0.6.11 // indirect
)
