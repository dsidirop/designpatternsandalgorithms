// Write a function that finds the zerobased index of the longest run in a string    A run is a consecutive sequence of the same
// character  If there is more than one run with the same length return the index of the first one
//
// For example IndexOfLongestRun("abbcccddddcccbba") should return 6 as the longest run is dddd and it first appears on index 6

package StringLongestRun

type IStringLongestRun interface {
	FindIndexOfLongestRun(input string) (length int, error error)
}

type StringLongestRun struct {
}

func SpawnStringLongestRun() IStringLongestRun {
	return &StringLongestRun{}
}

func (s *StringLongestRun) FindIndexOfLongestRun(input string) (longestRun int, error error) {
	var length = len(input)
	if length == 0 {
		return -1, nil
	}

	var c = string([]rune(input)[0])
	var maxLength = 0
	var currentLength = 0
	var sequenceStartIndex = 0
	var maxLengthStartIndex = 0
	for i := 0; i <= length; i++ { //notice the use of <=
		var t = ""
		if i < length {
			t = string([]rune(input)[i])
		}

		if c == t {
			currentLength++
			if currentLength > maxLength && length-i /*charsremaining*/ <= currentLength {
				maxLengthStartIndex = sequenceStartIndex //order
				break
			}
			continue
		}

		if maxLength < currentLength {
			maxLength = currentLength
			maxLengthStartIndex = sequenceStartIndex //order
		}

		if length-i <= maxLength {
			break
		}

		c = ""
		if len(t) > 0 {
			c = t
		}

		currentLength = 0
		sequenceStartIndex = i //order
	}

	return maxLengthStartIndex, nil
}
