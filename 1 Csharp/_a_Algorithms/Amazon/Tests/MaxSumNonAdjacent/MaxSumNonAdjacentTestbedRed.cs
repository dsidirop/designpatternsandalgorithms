﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.MaxSumNonAdjacent
{
    [TestClass]
    public class MaxSumNonAdjacentTestbedRed
    {
        [TestMethod]
        public void ShouldThrowArgumentExceptionGivenNullArray()
        {
            //Arrange
            var input = (int[]) null;

            //Act
            var action = new Action(() => Amazon.MaxSumNonAdjacent.Calculate(input));

            //Assert
            action.Should().ThrowExactly<ArgumentNullException>().WithMessage("*'input'*");
        }
    }
}
