﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testbed._a_Algorithms.Amazon.Tests.CountTheTriplets
{
    [TestClass]
    public class CountTheTripletsTestbedGreen
    {
        [TestMethod]
        [DataRow(new[] {1}, 0)]
        [DataRow(new[] {1, 2}, 0)]
        [DataRow(new[] {1, 2, 3}, 1)]
        [DataRow(new[] {1, 2, 3, 4}, 2)]
        [DataRow(new[] {1, 5, 3, 2}, 2)]
        public void ShouldReturnFoundGivenMatchingParameters(in int[] input, in int expectedCount)
        {
            //Arrange
            //...

            //Act
            var count = Amazon.CountTheTriplets.Calculate(input);

            //Assert
            count.Should().Be(expectedCount);
        }
    }
}
