﻿using C5;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

// https://c5docs.azurewebsites.net/interfaceC5_1_1ISorted.html
//
// although we do use parallelization the performance is horrible with this approach
// because merging the trees in the end proves to be very costly despite the fact that
// we employ C5 for optimum performance

namespace Testbed._a_Algorithms.ATCom
{
    [TestClass]
    public class MovieNight1aTreeSortWithC5
    {
        public class Movie : IComparable<Movie>
        {
            public DateTime End { get; private set; }
            public DateTime Start { get; private set; }

            public Movie(DateTime start, DateTime end)
            {
                End = end;
                Start = start;
            }

            public int CompareTo(Movie other)
            {
                if (ReferenceEquals(this, other)) return 0;
                if (ReferenceEquals(null, other)) return 1;
                //var endComparison = End.CompareTo(other.End);
                //if (endComparison != 0) return endComparison;
                return Start.CompareTo(other.Start);
            }
        }

        public class MovieSpecialComparer : IComparer<Movie>, IEqualityComparer<Movie>
        {
            public int Compare(Movie x, Movie y)
            {
                if (ReferenceEquals(x, y)) return 0;
                if (ReferenceEquals(x, null)) return -1;
                if (ReferenceEquals(y, null)) return 1;

                var xStartVsYEnd = x.Start.CompareTo(y.End);
                var xStartVsYStart = x.Start.CompareTo(y.Start);

                var xStartContainedInY = xStartVsYStart >= 0 && xStartVsYEnd < 0; //   its perfectly ok if   y.End == x.Start
                if (xStartContainedInY) throw new MovieOverlapDetectedException();

                var xEndVsYEnd = x.End.CompareTo(y.End);
                var xEndVsYStart = x.End.CompareTo(y.Start);

                var xEndContainedInY = xEndVsYStart > 0 && xEndVsYEnd <= 0; //         its perfectly ok if   y.Start == x.End
                if (xEndContainedInY) throw new MovieOverlapDetectedException();

                var yStartContainedInX = xStartVsYStart <= 0 && xEndVsYEnd > 0; //     its perfectly ok if   x.End == y.Start
                if (yStartContainedInX) throw new MovieOverlapDetectedException();

                var yEndContainedInX = xStartVsYEnd < 0 && xEndVsYEnd >= 0; //         its perfectly ok if   x.Start == y.End
                if (yEndContainedInX) throw new MovieOverlapDetectedException();

                return xStartVsYStart;
            }

            public bool Equals(Movie x, Movie y)
            {
                var xStartVsYEnd = x.Start.CompareTo(y.End);
                var xStartVsYStart = x.Start.CompareTo(y.Start);

                var xStartContainedInY = xStartVsYStart >= 0 && xStartVsYEnd < 0; //   its perfectly ok if   y.End == x.Start
                if (xStartContainedInY) return true;

                var xEndVsYEnd = x.End.CompareTo(y.End);
                var xEndVsYStart = x.End.CompareTo(y.Start);

                var xEndContainedInY = xEndVsYStart > 0 && xEndVsYEnd <= 0; //         its perfectly ok if   y.Start == x.End
                if (xEndContainedInY) return true;

                var yStartContainedInX = xStartVsYStart <= 0 && xEndVsYEnd > 0; //     its perfectly ok if   x.End == y.Start
                if (yStartContainedInX) return true;

                var yEndContainedInX = xStartVsYEnd < 0 && xEndVsYEnd >= 0; //         its perfectly ok if   x.Start == y.End
                if (yEndContainedInX) return true;

                return false;
            }

            public int GetHashCode(Movie obj)
            {
                return 0;
            }
        }

        public class MovieOverlapDetectedException : Exception
        {
        }

        static public bool CanViewAll(IEnumerable<Movie> movies)
        {
            if (!(movies?.Any() ?? false) || !movies.Skip(1).Any())
                return true;

            try
            {
                var tokenSource = new CancellationTokenSource();
                var parallelOptions = new ParallelOptions { CancellationToken = tokenSource.Token };

                var options = new ParallelOptions { CancellationToken = new CancellationToken() };
                var comparer = new MovieSpecialComparer();
                var allMovieSets = new[]
                {
                    new TreeSet<Movie>(comparer),
                    new TreeSet<Movie>(comparer)
                };

                var counter = 0;
                var factoryLock = new object();
                var localStateFactory = new Func<(TreeSet<Movie> Set, CancellationTokenSource TokenSource)>(() =>
                {
                    lock (factoryLock)
                    {
                        return (
                            Set: allMovieSets[counter++ % allMovieSets.Length],
                            TokenSource: tokenSource
                        );
                    }
                });

                Parallel.ForEach(
                    movies,
                    options,
                    localStateFactory,
                    (movie, loopState, localStorage) =>
                    {
                        if (parallelOptions.CancellationToken.IsCancellationRequested)
                        {
                            loopState.Stop();
                            return localStorage;
                        }

                        try
                        {
                            lock (localStorage.Set)
                            {
                                localStorage.Set.Add(movie);
                            }
                        }
                        catch (MovieOverlapDetectedException)
                        {
                            loopState.Stop();
                            localStorage.TokenSource.Cancel();
                            throw;
                        }

                        return localStorage;
                    },
                    localStorage => { }
                );

                var allNonEmptyMovieSets = allMovieSets.Where(x => !x.IsEmpty).ToArray();
                if (allNonEmptyMovieSets.Length <= 1)
                    return true;

                var firstNonEmptySet = allNonEmptyMovieSets.First();
                var setsThatMightContainingOverlapsWithFirstSet = allNonEmptyMovieSets
                    .Skip(1)
                    .Where(
                        x => firstNonEmptySet.Min().Start <= x.Min().Start && firstNonEmptySet.Max().End > x.Min().Start
                             || firstNonEmptySet.Min().Start < x.Min().End && firstNonEmptySet.Max().End >= x.Min().End
                             || x.Min().Start <= firstNonEmptySet.Min().Start && x.Max().End > firstNonEmptySet.Min().Start
                             || x.Min().Start < firstNonEmptySet.Min().End && x.Max().End >= firstNonEmptySet.Min().End
                    )
                    .ToList();

                setsThatMightContainingOverlapsWithFirstSet.ForEach(x => firstNonEmptySet.AddSorted(x)); //also tried contains but it performs horribly

                return true;
            }
            catch (Exception ex) //also need this as well
            {
                for (; ex != null; ex = ex.InnerException)
                {
                    if (ex is MovieOverlapDetectedException)
                        return false;
                }

                throw;
            }

            //0 this approach is generally worst than quicksort   it can only prove somewhat useful
            //  if and only if the following two conditions hold at the same time
            //
            //  1  we cant modify the original list
            //  2  its very frequent to be given movie lists which contain collisions
            //
            //  if any of these two conditions dont hold its probably better to resort to quicksort
        }

        [TestMethod]
        public void CanViewAllShouldReturnTrueGivenEmptyListOfMovies()
        {
            //Arrange
            var movies1 = new Movie[] { };

            //Act
            var verdict = CanViewAll(movies1);

            //Assert
            verdict.Should().BeTrue();
        }


        [TestMethod]
        public void CanViewAllShouldReturnTrueGivenSingleItemList()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;
            var movies1 = new[]
            {
                new Movie(DateTime.Parse("1/1/2015 20:00", format), DateTime.Parse("1/1/2015 21:30", format))
            };

            //Act
            var verdict = CanViewAll(movies1);

            //Assert
            verdict.Should().BeTrue();
        }

        [TestMethod]
        public void CanViewAllShouldReturnTrueGivenNonOverlappingMovies()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;
            var movies1 = new[]
            {
                new Movie(DateTime.Parse("1/1/2015 20:00", format), DateTime.Parse("1/1/2015 21:30", format)),
                new Movie(DateTime.Parse("1/1/2015 23:10", format), DateTime.Parse("1/1/2015 23:30", format)),
                new Movie(DateTime.Parse("1/1/2015 21:30", format), DateTime.Parse("1/1/2015 23:00", format))
            };

            //Act
            var verdict = CanViewAll(movies1);

            //Assert
            verdict.Should().BeTrue();
        }

        [TestMethod]
        public void CanViewAllShouldReturnFalseGivenOverlappingMovies1()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;
            var movies2 = new[]
            {
                new Movie(DateTime.Parse("1/1/2015 20:00", format), DateTime.Parse("1/1/2015 21:30", format)),
                new Movie(DateTime.Parse("1/1/2015 21:15", format), DateTime.Parse("1/1/2015 21:45", format)), //overlap
                new Movie(DateTime.Parse("1/1/2015 21:30", format), DateTime.Parse("1/1/2015 23:00", format))
            };

            //Act
            var verdict = CanViewAll(movies2);

            //Assert
            verdict.Should().BeFalse();
        }

        [TestMethod]
        public void CanViewAllShouldReturnFalseGivenOverlappingMovies2()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;
            var movies3 = new[]
            {
                new Movie(DateTime.Parse("1/1/2015 21:15", format), DateTime.Parse("1/1/2015 21:45", format)),
                new Movie(DateTime.Parse("1/1/2015 20:00", format), DateTime.Parse("1/1/2015 21:30", format)), //overlap
                new Movie(DateTime.Parse("1/1/2015 21:30", format), DateTime.Parse("1/1/2015 23:00", format))
            };

            //Act
            var verdict = CanViewAll(movies3);

            //Assert
            verdict.Should().BeFalse();
        }

        [TestMethod]
        public void CanViewAllShouldReturnTrueGivenMillionsOfNonOverlappingMovies()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;
            var current = DateTime.Parse("1/1/2015 21:15", format);
            var movies = Enumerable.Range(0, 1_000_000).Select(x =>
            {
                var start = current;
                current = current.AddMinutes(5);
                return new Movie(start, current);
            }).ToArray();

            //Act
            var verdict = CanViewAll(movies);

            //Assert
            verdict.Should().BeTrue();
        }

        [TestMethod]
        public void CanViewAllShouldReturnFalseGivenMillionsOfOverlappingMovies()
        {
            //Arrange
            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;

            var current = DateTime.Parse("1/1/2015 21:15", format);
            var movies = Enumerable.Range(0, 1_000_000).Select(x =>
            {
                if (x == 650_000)
                {
                    return new Movie(DateTime.Parse("1/1/2015 21:15", format), DateTime.Parse("1/1/2015 21:30", format));
                }

                var start = current;
                current = current.AddMinutes(5);
                return new Movie(start, current);
            }).ToArray();

            //Act
            var verdict = CanViewAll(movies);

            //Assert
            verdict.Should().BeFalse();
        }
    }
}
