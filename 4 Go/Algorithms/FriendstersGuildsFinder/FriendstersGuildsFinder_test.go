package FriendstersGuildsFinder

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_FriendstersGuildsFinder_ShouldReturnEmptyResults_GivenEmptyInput(t *testing.T) {
	//Arrange
	var guildFinder = SpawnFriendstersGuildsFinder().InitGraph(make([]Friendship, 0))

	//Act
	results := guildFinder.Run()

	//Assert
	assert.Equal(t, 0, len(results))
}
