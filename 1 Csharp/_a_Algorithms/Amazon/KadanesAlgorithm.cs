﻿//
//      https://practice.geeksforgeeks.org/problems/kadanes-algorithm/0
//      https://www.geeksforgeeks.org/largest-sum-contiguous-subarray/
//      https://en.wikipedia.org/wiki/Maximum_subarray_problem
//      https://www.youtube.com/watch?v=2MmGzdiKR9Y
//
// Given an array of random integer numbers (positive zero or negative) find the subarray
// (startindex endindex and sum) which yields the maximum sum   If two subarrays have the
// yield the same sum it should return the subarray of maximum length
//
// Applications
//
//       Maximum subarray problems arise in many fields such as genomic sequence analysis and computer vision
//
//       Genomic sequence analysis employs maximum subarray algorithms to identify important biological segments
//       of protein sequences  These problems include conserved segments GCrich regions tandem repeats lowcomplexity
//       filter DNA binding domains and regions of high charge
//
//       In computer vision maximumsubarray algorithms are used on bitmap images to detect the brightest area in an image
//
// Notes
//
//       Meets both criteria for a dp driven Solution
//
// Parallelization
//
//       Seems to be extremely hard for little gain
//
// Complexities
//
//       Time   O(N)
//       Space  O(1)
//

using Ardalis.GuardClauses;

namespace Testbed._a_Algorithms.Amazon
{
    public class KadanesAlgorithm
    {
        static public (int StartIndex, int EndIndex, decimal Sum) Calculate(in int[] input)
        {
            var length = input?.Length ?? 0;
            Guard.Against.Null(input, nameof(input));
            Guard.Against.Zero(length, nameof(input));

            var overallMaxSoFar = decimal.MinValue;
            var maxEndingHere = 0M;
            
            var s = 0;
            var end = 0;
            var start = 0;

            for (var i = 0; i < length; i++)  
            { 
                maxEndingHere += input[i];
                if (overallMaxSoFar < maxEndingHere)
                {
                    end = i;
                    start = s;
                    overallMaxSoFar = maxEndingHere;
                } 
  
                if (maxEndingHere < 0)  
                {
                    s = i + 1;
                    maxEndingHere = 0;
                } 
            } 

            return (start, end, overallMaxSoFar);
        }
    }
}
