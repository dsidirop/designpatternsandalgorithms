package main

import (
	"fmt"
	"reflect"
)

func do(x interface{}) {
	// v := x.(type)
	// ^--- this will cause a compilation error because its outside of a switch

	switch v := x.(type) { // .(type) must be exactly here
	case int:
		fmt.Printf("Twice %v is %v\n", v, v*2)
	case string:
		fmt.Printf("%q is %v bytes long\n", v, len(v))
	default:
		fmt.Printf("I don't know about type %T!\n", v)
	}

	fmt.Printf("** Reflected type is: %q\n", reflect.TypeOf(x))
}

func main() {
	do(21)
	do("hello")
	do(true)
}
